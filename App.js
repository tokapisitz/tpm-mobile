import React, { Component } from 'react';
import { createSwitchNavigator, createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { View } from 'react-native';






import login from './src/login';
import choose from './src/choose';
import barcode from './src/repair/barcode';
import chooseRepair from './src/repair/choose';
import detailRepair from './src/repair/detail';
import listdetailRepair from './src/repair/listdetail';
import listdetailRepairData from './src/repair/listdetailrepair';
import continueData from './src/repair/continue';
import history from './src/repair/history';
import addRepair from './src/repair/add';
import adddetailRepair from './src/repair/adddetail';
import success from './src/work/success';
import detail from './src/work/detail';
import add from './src/work/add';
import listdetail from './src/work/listdetail';
import adddetail from './src/work/adddetail';




const RootStack = createSwitchNavigator(
  {

    Login: login,
    App: createStackNavigator({
      Choose: choose,
      Barcode: barcode,
      DetailRepair: detailRepair,
      ListDetailRepair: listdetailRepair,
      ListDetailRepairData: listdetailRepairData,
      Continue: continueData,
      History: history,
      ChooseRepair: chooseRepair,
      AddRepair: addRepair,
      Add: add,
      Detail: detail,
      ListDetail: listdetail,
      AddDetail: adddetail,
    }),
    Success: success,
    AddDetailRepair: adddetailRepair,

  },
  {
    initialRouteName: 'Login',
  }
);

export default createAppContainer(RootStack);
