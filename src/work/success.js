import React, { useState, useCallback, useMemo } from 'react';
import { StyleSheet, View, Text, Keyboard, Alert, Image, ActivityIndicator, AsyncStorage, Platform, StatusBar, Linking, TouchableOpacity } from 'react-native';
import { Button, Input } from "react-native-elements";
import { useNavigation, useNavigationParam } from 'react-navigation-hooks'


const Success = () => {
    const { navigate } = useNavigation();
    return (
        <View style={styles.MainContainer}>

            {/* <View style={{
                flex: 0.4, justifyContent: 'center',
                alignItems: 'center',
            }} >
               
            </View> */}
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }} >

                <Image
                    style={styles.stretch}
                    source={require('../../img/lin.png')}
                ></Image>

                <Text style={[styles.baseText, { marginTop: 32, fontSize: 24, alignSelf: "center", color: "#3E5481", fontFamily: "Prompt-Regular" }]}>บันทึกข้อมูลเรียบร้อยเเล้ว</Text>

                <Button
                    buttonStyle={{ backgroundColor: "#4DB6AC", height: 50, width: 200, alignSelf: "center", marginTop: 48, borderRadius: 25, borderWidth: 1, borderColor: '#4DB6AC' }}
                    title="กลับสู่หน้าหลัก"
                    titleStyle={{ color: '#FFF', fontSize: 18, fontFamily: "Prompt-Bold" }}
                    onPress={() =>
                        navigate('Choose')

                    }>
                </Button>
            </View>




        </View>
    );
}

const styles = StyleSheet.create({
    MainContainer: {
        justifyContent: 'center',
        flex: 1,
        backgroundColor: '#FFF',
        flexDirection: "column",
    },
    loadding_style: {
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
        backgroundColor: '#fff',
    },
    appleBtn: { height: 44, width: 320, marginTop: 16 },
    spinnerTextStyle: {
        color: '#FFF',
        fontFamily: "NotoSans-Regular",
        fontSize: 14
    },
    button: {
        marginTop: 24,
        backgroundColor: "#e42179",
        width: "100%",
    },
    stretch: {
        resizeMode: "stretch",
        justifyContent: 'center',
        alignItems: 'center'
    },
    defaultText: {
        fontSize: 16,
        color: '#000'
    },
    baseText: {
        fontSize: 20,
    },

});

export default Success