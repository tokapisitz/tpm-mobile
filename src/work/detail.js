import React, { useState, useCallback, useMemo, useEffect } from 'react';
import { StyleSheet, View, Text, Keyboard, Alert, Image, ActivityIndicator, AsyncStorage, Platform, StatusBar, Linking, ScrollView, TouchableOpacity, TextInput } from 'react-native';
import { Divider, Input, Button, Card } from 'react-native-elements';
import { useNavigation, useNavigationParam } from 'react-navigation-hooks'
import axios from 'axios';


const Detail = () => {
    const { navigate } = useNavigation();
    const [data, setData] = useState({});
    const [image_show, setImageShow] = useState('');

    useEffect(() => {
        async function fetchMyAPI() {
            callDetailData(await AsyncStorage.getItem('token'), await AsyncStorage.getItem('jobId'))
        }
        fetchMyAPI()
    }, [])



    callDetailData = async (token, id) => {
        //  setLoading(true)
        axios.get('http://apitpm.bsisugarcane.com/api/RequestServiceApp/jobDetail?jobId=' + Number(id) + '&token=' + token)
            .then((response) => {
                console.log('response response 1 : ', response.data)
                var data = response.data.Results
                setData(data)
                setImageShow(data.File1Path)
            })
            .catch((error) => {
                console.log('error response : ', error)
            })
            .finally(function () {
            });

    };
    return (
        <View style={styles.MainContainer}>

            {/* <View style={{ backgroundColor: "#3E5481", height: 100 }}>

                <Text style={[styles.baseText, { marginTop: 48, marginLeft: 48, color: "#FFF", fontFamily: "Prompt-Bold", fontSize: 24 }]}>รายละเอียดใบขอใช้บริการ</Text>

            </View> */}

            <ScrollView style={{ paddingHorizontal: '2%' }}>
                <View style={{ margin: 8, marginBottom: 36 }}>

                    {/* <View style={{
                        flexDirection: "row",
                    }}>
                        <View style={{ flex: 0.5 }}>
                            <Text style={[styles.baseText, { marginTop: 36, marginLeft: 48, color: "#000", fontFamily: "THSarabunNew" }]}>เลขทะเบียนเครื่องจักร</Text>
                            <Text style={[styles.baseTextTwo, { marginTop: 18, marginLeft: 48, color: "#000", fontFamily: "THSarabunNew" }]}>เเผนก xxxx</Text>
                            <Text style={[styles.baseTextTwo, { marginTop: 12, marginLeft: 56, color: "#000", fontFamily: "THSarabunNew" }]}>เลขทะเบียนเครื่องจักร</Text>
                            <Text style={[styles.baseTextTwo, { marginTop: 6, marginLeft: 56, color: "#000", fontFamily: "THSarabunNew" }]}>เเผนก xxxx</Text>
                            <Text style={[styles.baseTextTwo, { marginTop: 6, marginLeft: 56, color: "#000", fontFamily: "THSarabunNew" }]}>ขนาด/ยี่ห้อ/รุ่น</Text>

                        </View>
                        <View style={{ flex: 0.5 }}>
                            <Text style={[styles.baseText, { marginTop: 40, marginLeft: 24, color: "#000", fontFamily: "THSarabunNew" }]}>รูปภาพ</Text>
                            <Card containerStyle={{ borderRadius: 20, backgroundColor: "#F5F5F5" }}>
                                <View style={{
                                    height: 140,
                                    justifyContent: 'center',
                                    alignItems: 'center',

                                }}>
                                    <Image

                                        source={require('../img/image.png')}
                                    ></Image>

                                </View>


                            </Card>
                        </View>
                    </View> */}

                    {/* <Text style={[styles.baseText, { marginTop: 36, marginLeft: 32, color: "#000", fontFamily: "THSarabunNew" }]}>ส่วนประกอบ</Text> */}


                    <Card containerStyle={{ borderRadius: 20, flex: 0.9, marginLeft: 4, marginRight: 4 }}>


                        <Text style={[styles.baseText, { marginTop: 4, marginLeft: 12, color: "#00598B", fontFamily: "Prompt-Regular" }]}>วันที่:</Text>
                        <Input
                            inputStyle={{
                                paddingLeft: 6, borderWidth: 1,
                                borderRadius: 4,
                                borderColor: 'lightgrey',
                                fontFamily: "Prompt-Regular"
                            }}
                            disabled
                            inputContainerStyle={{ borderBottomWidth: 0 }}
                        >

                            {<Text style={[styles.baseText, { marginTop: 4, marginLeft: 8, color: "#000", fontFamily: "Prompt-Regular" }]}>{data.CreatedDateText}</Text>}
                        </Input>

                        <Text style={[styles.baseText, { marginLeft: 12, color: "#00598B", fontFamily: "Prompt-Regular" }]}>เเผนกของผู้ขอใช้บริการ:</Text>
                        <Input
                            inputStyle={{
                                paddingLeft: 6, borderWidth: 1,
                                borderRadius: 4,
                                borderColor: 'lightgrey',
                                fontFamily: "Prompt-Regular"
                            }}
                            disabled
                            inputContainerStyle={{ borderBottomWidth: 0 }}
                        >
                            {<Text style={[styles.baseText, { marginTop: 4, marginLeft: 8, color: "#000", fontFamily: "Prompt-Regular" }]}>{data.Section}</Text>}
                        </Input>

                        <Text style={[styles.baseText, { marginLeft: 12, color: "#00598B", fontFamily: "Prompt-Regular" }]}>งานที่ขอใช้บริการ:</Text>
                        <Input
                            inputStyle={{
                                paddingLeft: 6, borderWidth: 1,
                                borderRadius: 4,
                                borderColor: 'lightgrey',
                                fontFamily: "Prompt-Regular"
                            }}
                            disabled
                            inputContainerStyle={{ borderBottomWidth: 0 }}
                        >
                            {<Text style={[styles.baseText, { marginTop: 4, marginLeft: 8, color: "#000", fontFamily: "Prompt-Regular" }]}>{data.Topic}</Text>}
                        </Input>


                        <Text style={[styles.baseText, { marginLeft: 12, color: "#00598B", fontFamily: "Prompt-Regular" }]}>รหัสหลักทรัพย์:</Text>
                        <Input
                            inputStyle={{
                                paddingLeft: 6, borderWidth: 1,
                                borderRadius: 4,
                                borderColor: 'lightgrey',
                                fontFamily: "Prompt-Regular"
                            }}
                            disabled
                            inputContainerStyle={{ borderBottomWidth: 0 }}
                        >
                            {<Text style={[styles.baseText, { marginTop: 4, marginLeft: 8, color: "#000", fontFamily: "Prompt-Regular" }]}>{data.AssetNo}</Text>}
                        </Input>


                        <Text style={[styles.baseText, { marginLeft: 12, color: "#00598B", fontFamily: "Prompt-Regular" }]}>ฝ่าย:</Text>
                        <Input
                            inputStyle={{
                                paddingLeft: 6, borderWidth: 1,
                                borderRadius: 4,
                                borderColor: 'lightgrey',
                                fontFamily: "Prompt-Regular"
                            }}
                            disabled
                            inputContainerStyle={{ borderBottomWidth: 0 }}
                        >
                            {<Text style={[styles.baseText, { marginTop: 4, marginLeft: 8, color: "#000", fontFamily: "Prompt-Regular" }]}>{data.DepartmentWorker}</Text>}
                        </Input>


                        <Text style={[styles.baseText, { marginLeft: 12, color: "#00598B", fontFamily: "Prompt-Regular" }]}>เเผนก:</Text>
                        <Input
                            inputStyle={{
                                paddingLeft: 6, borderWidth: 1,
                                borderRadius: 4,
                                borderColor: 'lightgrey',
                                fontFamily: "Prompt-Regular"
                            }}
                            disabled
                            inputContainerStyle={{ borderBottomWidth: 0 }}
                        >
                            {<Text style={[styles.baseText, { marginTop: 4, marginLeft: 8, color: "#000", fontFamily: "Prompt-Regular" }]}>{data.Section}</Text>}
                        </Input>



                        <Text style={[styles.baseText, { marginLeft: 12, color: "#00598B", fontFamily: "Prompt-Regular" }]}>สถานที่:</Text>
                        <Input
                            inputStyle={{
                                paddingLeft: 6, borderWidth: 1,
                                borderRadius: 4,
                                borderColor: 'lightgrey',
                                fontFamily: "Prompt-Regular"
                            }}
                            disabled
                            inputContainerStyle={{ borderBottomWidth: 0 }}
                        >
                            {<Text style={[styles.baseText, { marginTop: 4, marginLeft: 8, color: "#000", fontFamily: "Prompt-Regular" }]}>{data.Location}</Text>}
                        </Input>


                        <Text style={[styles.baseText, { marginLeft: 12, color: "#00598B", fontFamily: "Prompt-Regular" }]}>ประเภทงาน:</Text>
                        <Input
                            inputStyle={{
                                paddingLeft: 6, borderWidth: 1,
                                borderRadius: 4,
                                borderColor: 'lightgrey',
                                fontFamily: "Prompt-Regular"
                            }}
                            disabled
                            inputContainerStyle={{ borderBottomWidth: 0 }}
                        >
                            {<Text style={[styles.baseText, { marginTop: 4, marginLeft: 8, color: "#000", fontFamily: "Prompt-Regular" }]}>{data.DepartmentJob}</Text>}
                        </Input>


                        <Text style={[styles.baseText, { marginLeft: 12, color: "#00598B", fontFamily: "Prompt-Regular" }]}>กำหนดเริ่มงาน:</Text>
                        <Input
                            inputStyle={{
                                paddingLeft: 6, borderWidth: 1,
                                borderRadius: 4,
                                borderColor: 'lightgrey',
                                fontFamily: "Prompt-Regular"
                            }}
                            disabled
                            inputContainerStyle={{ borderBottomWidth: 0 }}
                        >
                            {<Text style={[styles.baseText, { marginTop: 4, marginLeft: 8, color: "#000", fontFamily: "Prompt-Regular" }]}>{data.StartTargetDateText}</Text>}
                        </Input>


                        <Text style={[styles.baseText, { marginLeft: 12, color: "#00598B", fontFamily: "Prompt-Regular" }]}>กำหนดเเล้วเสร็จ:</Text>
                        <Input
                            inputStyle={{
                                paddingLeft: 6, borderWidth: 1,
                                borderRadius: 4,
                                borderColor: 'lightgrey',
                                fontFamily: "Prompt-Regular"
                            }}
                            inputContainerStyle={{ borderBottomWidth: 0 }}
                            disabled
                        >
                            {<Text style={[styles.baseText, { marginTop: 4, marginLeft: 8, color: "#000", fontFamily: "Prompt-Regular" }]}>{data.EndTargetDateText}</Text>}
                        </Input>


                        <Text style={[styles.baseText, { marginLeft: 12, color: "#00598B", fontFamily: "Prompt-Regular" }]}>เครื่องจักร:</Text>
                        <Input
                            inputStyle={{
                                paddingLeft: 6, borderWidth: 1,
                                borderRadius: 4,
                                borderColor: 'lightgrey',
                                fontFamily: "Prompt-Regular"
                            }}
                            disabled
                            inputContainerStyle={{ borderBottomWidth: 0 }}
                        >
                            {<Text style={[styles.baseText, { marginTop: 4, marginLeft: 8, color: "#000", fontFamily: "Prompt-Regular" }]}>{data.MachineUnit}</Text>}
                        </Input>


                        <Text style={[styles.baseText, { marginLeft: 12, color: "#00598B", fontFamily: "Prompt-Regular" }]}>ความเร่งด่วน:</Text>
                        <Input
                            inputStyle={{
                                paddingLeft: 6, borderWidth: 1,
                                borderRadius: 4,
                                borderColor: 'lightgrey',
                                fontFamily: "Prompt-Regular"
                            }}
                            disabled
                            inputContainerStyle={{ borderBottomWidth: 0 }}
                        >
                            {<Text style={[styles.baseText, { marginTop: 4, marginLeft: 8, color: "#000", fontFamily: "Prompt-Regular" }]}>{data.PriorityRequestService}</Text>}
                        </Input>

                        <Text style={[styles.baseText, { marginLeft: 12, color: "#00598B", fontFamily: "Prompt-Regular" }]}>เหตุผลการลา</Text>
                        <TextInput
                            multiline={true}
                            style={{
                                fontSize: 18, marginTop: 12, paddingLeft: 8, height: 80, fontFamily: "Prompt-Regular", textAlignVertical: 'top', borderWidth: 1,
                                borderRadius: 4,
                                borderColor: 'lightgrey',
                                marginLeft: 10,
                                marginRight: 10,
                                fontFamily: "Prompt-Regular"
                            }}
                            editable={false}
                            selectTextOnFocus={false}
                            value={data.Detail}

                            onChange={(value) => {
                                onChangeTextRemark(value)
                            }}
                        />




                        <Card containerStyle={{ borderRadius: 20, height: 360, marginLeft: 4, marginRight: 4 }}>

                            <Text style={[styles.baseText, { marginLeft: 4, color: "#000", fontFamily: "Prompt-Regular" }]}>ไฟล์เเนบ</Text>


                            <Text style={[styles.baseText, { marginTop: 24, marginLeft: 24, color: "#000", fontFamily: "Prompt-Regular" }]}>ไฟล์ที่ 1</Text>

                            <View style={{
                                height: 160, justifyContent: 'center',
                                alignItems: 'center',

                            }}>
                                <Image
                                    style={{ height: 160, width: 160 }}
                                    source={{ uri: image_show }}
                                ></Image>

                            </View>


                            <Text style={[styles.baseText, { marginTop: 24, marginLeft: 24, color: "#000", fontFamily: "Prompt-Regular" }]}>ไฟล์ที่ 2</Text>

                            <TouchableOpacity
                                onPress={() =>
                                    setImageShow(data.File2Path)
                                }
                            >
                                <Text style={[styles.baseText, { marginLeft: 24, color: "#2D9CDB", fontFamily: "Prompt-Regular" }]}>คลิ๊กดูไฟล์เเนบที่ 2</Text>

                            </TouchableOpacity>


                        </Card>




                    </Card>



                    <View style={{
                        justifyContent: 'center',
                        alignItems: 'flex-start',
                        flexDirection: "row"
                    }}>




                        <Button
                            buttonStyle={{ backgroundColor: "#4DB6AC", height: 50, width: 200, alignSelf: "center", borderRadius: 25, marginTop: 20, borderWidth: 1, borderColor: '#4DB6AC' }}
                            title="ส่งรายงาน"

                            titleStyle={{ color: '#FFF', fontSize: 18, fontFamily: "Prompt-Bold", paddingBottom: 30 }}
                            onPress={() =>
                                navigate('AddDetail')
                                // 
                                //  

                            }>
                        </Button>




                    </View>

                    {/* <TouchableOpacity
                        onPress={() =>
                            navigate('ListDetail')
                        }
                    >

                        <View style={{
                            justifyContent: 'flex-start',
                            alignItems: 'flex-start',
                            borderRadius: 20,
                            marginTop: 12,
                            marginLeft: 36,
                            width: 40,
                            height: 40,
                            backgroundColor: "#80CBC4",
                            justifyContent: "center"
                        }}>

                            <Image
                                style={{ width: 20, height: 20, alignSelf: "center" }}
                                source={require('../../img/arrow.png')}
                            ></Image>


                        </View>

                    </TouchableOpacity> */}

                    <View style={{
                        justifyContent: 'flex-end',
                        alignItems: 'flex-end',
                    }}>

                        <Text style={[styles.baseText, { marginTop: 12, marginRight: 24, color: "#000", fontFamily: "Prompt-Regular" }]}>วัน/เดือน/ปี-เวลา:เวลา</Text>

                    </View>

                </View>
            </ScrollView>

        </View >
    );
}

Detail.navigationOptions = ({ navigation }) => ({
    title: 'รายละเอียดใบขอใช้บริการ',
    headerBackTitleVisible: false,
    headerTitleStyle: { color: '#fff', fontFamily: 'Prompt-Bold' },
    headerBackTitleStyle: { color: '#fff', fontFamily: 'Prompt-Bold' },
    headerStyle: { backgroundColor: '#3E5481' },
    headerTintColor: 'white',
})


const styles = StyleSheet.create({
    MainContainer: {
        flex: 1,
        backgroundColor: '#FFFFFF',
    },
    loadding_style: {
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
        backgroundColor: '#fff',
    },
    appleBtn: { height: 44, width: 320, marginTop: 16 },
    spinnerTextStyle: {
        color: '#FFF',
        fontFamily: "NotoSans-Regular",
        fontSize: 14
    },
    button: {
        marginTop: 24,
        backgroundColor: "#e42179",
        width: "100%",
    },
    stretch: {
        width: 200,
        height: 200,
        resizeMode: "stretch",
        justifyContent: 'center',
        alignItems: 'center'
    },
    baseText: {
        fontSize: 18,
    },
    baseTextTwo: {
        fontSize: 22,
    },

});

export default Detail