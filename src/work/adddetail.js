import React, { useState, useCallback, useMemo, useEffect } from 'react';
import { StyleSheet, View, Text, Keyboard, Alert, Image, ActivityIndicator, AsyncStorage, Platform, StatusBar, Linking, ScrollView, TouchableOpacity, TextInput } from 'react-native';
import { Divider, Input, Button, Card } from 'react-native-elements';
import { useNavigation, useNavigationParam } from 'react-navigation-hooks'
import DropDownPicker from 'react-native-dropdown-picker';
import DateTimePicker from "react-native-modal-datetime-picker";
var moment = require('moment-timezone');
import ImagePicker from 'react-native-image-picker';



const options = {
    // title: 'Select Avatar',
    // customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
    quality: 1.0,
    maxWidth: 140,
    maxHeight: 140,
    storageOptions: {
        skipBackup: true,
        path: 'images',
    },
};

const AddDetail = () => {
    const { navigate } = useNavigation();
    var listStatus = [{ StatusText: 'ผ่าน', Status: true }, { StatusText: 'ไม่ผ่าน', Status: false }]
    const [isLoading, setLoading] = useState(false);
    const [isVisibleDateStart, setVisibleDateStart] = useState(false);
    const [isVisibleDateFinish, setVisibleDateFinish] = useState(false);
    const [textDateStart, setDateStartCampaign] = useState(moment().format('LL'));
    const [textDateFinish, setDateFinishCampaign] = useState(moment().format('LL'));
    const [jobId, setJobId] = useState('');
    const [name, setName] = useState('');
    const [position, setPosition] = useState('');
    const [image1, setImage1] = useState('');
    const [image2, setImage2] = useState('');
    const [token, setToken] = useState('');



    const [startDate, setDateStart] = useState(moment(new Date()).format("YYYY-MM-DD"));
    const [endDate, setDateFinish] = useState(moment(new Date()).format("YYYY-MM-DD"));
    const [base64Image1, setBase64Image1] = useState('');
    const [base64Image2, setBase64Image2] = useState('');



    const [jobHour, setJobHour] = useState(0);
    const [personHour, setPersonHour] = useState(0);
    const [machineHour, setMachineHour] = useState(0);
    const [comment, setComment] = useState('');
    const [jobStatus, setJobStatus] = useState(false);


    console.log('job : ', jobHour)



    onChangeTextJobHour = (value) => {
        console.log('job 1 : ', jobHour)
        setJobHour(value)
    }

    onChangeTextPersonHour = (value) => {
        setPersonHour(value)
    }

    onChangeTextMachineHour = (value) => {
        setMachineHour(value)
    }

    onChangeTextComment = (value) => {
        setComment(value)
    }




    chooseImageFile = () => {
        ImagePicker.showImagePicker(options, (response) => {


            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            } else {
                setImage1(response.uri)
                setBase64Image1('data:image/jpeg;base64,' + response.data)
            }
        });
    };


    chooseImageFileTwo = () => {
        ImagePicker.showImagePicker(options, (response) => {


            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            } else {
                setImage2(response.uri)
                setBase64Image2('data:image/jpeg;base64,' + response.data)
            }
        });
    };


    callUpdateJob = async (jobId, startDate, endDate, jobHour, personHour, machineHour, jobStatus, comment, token) => {
        console.log('jobId : ', jobId)
        console.log('startDate : ', startDate)
        console.log('endDate : ', endDate)
        console.log('jobHour : ', jobHour)
        console.log('personHour : ', personHour)
        console.log('machineHour : ', machineHour)
        console.log('jobStatus : ', jobStatus)
        console.log('comment : ', comment)
        console.log('token : ', token)
        setLoading(true)
        const data = {
            "jobId": jobId,
            "startDate": startDate,
            "endDate": endDate,
            "jobHour": jobHour,
            "personHour": personHour,
            "machineHour": machineHour,
            "jobStatus": jobStatus,
            "comment": comment
        }
        axios.post('http://apitpm.bsisugarcane.com/api/RequestServiceApp/updateJob?token=' + token, data)
            .then((response) => {
                setLoading(false)
                navigate('Success')
            })
            .catch((error) => {
            })
            .finally(function () {
            });


    };


    useEffect(() => {
        async function fetchMyAPI() {
            setToken(await AsyncStorage.getItem('token'))
            setJobId(await AsyncStorage.getItem('jobId'))
            setName(await AsyncStorage.getItem('name'))
            setPosition(await AsyncStorage.getItem('position'))
        }
        fetchMyAPI()
    }, [])
    return (
        <View style={{
            flex: 1,
            backgroundColor: "#fff",
            width: "100%",
        }}>
            {isLoading ?
                <Loading
                    visible={true}
                    textContent={'Loading...'}
                    textStyle={styles.spinnerTextStyle}
                />
                :
                <View style={styles.MainContainer}>

                    {/* <View style={{ backgroundColor: "#3E5481", height: 100 }}>

                        <Text style={[styles.baseText, { marginTop: 48, marginLeft: 48, color: "#FFF", fontFamily: "Prompt-Bold", fontSize: 24 }]}>หัวข้อขอใช้บริการ</Text>

                    </View> */}


                    <ScrollView style={{ paddingHorizontal: '2%' }}>

                        <View style={{ margin: 8, marginBottom: 36 }}>

                            <Text style={[styles.baseText, { marginTop: 16, marginLeft: 8, color: "#000", fontFamily: "Prompt-Regular" }]}>หมายเลขใบขอใช้บริการ : {jobId}</Text>

                            <Text style={[styles.baseText, { marginLeft: 8, color: "#000", fontFamily: "Prompt-Regular" }]}>เเผนก : {position}</Text>

                            <Text style={[styles.baseText, { marginLeft: 8, color: "#000", fontFamily: "Prompt-Regular" }]}>ผู้ดำเนินการ : {name}</Text>



                            <Card containerStyle={{ borderRadius: 20, flex: 0.9, marginLeft: 4, marginRight: 4, zIndex: 2 }}>





                                <View style={{
                                    height: 120, justifyContent: 'center',
                                    alignItems: 'center',
                                    flexDirection: "row",
                                }}>
                                    <View style={{ flex: 0.5, flexDirection: "column" }}>
                                        <Text style={[styles.baseText, { marginLeft: 8, fontFamily: "Prompt-Regular", fontSize: 18 }]}>เริ่มงาน</Text>

                                        <View style={styles.containerStartDate}>
                                            <View style={styles.containerDateRight}>
                                                <Button title={textDateStart} titleStyle={styles.TextSmallForButtom} buttonStyle={styles.dateTimePickerButtonStyle} style={{ width: '94%', height: 40 }} onPress={() => {
                                                    setVisibleDateStart(true)
                                                }} />

                                                <DateTimePicker
                                                    textColor="#d5d5d5"
                                                    mode="date"
                                                    locale="th_EN"
                                                    date={new Date()}
                                                    isVisible={isVisibleDateStart}
                                                    onConfirm={(value) => {
                                                        setDateStartCampaign(moment(value).format('LL'))
                                                        setDateStart(moment(value).format("YYYY-MM-DD"))
                                                        setVisibleDateStart(false)
                                                    }}
                                                    onCancel={() => {
                                                        setVisibleDateStart(false)
                                                    }}
                                                />
                                            </View>
                                        </View>

                                    </View>
                                    <View style={{ flex: 0.5 }}>
                                        <Text style={[styles.baseText, { marginLeft: 8, fontFamily: "Prompt-Regular", fontSize: 18 }]}>จบงาน</Text>

                                        <View style={styles.containerStartDate}>
                                            <View style={styles.containerDateRight}>
                                                <Button title={textDateFinish} titleStyle={styles.TextSmallForButtom} buttonStyle={styles.dateTimePickerButtonStyle} style={{ width: '94%' }} onPress={() => {
                                                    setVisibleDateFinish(true)
                                                }} />
                                                <DateTimePicker
                                                    textColor="#d5d5d5"
                                                    mode="date"
                                                    locale="th_EN"
                                                    date={new Date()}
                                                    isVisible={isVisibleDateFinish}
                                                    onConfirm={(value) => {
                                                        console.log('value : ', value)
                                                        setDateFinishCampaign(moment(value).format('LL'))
                                                        setDateFinish(moment(value).format("YYYY-MM-DD"))
                                                        setVisibleDateFinish(false)
                                                    }}
                                                    onCancel={() => {
                                                        console.log('value : ')
                                                        setVisibleDateFinish(false)
                                                    }}
                                                />

                                            </View>
                                        </View>

                                    </View>

                                </View>




                                <View style={{
                                    height: 50, justifyContent: 'center',
                                    alignItems: 'center',
                                    flexDirection: "row",
                                }}>
                                    <View style={{ flex: 0.4, flexDirection: "column" }}>
                                        <Text style={[styles.baseText, { marginLeft: 8, fontFamily: "Prompt-Regular", fontSize: 16 }]}>รวมเวลาให้บริการ</Text>


                                    </View>
                                    <View style={{ flex: 0.4 }}>

                                        <Input
                                            inputStyle={{
                                                paddingLeft: 6, borderWidth: 1,
                                                fontFamily: "Prompt-Regular", fontSize: 16,
                                                borderRadius: 4,
                                                marginTop: 20,
                                                alignSelf: 'flex-end',
                                                borderColor: 'lightgrey'
                                            }}

                                            inputContainerStyle={{ borderBottomWidth: 0 }}
                                            onChangeText={(value) => {
                                                onChangeTextJobHour(value)
                                            }}

                                        >
                                        </Input>

                                    </View>
                                    <View style={{ flex: 0.2 }}>

                                        <Text style={[styles.baseText, { marginLeft: 8, fontFamily: "Prompt-Regular", fontSize: 16 }]}>ชั่วโมง</Text>

                                    </View>

                                </View>

                                <View style={{
                                    height: 50, justifyContent: 'center',
                                    alignItems: 'center',
                                    flexDirection: "row",
                                }}>
                                    <View style={{ flex: 0.4, flexDirection: "column" }}>
                                        <Text style={[styles.baseText, { marginLeft: 8, fontFamily: "Prompt-Regular", fontSize: 16 }]}>จำนวนชั่วโมง(คน)</Text>


                                    </View>
                                    <View style={{ flex: 0.4 }}>

                                        <Input
                                            inputStyle={{
                                                paddingLeft: 6, borderWidth: 1,
                                                fontFamily: "Prompt-Regular", fontSize: 16,
                                                borderRadius: 4,
                                                marginTop: 20,
                                                borderColor: 'lightgrey'
                                            }}

                                            inputContainerStyle={{ borderBottomWidth: 0 }}
                                            onChangeText={(value) => {
                                                onChangeTextPersonHour(value)
                                            }}
                                        >
                                        </Input>

                                    </View>
                                    <View style={{ flex: 0.2 }}>

                                        <Text style={[styles.baseText, { marginLeft: 8, fontFamily: "Prompt-Regular", fontSize: 16 }]}>ชั่วโมง</Text>

                                    </View>

                                </View>


                                <View style={{
                                    height: 50, justifyContent: 'center',
                                    alignItems: 'center',
                                    flexDirection: "row",
                                }}>
                                    <View style={{ flex: 0.4, flexDirection: "column" }}>
                                        <Text style={[styles.baseText, { marginLeft: 8, fontFamily: "Prompt-Regular", fontSize: 12 }]}>จำนวนชั่วโมง(เครื่องจักร)</Text>


                                    </View>
                                    <View style={{ flex: 0.4 }}>

                                        <Input
                                            inputStyle={{
                                                paddingLeft: 6, borderWidth: 1,
                                                fontFamily: "Prompt-Regular", fontSize: 16,
                                                borderRadius: 4,
                                                marginTop: 20,
                                                borderColor: 'lightgrey'
                                            }}

                                            inputContainerStyle={{ borderBottomWidth: 0 }}
                                            onChangeText={(value) => {
                                                onChangeTextMachineHour(value)
                                            }}
                                        >
                                        </Input>

                                    </View>
                                    <View style={{ flex: 0.2 }}>

                                        <Text style={[styles.baseText, { marginLeft: 8, fontFamily: "Prompt-Regular", fontSize: 16 }]}>ชั่วโมง</Text>

                                    </View>

                                </View>


                                <View style={{
                                    height: 50,
                                    flexDirection: "row",
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                }}>
                                    <View style={{ flex: 0.3, flexDirection: "column" }}>
                                        <Text style={[styles.baseText, { marginLeft: 8, fontFamily: "Prompt-Regular", fontSize: 16 }]}>ผลการตรวจ</Text>


                                    </View>
                                    <View style={{ flex: 0.7 }}>

                                        <DropDownPicker
                                            items={
                                                listStatus.map((item, i) => {
                                                    return (
                                                        { label: listStatus[i].StatusText, value: listStatus[i].Status }
                                                    )
                                                })
                                            }
                                            labelStyle={{
                                                fontFamily: "Prompt-Regular",
                                                fontSize: 16
                                            }}
                                            placeholder={'กรุณาเลือก'}

                                            containerStyle={{
                                                height: 40,
                                                marginRight: 8,
                                                marginLeft: 8,
                                                width: 140
                                            }}
                                            style={{ backgroundColor: '#fafafa' }}
                                            itemStyle={{
                                                justifyContent: 'flex-start'
                                            }}
                                            dropDownStyle={{ backgroundColor: '#fafafa', zIndex: 100 }}
                                            onChangeItem={item =>
                                                setJobStatus(item.value)
                                                //  console.log('item : ', item.value)
                                                // callData(item.value, token)
                                            }
                                            showArrow={Platform.OS == "ios" ? true : false}
                                        />

                                    </View>


                                </View>






                            </Card>



                            <Card containerStyle={{ borderRadius: 20, height: 220, marginLeft: 4, marginRight: 4 }}>

                                <Text style={[styles.baseText, { marginLeft: 4, color: "#000", fontFamily: "Prompt-Regular" }]}>รูปภาพ</Text>




                                <View style={{
                                    height: 140, justifyContent: 'center',
                                    alignItems: 'center',
                                    flexDirection: "row",
                                }}>
                                    <View style={{
                                        flex: 0.5, justifyContent: 'center',
                                        alignItems: 'center',
                                    }}>
                                        <TouchableOpacity onPress={() => { chooseImageFile() }}>
                                            {image1 != '' ?


                                                <Image
                                                    style={{ width: 140, height: 140 }}
                                                    source={{ uri: image1 }}
                                                ></Image>

                                                :


                                                <Image

                                                    source={require('../../img/test.png')}
                                                ></Image>
                                            }

                                        </TouchableOpacity>
                                    </View>
                                    <View style={{
                                        flex: 0.5, justifyContent: 'center',
                                        alignItems: 'center',
                                    }}>
                                        <TouchableOpacity onPress={() => { chooseImageFileTwo() }}>
                                            {image2 != '' ?


                                                <Image
                                                    style={{ width: 140, height: 140 }}
                                                    source={{ uri: image2 }}
                                                ></Image>

                                                :
                                                <Image

                                                    source={require('../../img/test.png')}
                                                ></Image>
                                            }
                                        </TouchableOpacity>
                                    </View>

                                </View>





                            </Card>



                            <Card containerStyle={{ borderRadius: 20, flex: 0.9, marginLeft: 4, marginRight: 4 }}>






                                <Text style={[styles.baseText, { marginLeft: 8, fontFamily: "Prompt-Regular" }]}>บันทึกการซ่อม</Text>
                                <TextInput
                                    multiline={true}
                                    style={{
                                        fontSize: 20, marginTop: 12, paddingLeft: 8, height: 120, fontFamily: "Prompt-Regular", textAlignVertical: 'top', borderWidth: 1,
                                        borderRadius: 4,
                                        marginLeft: 10,
                                        marginRight: 10,
                                        borderColor: 'lightgrey'
                                    }}
                                    onChangeText={(value) => {
                                        onChangeTextComment(value)
                                    }}
                                />







                            </Card>



                            <View style={{
                                justifyContent: 'center',
                                alignItems: 'flex-start',
                                flexDirection: "row",
                                flex: 0.9
                            }}>




                                <Button
                                    buttonStyle={{ backgroundColor: "#4DB6AC", height: 50, width: 200, alignSelf: "center", borderRadius: 25, marginTop: 20, borderWidth: 1, borderColor: '#4DB6AC' }}
                                    title="บันทึก"

                                    titleStyle={{ color: "#FFF", fontFamily: "Prompt-Bold", fontSize: 18 }}
                                    onPress={() => {

                                        callUpdateJob(jobId, startDate, endDate, jobHour, personHour, machineHour, jobStatus, comment, token)
                                    }
                                    }>
                                </Button>

                            </View>


                            {/* <TouchableOpacity
                                onPress={() =>
                                    navigate('Detail')
                                }
                            >

                                <View style={{
                                    justifyContent: 'flex-start',
                                    alignItems: 'flex-start',
                                    borderRadius: 20,
                                    marginLeft: 36,
                                    marginTop: 12,
                                    width: 40,
                                    height: 40,
                                    backgroundColor: "#80CBC4",
                                    justifyContent: "center"
                                }}>

                                    <Image
                                        style={{ width: 20, height: 20, alignSelf: "center" }}
                                        source={require('../../img/arrow.png')}
                                    ></Image>


                                </View>

                            </TouchableOpacity> */}

                            <View style={{
                                justifyContent: 'flex-end',
                                alignItems: 'flex-end',
                            }}>

                                <Text style={[styles.baseText, { marginTop: 16, marginRight: 24, color: "#000", fontFamily: "Prompt-Regular" }]}>วัน/เดือน/ปี-เวลา:เวลา</Text>

                            </View>

                        </View>
                    </ScrollView>

                </View >
            }
        </View>
    );
}

AddDetail.navigationOptions = ({ navigation }) => ({
    title: 'หัวข้อขอใช้บริการ',
    headerBackTitleVisible: false,
    headerTitleStyle: { color: '#fff', fontFamily: 'Prompt-Bold' },
    headerBackTitleStyle: { color: '#fff', fontFamily: 'Prompt-Bold' },
    headerStyle: { backgroundColor: '#3E5481' },
    headerTintColor: 'white',
})

const styles = StyleSheet.create({
    MainContainer: {
        flex: 1,
        backgroundColor: '#FFFFFF',
    },
    loadding_style: {
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
        backgroundColor: '#fff',
    },
    appleBtn: { height: 44, width: 320, marginTop: 16 },
    spinnerTextStyle: {
        color: '#FFF',
        fontFamily: "NotoSans-Regular",
        fontSize: 14
    },
    button: {
        marginTop: 24,
        backgroundColor: "#e42179",
        width: "100%",
    },
    stretch: {
        width: 200,
        height: 200,
        resizeMode: "stretch",
        justifyContent: 'center',
        alignItems: 'center'
    },
    baseText: {
        fontSize: 20,
    },
    baseTextTwo: {
        fontSize: 22,
    },
    containerStartDate: {
        flexDirection: 'row',
        justifyContent: 'center',
        marginLeft: 8,
        marginTop: 4
    },
    containerDateRight: {
        flex: 1,
        justifyContent: 'center',
        height: 40
    },
    dateTimePickerButtonStyle: {
        backgroundColor: '#fff',
        borderWidth: 1,
        borderRadius: 4,
        borderColor: 'lightgrey',
        height: 40
    },
    TextSmallForButtom: {
        fontFamily: "Prompt-Regular", fontSize: 16,
        marginTop: -4,
        color: '#000'
    },
    spinnerTextStyle: {
        color: '#FFF',
        fontFamily: "THSarabunNew",
        fontSize: 20
    },
});

export default AddDetail