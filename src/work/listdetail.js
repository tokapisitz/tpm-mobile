import React, { useState, useCallback, useMemo, useEffect } from 'react';
import { StyleSheet, ScrollView, View, Text, Keyboard, Alert, Image, ActivityIndicator, AsyncStorage, Platform, StatusBar, Linking, TouchableOpacity } from 'react-native';
import { Divider, Input, Button, Card, Overlay } from 'react-native-elements';
import { useNavigation, useNavigationParam } from 'react-navigation-hooks'
import DropDownPicker from 'react-native-dropdown-picker';
import axios from 'axios';
import Loading from 'react-native-loading-spinner-overlay';


const ListDetail = () => {
    const { navigate } = useNavigation();
    const [isLoading, setLoading] = useState(false);
    const [listStatus, setListStatus] = useState([]);
    const [listData, setListData] = useState([]);
    const [token, setToken] = useState('');

    const [name, setName] = useState('');
    const [position, setPosition] = useState('');
    const [dialogVisible, setDialogVisible] = useState(false);


    const [topic, setTopic] = useState('');
    const [department, setDepartment] = useState('');
    const [type, setType] = useState('');
    const [piority, setPiority] = useState('');
    const [status, setStatus] = useState('');

    callListStatus = async (token) => {
        setLoading(true)

        console.log('token response : ', token)



        axios.get('http://apitpm.bsisugarcane.com/api/RequestServiceApp/statusList?token=' + token)
            .then((response) => {
                setLoading(false)
                var data = response.data.Results
                var i
                var objectData = {}
                var list = []

                for (i = 0; i < data.length; i++) {
                    objectData = {
                        Status: data[i].Status,
                        StatusText: data[i].StatusText
                    }
                    list.push(objectData)
                }
                console.log('list data : ', list)
                setListStatus(list)
                // callData(status, token)
            })
            .catch((error) => {
                console.log('error response : ', error)
            })
            .finally(function () {
            });

    };


    callData = async (status, token) => {
        setLoading(true)

        axios.get('http://apitpm.bsisugarcane.com/api/RequestServiceApp/jobList?status=' + status + '&token=' + token)
            .then((response) => {
                setLoading(false)
                var data = response.data.Results
                var i
                var objectData = {}
                var list = []

                console.log('data : ', response.data.Results)
                for (i = 0; i < data.length; i++) {
                    objectData = {
                        CreatedDateText: data[i].CreatedDateText,
                        Department: data[i].Department,
                        PriorityRequestService: data[i].PriorityRequestService,
                        ServiceType: data[i].ServiceType,
                        StatusText: data[i].StatusText,
                        Topic: data[i].Topic,
                        Id: data[i].Id
                    }
                    list.push(objectData)
                }
                setListData(list)
            })
            .catch((error) => {
                console.log('error response : ', error)
            })
            .finally(function () {
            });

    };

    useEffect(() => {
        async function fetchMyAPI() {
            setToken(await AsyncStorage.getItem('token'))
            setName(await AsyncStorage.getItem('name'))
            setPosition(await AsyncStorage.getItem('position'))
            callListStatus(await AsyncStorage.getItem('token'))
        }
        fetchMyAPI()
    }, [])


    var popupDetail = (
        <Overlay fullScreen isVisible={dialogVisible} overlayStyle={{ flex: 1, padding: 0 }} width="94%" height="100%" onBackdropPress={() => console.log('yahoo')} >
            <View style={styles.containerObjectiveText}>
                <Text style={Platform.OS == 'ios' ? styles.objectiveIOSTextStyle : styles.objectiveTextStyle}>รายละเอียดข้อมูล</Text>
            </View>
            <ScrollView style={{ paddingHorizontal: '2%' }}>
                <View style={{
                    flexDirection: "column",

                }}>

                    <View style={{
                        flexDirection: "row",
                        marginTop: 24,
                        justifyContent: 'center',
                        alignItems: 'center'

                    }}>
                        <View style={{ flex: 0.5, marginTop: 10, marginBottom: 10 }}>
                            <Text style={[styles.baseText], { textAlign: 'right', fontFamily: "Prompt-Regular", fontSize: 22 }}>งานที่ขอใช้บริการ : </Text>

                        </View>


                        <View style={{ flex: 0.5, marginTop: 10, marginBottom: 10 }}>
                            <Text style={[styles.baseText], { textAlign: 'center', fontFamily: "Prompt-Regular", fontSize: 22 }}>{topic}</Text>
                        </View>
                    </View>

                    <View style={{
                        flexDirection: "row",
                        marginTop: 10,
                        justifyContent: 'center',
                        alignItems: 'center'

                    }}>
                        <View style={{ flex: 0.5, marginTop: 10, marginBottom: 10 }}>
                            <Text style={[styles.baseText], { textAlign: 'right', fontFamily: "Prompt-Regular", fontSize: 22 }}>เเผนก : </Text>

                        </View>

                        <View style={{ flex: 0.5, marginTop: 10, marginBottom: 10 }}>
                            <Text style={[styles.baseText], { textAlign: 'center', fontFamily: "Prompt-Regular", fontSize: 22 }}>{department}</Text>
                        </View>
                    </View>


                    <View style={{
                        flexDirection: "row",
                        marginTop: 10,
                        justifyContent: 'center',
                        alignItems: 'center'

                    }}>
                        <View style={{ flex: 0.5, marginTop: 10, marginBottom: 10 }}>
                            <Text style={[styles.baseText], { textAlign: 'right', fontFamily: "Prompt-Regular", fontSize: 22 }}>ประเภทงาน : </Text>

                        </View>



                        <View style={{ flex: 0.5, marginTop: 10, marginBottom: 10 }}>
                            <Text style={[styles.baseText], { textAlign: 'center', fontFamily: "Prompt-Regular", fontSize: 22 }}>{type}</Text>
                        </View>
                    </View>


                    <View style={{
                        flexDirection: "row",
                        marginTop: 10,
                        justifyContent: 'center',
                        alignItems: 'center'

                    }}>
                        <View style={{ flex: 0.5, marginTop: 10, marginBottom: 10 }}>
                            <Text style={[styles.baseText], { textAlign: 'right', fontFamily: "Prompt-Regular", fontSize: 22 }}>ความเร่งด่วน : </Text>

                        </View>








                        <View style={{ flex: 0.5, marginTop: 10, marginBottom: 10 }}>
                            <Text style={[styles.baseText], { textAlign: 'center', fontFamily: "Prompt-Regular", fontSize: 22 }}>{piority}</Text>
                        </View>
                    </View>


                    <View style={{
                        flexDirection: "row",
                        marginTop: 10,
                        justifyContent: 'center',
                        alignItems: 'center'

                    }}>
                        <View style={{ flex: 0.5, marginTop: 10, marginBottom: 10 }}>
                            <Text style={[styles.baseText], { textAlign: 'right', fontFamily: "Prompt-Regular", fontSize: 22 }}>สถานะ : </Text>

                        </View>








                        <View style={{ flex: 0.5, marginTop: 10, marginBottom: 10 }}>
                            <Text style={[styles.baseText], { textAlign: 'center', fontFamily: "Prompt-Regular", fontSize: 22 }}>{status}</Text>
                        </View>
                    </View>
                </View>



            </ScrollView>

            <Button
                title="ปิด"
                titleStyle={[styles.defaultText, { color: '#000', fontSize: 24 }]}
                buttonStyle={{ backgroundColor: '#B8E8A8', height: 46 }}
                containerStyle={[styles.closeButtonContainer, { justifyContent: 'flex-end' }]}
                onPress={() => {
                    setDialogVisible(false)
                }}
            />
        </Overlay>

    )


    return (
        <View style={styles.MainContainer}>
            {/* <View style={{ backgroundColor: "#3E5481", height: 100 }}>

                <Text style={[styles.baseText, { marginTop: 48, marginLeft: 48, color: "#FFF", fontFamily: "Prompt-Bold", fontSize: 24 }]}>ส่วนผู้ให้บริการ</Text>

            </View> */}




            <Text style={[styles.baseText, { marginTop: 16, marginLeft: 48, color: "#000", fontFamily: "Prompt-Regular" }]}>เเผนก : {position}</Text>

            <Text style={[styles.baseText, { marginLeft: 48, color: "#000", fontFamily: "Prompt-Regular" }]}>ผู้ดำเนินการ : {name}</Text>



            <View style={{ flex: 1 }}>

                <View style={{ zIndex: 2 }}>

                    <View style={{
                        justifyContent: 'flex-start',
                        alignItems: 'flex-start',
                        flexDirection: "row"
                    }}>

                        <Text style={[styles.baseText, { marginTop: 16, marginLeft: 48, color: "#000", fontFamily: "Prompt-Regular" }]}>สถานะ</Text>





                        <DropDownPicker

                            items={
                                listStatus.map((item, i) => {
                                    return (
                                        { label: listStatus[i].StatusText, value: listStatus[i].Status }
                                    )
                                })
                            }
                            labelStyle={{
                                fontFamily: "Prompt-Regular",
                                fontSize: 18
                            }}
                            placeholder={'กรุณาเลือก'}

                            containerStyle={{
                                height: 40, marginRight: 8,
                                marginTop: 12,
                                marginLeft: 12,
                                width: 200
                            }}
                            style={{ backgroundColor: '#fafafa', zIndex: 9 }}
                            itemStyle={{
                                justifyContent: 'flex-start'
                            }}
                            dropDownStyle={{ backgroundColor: '#fafafa' }}
                            onChangeItem={item =>
                                //  console.log('data : ', item.value)

                                // setStatus(item.value)
                                callData(item.value, token)
                            }
                            showArrow={Platform.OS == "ios" ? true : false}
                        />

                    </View>
                </View>




                <View style={{
                    flexDirection: "row",
                    backgroundColor: "#e0e0e0",
                    marginTop: 20,
                    height: 50,
                    marginLeft: 24,
                    marginRight: 24,
                    justifyContent: 'center',
                    alignItems: 'center'

                }}>
                    <View style={{ flex: 0.2, }}>
                        <Text style={[styles.baseText], { marginLeft: 12, fontFamily: "Prompt-Regular", }}></Text>

                    </View>
                    <View style={{ flex: 0.1, }}>
                        <Text style={[styles.baseText], { marginLeft: 12, fontFamily: "Prompt-Regular", }}>#</Text>

                    </View>




                    <View style={{ flex: 0.4, }}>
                        <Text style={[styles.baseText], { marginLeft: 24, fontFamily: "Prompt-Regular" }}>วันที่ขอ</Text>

                    </View>



                    <View style={{ flex: 0.3, }}>
                        <Text style={[styles.baseText], { textAlign: 'center', fontFamily: "Prompt-Regular", }}>จัดการ</Text>

                    </View>


                </View>


                {

                    listData.map((item, i) => {
                        return (
                            <View style={{
                                flexDirection: "row",
                                height: 46,
                                marginLeft: 24,
                                marginRight: 24,
                                justifyContent: 'center',
                                alignItems: 'center'

                            }}>

                                <View style={{ flex: 0.2, }}>
                                    <TouchableOpacity
                                        onPress={() => {
                                            setTopic(item.Topic)
                                            setDepartment(item.Department)
                                            setType(item.ServiceType)
                                            setPiority(item.PriorityRequestService)
                                            setStatus(item.StatusText)
                                            setDialogVisible(true)
                                        }
                                        }
                                    >
                                        <Image
                                            style={{ width: 20, height: 12, alignSelf: "center" }}
                                            source={require('../../img/polygon.png')}
                                        ></Image>
                                    </TouchableOpacity>

                                </View>
                                <View style={{ flex: 0.1, }}>
                                    <Text style={[styles.baseText], { marginLeft: 12 }}>{i + 1}</Text>

                                </View>




                                <View style={{ flex: 0.4, }}>
                                    <Text style={[styles.baseText], { marginLeft: 24 }}>{item.CreatedDateText.substring(0, 10)}</Text>

                                </View>



                                <View style={{ flex: 0.3, }}>
                                    <Button
                                        title="รายละเอียด"
                                        titleStyle={{ color: '#000', fontSize: 18, paddingBottom: 23, fontFamily: "THSarabunNew" }}
                                        buttonStyle={{ backgroundColor: '#C4C4C4', height: 30, width: 80, borderRadius: 30 }}
                                        containerStyle={{ alignItems: 'center' }}
                                        onPress={() => {
                                            AsyncStorage.setItem('jobId', item.Id.toString());
                                            navigate('Detail')
                                        }}
                                    />

                                </View>
                            </View>

                        )
                    })
                }


                {/* <View style={{
                        justifyContent: 'flex-start',
                        alignItems: 'flex-start',
                        flexDirection: "row",
                        flex: 0.9
                    }}>

                    </View> */}


            </View>


            <View style={{
                justifyContent: 'flex-end',
                alignItems: 'flex-end',
            }}>

                <Text style={[styles.baseText, { marginTop: 12, marginBottom: 24, marginRight: 24, color: "#000", fontFamily: "Prompt-Regular" }]}>วัน/เดือน/ปี-เวลา:เวลา</Text>

            </View>

            {popupDetail}

        </View>
    );
}


ListDetail.navigationOptions = ({ navigation }) => ({
    title: 'ส่วนผู้ให้บริการ',
    headerBackTitleVisible: false,
    headerTitleStyle: { color: '#fff', fontFamily: 'Prompt-Bold' },
    headerBackTitleStyle: { color: '#fff', fontFamily: 'Prompt-Bold' },
    headerStyle: { backgroundColor: '#3E5481' },
    headerTintColor: 'white',
})

const styles = StyleSheet.create({
    MainContainer: {
        flex: 1,
        backgroundColor: '#FFF',
    },
    loadding_style: {
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
        backgroundColor: '#fff',
    },
    appleBtn: { height: 44, width: 320, marginTop: 16 },
    spinnerTextStyle: {
        color: '#FFF',
        fontFamily: "NotoSans-Regular",
        fontSize: 14
    },
    button: {
        marginTop: 24,
        backgroundColor: "#e42179",
        width: "100%",
    },
    stretch: {
        width: 200,
        height: 200,
        resizeMode: "stretch",
        justifyContent: 'center',
        alignItems: 'center'
    },
    baseText: {
        fontSize: 20,
    },
    baseTextTwo: {
        fontSize: 16,
    },
    containerObjectiveText: {
        height: 100,
        backgroundColor: '#F28F8F',
        justifyContent: 'center'
    },
    objectiveIOSTextStyle: {
        fontFamily: 'Prompt-Regular',
        textAlign: 'center',
        marginTop: 44,
        fontSize: 22,
        color: '#fff'
    },
    objectiveTextStyle: {
        fontFamily: 'Prompt-Regular',
        textAlign: 'center',
        fontSize: 22,
        color: '#fff'
    },
    closeButtonContainer: {
        paddingBottom: '12%',
        paddingHorizontal: '2%',
        flexDirection: 'column-reverse',
        paddingTop: 4,
        marginTop: 20
    },
    defaultText: {
        fontFamily: 'THSarabunNew',
        color: '#000'
    },

});

export default ListDetail