import React, { useState, useCallback, useMemo } from 'react';
import { StyleSheet, View, Text, Keyboard, Alert, Image, ActivityIndicator, AsyncStorage, Platform, StatusBar, Linking, TouchableOpacity } from 'react-native';
import { Divider, Input, Button, Card } from 'react-native-elements';
import { useNavigation, useNavigationParam } from 'react-navigation-hooks'


const Add = () => {
    const { navigate } = useNavigation();
    return (
        <View style={styles.MainContainer}>
            {/* <View style={{ backgroundColor: "#3E5481", height: 100 }}>

                <Text style={[styles.baseText, { marginTop: 48, marginLeft: 48, color: "#FFF", fontFamily: "Prompt-Bold", fontSize: 24 }]}>ชื่อเครื่องจักร</Text>

            </View> */}

            <View style={{ flex: 1 }}>

                <Text style={[styles.baseText, { marginTop: 24, marginLeft: 48, color: "#000", fontFamily: "Prompt-Regular" }]}>เลขทะเบียนเครื่องจักร</Text>

                <Text style={[styles.baseText, { marginTop: 6, marginLeft: 48, color: "#000", fontFamily: "Prompt-Regular" }]}>เเผนก XXXX</Text>


                <Card containerStyle={{ borderRadius: 20, height: 280, marginLeft: 32, marginRight: 32 }}>

                    <Text style={[styles.baseText, { marginLeft: 12, color: "#000", fontFamily: "Prompt-Regular" }]}>รูปภาพ</Text>

                    <View style={{
                        flexDirection: "row",
                        height: 100
                    }}>
                        <View style={{ flex: 0.5 }}>
                            <View style={{
                                height: 140, justifyContent: 'center',
                                alignItems: 'center',
                            }}>
                                <Image

                                    source={require('../../img/camera_color.png')}
                                ></Image>

                            </View>
                        </View>
                        <View style={{ flex: 0.5 }}>
                            <View style={{
                                height: 140, justifyContent: 'center',
                                alignItems: 'center',
                            }}>
                                <Image

                                    source={require('../../img/camera_color.png')}
                                ></Image>

                            </View>
                        </View>
                    </View>

                    <View style={{
                        flexDirection: "row",
                        height: 100
                    }}>
                        <View style={{ flex: 0.5 }}>
                            <View style={{
                                height: 140, justifyContent: 'center',
                                alignItems: 'center'
                            }}>
                                <Image

                                    source={require('../../img/camera_color.png')}
                                ></Image>

                            </View>
                        </View>
                        <View style={{ flex: 0.5 }}>
                            <View style={{
                                height: 140, justifyContent: 'center',
                                alignItems: 'center'
                            }}>
                                <Image

                                    source={require('../../img/camera_color.png')}
                                ></Image>

                            </View>
                        </View>
                    </View>

                </Card>


                <Text style={[styles.baseText, { marginTop: 12, marginLeft: 48, color: "#000", fontFamily: "Prompt-Regular" }]}>ส่วนประกอบ</Text>




                <Input
                    inputStyle={[styles.defaultText, { borderRadius: 20, fontSize: 16, paddingLeft: 4, height: 150, marginLeft: 32, marginRight: 32, borderColor: "#E0DFDF", marginTop: 4, borderWidth: 1 }]}
                    inputContainerStyle={{ borderBottomWidth: 0 }}
                />


                <View style={{
                    flexDirection: "row"
                }}>
                    <View style={{ flex: 1 }}>
                        <Button
                            buttonStyle={{ backgroundColor: "#4DB6AC", height: 50, width: 200, alignSelf: "center", borderRadius: 25, borderWidth: 1, borderColor: '#4DB6AC' }}
                            title="บันทึก"
                            titleStyle={{ color: "#FFF", fontFamily: "Prompt-Bold", fontSize: 18 }}
                            onPress={() =>
                                navigate('Success')

                            }>
                        </Button>

                    </View>

                </View>

            </View>

            <View style={{
                justifyContent: 'flex-end',
                alignItems: 'flex-end',
            }}>

                <Text style={[styles.baseText, { marginTop: 12, marginBottom: 24, marginRight: 24, color: "#000", fontFamily: "Prompt-Regular" }]}>วัน/เดือน/ปี-เวลา:เวลา</Text>

            </View>

        </View>
    );
}

Add.navigationOptions = ({ navigation }) => ({
    title: 'ชื่อเครื่องจักร',
    headerBackTitleVisible: false,
    headerTitleStyle: { color: '#fff', fontFamily: 'Prompt-Bold' },
    headerBackTitleStyle: { color: '#fff', fontFamily: 'Prompt-Bold' },
    headerStyle: { backgroundColor: '#3E5481' },
    headerTintColor: 'white',
})

const styles = StyleSheet.create({
    MainContainer: {
        flex: 1,
        backgroundColor: '#FFF',
    },
    loadding_style: {
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
        backgroundColor: '#fff',
    },
    appleBtn: { height: 44, width: 320, marginTop: 16 },
    spinnerTextStyle: {
        color: '#FFF',
        fontFamily: "NotoSans-Regular",
        fontSize: 14
    },
    button: {
        marginTop: 24,
        backgroundColor: "#e42179",
        width: "100%",
    },
    stretch: {
        width: 200,
        height: 200,
        resizeMode: "stretch",
        justifyContent: 'center',
        alignItems: 'center'
    },
    baseText: {
        fontSize: 20,
    },
    baseTextTwo: {
        fontSize: 15,
    },

});

export default Add