import React, { useState, useCallback, useMemo } from 'react';
import { StyleSheet, View, Text, Keyboard, Alert, Image, ActivityIndicator, AsyncStorage, Platform, StatusBar, Linking, TouchableOpacity } from 'react-native';
import { Button, Input } from "react-native-elements";
import { useNavigation, useNavigationParam } from 'react-navigation-hooks'
import Ionicons from "react-native-vector-icons/FontAwesome5";


const Choose = ({ navigation }) => {
    const { navigate } = useNavigation();
    return (
        <View style={styles.MainContainer}>
            <StatusBar barStyle="light-content" />
            {/* <View style={{
                flex: 0.4, justifyContent: 'center',
                alignItems: 'center',
            }} >
                <Image
                    style={styles.stretch}
                    source={require('../img/logo.png')}
                ></Image>
            </View> */}
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }} >
                <Image
                    style={styles.stretch}
                    source={require('../img/lin.png')}
                ></Image>

                <Text style={[styles.baseText, { color: "#2E3E5C", fontFamily: "Prompt-Bold", fontSize: 22, marginTop: 48 }]}>Welcome TPM</Text>
                <Button
                    buttonStyle={{ backgroundColor: "#00598B", height: 60, width: 360, alignSelf: "center", marginTop: 36, borderRadius: 30, borderWidth: 1, borderColor: '#00598B' }}
                    title="ตรวจเช็คเครื่องจักร"
                    titleStyle={{ color: '#fff', fontSize: 18, fontFamily: "Prompt-Bold" }}
                    onPress={() =>
                        console.log('1')
                        //  

                    }>
                </Button>


                <Button
                    buttonStyle={{ backgroundColor: "#563860", height: 60, width: 360, alignSelf: "center", marginTop: 24, borderRadius: 30, borderWidth: 1, borderColor: '#563860' }}
                    title="บันทึกการซ่อม"
                    titleStyle={{ color: '#fff', fontSize: 18, fontFamily: "Prompt-Bold" }}
                    onPress={() => {

                        navigate('Barcode')
                    }
                        //console.log('2')
                        // 
                        //  

                    }>
                </Button>





                <Button
                    buttonStyle={{ backgroundColor: "#8C8A8A", height: 60, width: 360, alignSelf: "center", marginTop: 24, borderRadius: 30, borderWidth: 1, borderColor: '#8C8A8A' }}
                    title="บันทึกใบขอชำระบริการ"
                    titleStyle={{ color: '#fff', fontSize: 18, fontFamily: "Prompt-Bold" }}
                    onPress={() =>

                        navigate('ListDetail')

                    }>
                </Button>
            </View>




        </View>
    );
}

Choose.navigationOptions = ({ navigation }) => ({
    title: 'เลือกเมนู',
    headerBackTitleVisible: false,
    headerTitleStyle: { color: '#fff', fontFamily: 'Prompt-Bold' },
    headerBackTitleStyle: { color: '#fff', fontFamily: 'Prompt-Bold' },
    headerStyle: { backgroundColor: '#3E5481' },
    headerTintColor: 'white',
})

const styles = StyleSheet.create({
    MainContainer: {
        justifyContent: 'center',
        flex: 1,
        backgroundColor: '#FFF',
        flexDirection: "column",
    },
    loadding_style: {
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
        backgroundColor: '#fff',
    },
    appleBtn: { height: 44, width: 320, marginTop: 16 },
    spinnerTextStyle: {
        color: '#FFF',
        fontFamily: "NotoSans-Regular",
        fontSize: 14
    },
    button: {
        marginTop: 24,
        backgroundColor: "#e42179",
        width: "100%",
    },
    stretch: {
        resizeMode: "stretch",
        justifyContent: 'center',
        alignItems: 'center'
    },
    defaultText: {
        fontSize: 16,
        color: '#000'
    },

});

export default Choose