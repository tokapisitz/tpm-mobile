import React, { useState, useCallback, useMemo, useEffect } from 'react';
import { StyleSheet, View, Text, Keyboard, Alert, ScrollView, Image, ActivityIndicator, AsyncStorage, Platform, StatusBar, Linking, TouchableOpacity, TextInput } from 'react-native';
import { Button, Input } from "react-native-elements";
import { useNavigation, useNavigationParam } from 'react-navigation-hooks'
import Ionicons from "react-native-vector-icons/FontAwesome5";
import axios from 'axios';

const History = () => {
    const { navigate } = useNavigation();
    const [name, setTopic] = useState('');
    const [detail, setDetail] = useState('');
    const [countPerson, setCountPerson] = useState(0);
    const [countWork, setCountWork] = useState(0);


    const [image_one, setImageOne] = useState(0);
    const [image_two, setImageTwo] = useState(0);
    const [image_three, setImageThree] = useState(0);
    const [image_four, setImageFour] = useState(0);

    const [partMachine, setPartMachine] = useState('');
    const [changePart, setChangePart] = useState(0);
    const [listAsset, setListAsset] = useState([]);

    const [open_asset, setOpenAsset] = useState(0);
    const [open_machine, setOpenMachine] = useState(0);
    const [listAssetResult, setListAssetResult] = useState([]);
    useEffect(() => {
        async function fetchMyAPI() {
            var result_id = await AsyncStorage.getItem('result_id')
            callGetRepairList(await AsyncStorage.getItem('token'), parseInt(await AsyncStorage.getItem('repair_type_id')), await AsyncStorage.getItem('repair_type_list'), result_id, parseInt(await AsyncStorage.getItem('detail_id')))
            // setName(await AsyncStorage.getItem('repair_type_name'))
            // setNameTwo(await AsyncStorage.getItem('repair_type_head_2'))
            // setType(await AsyncStorage.getItem('repair_type_list'))
        }
        fetchMyAPI()
    }, [])



    onOpenDropDownAsset = (count) => {
        var count_open = count
        count_open += 1
        setOpenAsset(count_open)
    }


    onOpenDropDownMachine = (count) => {

        var count_open = count
        count_open += 1
        setOpenMachine(count_open)
    }

    callGetRepairList = async (token, id, type, result_id, detail_id) => {
        var type_result = ""
        if (type == 'null') {
            type_result = '1'
        } else {
            type_result = '0'
        }

        console.log('test 1 : ', 'http://apitpm.bsisugarcane.com/api/MaintenanceApp/api/MaintenanceApp/GetRepairList?token=' + token + '&MachineUnitID=' + result_id + '&status=' + type_result)

        axios.get('http://apitpm.bsisugarcane.com/api/MaintenanceApp/api/MaintenanceApp/GetRepairList?token=' + token + '&MachineUnitID=' + result_id + '&status=' + type_result)
            .then((response) => {

                var list = []
                var objectData = {}
                var i = 0
                var j = 0
                var index = 0
                var k = 0



                for (j = 0; j < response.data.Results.length; j++) {
                    if (response.data.Results[j].Id == id) {
                        index = j
                    }
                }

                for (i = 0; i < response.data.Results[index].RepairHistoryLogs.length; i++) {
                    if (response.data.Results[index].RepairHistoryLogs[i].Id == detail_id) {
                        setTopic(response.data.Results[index].RepairHistoryLogs[i].Topic)
                        setDetail(response.data.Results[index].RepairHistoryLogs[i].Detail)
                        setCountPerson(response.data.Results[index].RepairHistoryLogs[i].AmountPerson)
                        setCountWork(response.data.Results[index].RepairHistoryLogs[i].Hours)

                        var imageOne = ""
                        if (response.data.Results[index].RepairHistoryLogs[i].Image1 == null) {
                            imageOne = ""
                        } else {
                            imageOne = response.data.Results[index].RepairHistoryLogs[i].Image1
                        }
                        setImageOne(imageOne)

                        var imageTwo = ""
                        if (response.data.Results[index].RepairHistoryLogs[i].Image2 == null) {
                            imageTwo = ""
                        } else {
                            imageTwo = response.data.Results[index].RepairHistoryLogs[i].Image2
                        }
                        setImageTwo(imageTwo)

                        var imageThree = ""
                        if (response.data.Results[index].RepairHistoryLogs[i].Image3 == null) {
                            imageThree = ""
                        } else {
                            imageThree = response.data.Results[index].RepairHistoryLogs[i].Image3
                        }
                        setImageThree(imageThree)

                        var imageFour = ""
                        if (response.data.Results[index].RepairHistoryLogs[i].Image4 == null) {
                            imageFour = ""
                        } else {
                            imageFour = response.data.Results[index].RepairHistoryLogs[i].Image4
                        }
                        setImageFour(imageFour)

                        setPartMachine(response.data.Results[index].RepairHistoryLogs[i].PartMachine)
                        setChangePart(response.data.Results[index].RepairHistoryLogs[i].ChangePart)

                        for (k = 0; k < response.data.Results[index].RepairHistoryLogs[i].IsWastingAssetLogs.length; k++) {
                            objectData = {
                                Name: response.data.Results[index].RepairHistoryLogs[i].IsWastingAssetLogs[k].Name,
                                Amount: response.data.Results[index].RepairHistoryLogs[i].IsWastingAssetLogs[k].Amount,
                                Id: response.data.Results[index].RepairHistoryLogs[i].IsWastingAssetLogs[k].WastingAssetID,
                            }
                            list.push(objectData)
                        }

                    }
                }

                setListAssetResult(list)
                callGetWastingAssetList(token)
            })
            .catch((error) => {
                console.log('response error : ', error)
            })
            .finally(function () {
            });

    };

    callGetWastingAssetList = async (token) => {
        axios.get('http://apitpm.bsisugarcane.com/api/MaintenanceApp/api/MaintenanceApp/GetWastingAssetList?token=' + token)
            .then((response) => {


                var list = []
                var objectData = {}
                var i = 0
                var l = 0
                console.log('response first 1234 : ', response.data.Results)
                for (i = 0; i < response.data.Results.length; i++) {
                    for (l = 0; l < listAssetResult.length; l++) {
                        if (response.data.Results[i].Id == listAssetResult[l].Id) {

                            objectData = {
                                Name: response.data.Results[i].Name,
                                Amount: listAssetResult[l].Amount,
                            }
                            list.push(objectData)

                        }
                    }
                }

                console.log('name : ', list)
                setListAsset(list)
            })
            .catch((error) => {
                //  console.log('error first 1234 : ', error)
            })
            .finally(function () {
            });

    };

    return (
        <View style={styles.MainContainer}>

            <ScrollView style={{ marginBottom: 48 }}>

                <View style={{ flex: 1, flexDirection: 'column' }} >


                    <Text style={[styles.baseText, { color: "#3E5481", fontFamily: "Prompt-Bold", fontSize: 36, marginTop: 12, marginLeft: 24 }]}>{name}</Text>

                    <View style={{
                        flexDirection: "row",
                        height: 80,
                        marginTop: 36,
                        justifyContent: 'center', alignItems: 'center'
                    }}>
                        <View style={{ flex: 0.4 }}>
                            {image_one == '' ?
                                <View style={{
                                    height: 140, justifyContent: 'center',
                                    alignItems: 'center',
                                    backgroundColor: '#EEF1F6',
                                    borderRadius: 20,
                                    marginRight: 12
                                }}>
                                    <Image
                                        style={{ width: 60, height: 50 }}
                                        source={require('../../img/camera_grey.png')}
                                    ></Image>

                                    <Text style={{ color: "#9FA5C0", fontFamily: "Prompt-Regular", fontSize: 15, marginLeft: 18, marginRight: 18, marginTop: 12 }}>ไม่มีรูปภาพ</Text>

                                </View>


                                :
                                <View style={{
                                    height: 140, justifyContent: 'center',
                                    alignItems: 'center',
                                    marginRight: 12
                                }}>
                                    <Image
                                        style={styles.imageShow}
                                        source={{ uri: image_one }}
                                    ></Image>

                                </View>
                            }
                        </View>
                        <View style={{ flex: 0.4 }}>
                            {image_two == '' ?
                                <View style={{
                                    height: 140, justifyContent: 'center',
                                    alignItems: 'center',
                                    backgroundColor: '#EEF1F6',
                                    borderRadius: 20,
                                    marginLeft: 12
                                }}>
                                    <Image
                                        style={{ width: 60, height: 50 }}
                                        source={require('../../img/camera_grey.png')}
                                    ></Image>

                                    <Text style={{ color: "#9FA5C0", fontFamily: "Prompt-Regular", fontSize: 15, marginLeft: 18, marginRight: 18, marginTop: 12 }}>ไม่มีรูปภาพ</Text>
                                </View>
                                :
                                <View style={{
                                    height: 140, justifyContent: 'center',
                                    alignItems: 'center',
                                    borderRadius: 20,
                                    marginRight: 12
                                }}>
                                    <Image
                                        style={styles.imageShow}
                                        source={{ uri: image_two }}
                                    ></Image>

                                </View>
                            }
                        </View>
                    </View>

                    <View style={{
                        flexDirection: "row",
                        height: 80,
                        marginTop: 72,
                        justifyContent: 'center', alignItems: 'center'
                    }}>
                        <View style={{ flex: 0.4 }}>
                            {image_three == '' ?
                                <View style={{
                                    height: 140, justifyContent: 'center',
                                    alignItems: 'center',
                                    backgroundColor: '#EEF1F6',
                                    borderRadius: 20,
                                    marginRight: 12
                                }}>
                                    <Image
                                        style={{ width: 60, height: 50 }}
                                        source={require('../../img/camera_grey.png')}
                                    ></Image>

                                    <Text style={{ color: "#9FA5C0", fontFamily: "Prompt-Regular", fontSize: 15, marginLeft: 18, marginRight: 18, marginTop: 12 }}>ไม่มีรูปภาพ</Text>

                                </View>
                                :
                                <View style={{
                                    height: 140, justifyContent: 'center',
                                    alignItems: 'center',
                                    borderRadius: 20,
                                    marginRight: 12
                                }}>
                                    <Image
                                        style={styles.imageShow}
                                        source={{ uri: image_three }}
                                    ></Image>

                                </View>

                            }
                        </View>
                        <View style={{ flex: 0.4 }}>
                            {image_four == '' ?
                                <View style={{
                                    height: 140, justifyContent: 'center',
                                    alignItems: 'center',
                                    backgroundColor: '#EEF1F6',
                                    borderRadius: 20,
                                    marginLeft: 12
                                }}>
                                    <Image
                                        style={{ width: 60, height: 50 }}
                                        source={require('../../img/camera_grey.png')}
                                    ></Image>

                                    <Text style={{ color: "#9FA5C0", fontFamily: "Prompt-Regular", fontSize: 15, marginLeft: 18, marginRight: 18, marginTop: 12 }}>ไม่มีรูปภาพ</Text>

                                </View>
                                :

                                <View style={{
                                    height: 140, justifyContent: 'center',
                                    alignItems: 'center',
                                    borderRadius: 20,
                                    marginRight: 12
                                }}>
                                    <Image
                                        style={styles.imageShow}
                                        source={{ uri: image_four }}
                                    ></Image>



                                </View>

                            }
                        </View>
                    </View>






                    <View style={{ flex: 1, flexDirection: 'column' }} >
                        <View style={{ flexDirection: 'row', borderRadius: 10, height: 50, marginTop: 48 }} >



                            <View style={{ flex: 0.85, flexDirection: 'row', alignItems: 'center' }} >

                                <Text style={{ color: "#3E5481", fontFamily: "Prompt-Bold", fontSize: 20, marginLeft: 24 }}>เปลี่ยนส่วนประกอบเครื่องจักร</Text>
                            </View>

                            <View style={{ flex: 0.15, flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }} >


                                {open_machine % 2 == 0 ?
                                    <TouchableOpacity
                                        onPress={() => onOpenDropDownMachine(open_machine)
                                        } >
                                        <Image

                                            source={require('../../img/arrow_up.png')}
                                        ></Image>
                                    </TouchableOpacity>

                                    :

                                    <TouchableOpacity
                                        onPress={() => onOpenDropDownMachine(open_machine)
                                        } >
                                        <Image

                                            source={require('../../img/arrow_down.png')}
                                        ></Image>
                                    </TouchableOpacity>
                                }


                            </View>

                        </View>




                        {open_machine % 2 == 0 ?
                            <View>
                            </View>
                            :
                            partMachine == "-" || partMachine == '' ?
                                <View>
                                </View>
                                :
                                <View style={{ flexDirection: 'row', marginLeft: 24, marginRight: 24 }} >

                                    <View style={{ flex: 0.8, flexDirection: 'row', alignItems: 'center' }} >
                                        <Text style={{ color: "#3E5481", fontFamily: "Prompt-Regular", fontSize: 18, marginLeft: 24 }}>{partMachine}</Text>

                                    </View>





                                    {/* <View style={{ flex: 0.15, flexDirection: 'row', alignItems: 'center' }} >
    
    
    
                                    <Text style={{ color: "#3E5481", fontFamily: "Prompt-Regular", fontSize: 20 }}>{changePart}</Text>
    
    
                                </View> */}






                                    <View style={{ flex: 0.1, flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end' }} >

                                        <Text style={{ color: "#3E5481", fontFamily: "Prompt-Regular", fontSize: 20, marginRight: 4 }}>{changePart}</Text>

                                    </View>


                                    <View style={{ flex: 0.1, flexDirection: 'row', alignItems: 'center' }} >
                                    </View>


                                </View>

                        }
                    </View>








                    {/* เปลี่ยนชิ้นส่วนเครื่องจักร */}

                    <View style={{ flex: 1, flexDirection: 'column' }} >
                        <View style={{ flexDirection: 'row', borderRadius: 10, height: 50 }} >



                            <View style={{ flex: 0.85, flexDirection: 'row', alignItems: 'center' }} >

                                <Text style={{ color: "#3E5481", fontFamily: "Prompt-Bold", fontSize: 20, marginLeft: 24 }}>ชิ้นส่วนเครื่องจักร</Text>
                            </View>

                            <View style={{ flex: 0.15, flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }} >

                                {open_asset % 2 == 0 ?
                                    <TouchableOpacity
                                        onPress={() => onOpenDropDownAsset(open_asset)
                                        } >
                                        <Image

                                            source={require('../../img/arrow_up.png')}
                                        ></Image>
                                    </TouchableOpacity>
                                    :
                                    <TouchableOpacity
                                        onPress={() => onOpenDropDownAsset(open_asset)
                                        } >
                                        <Image

                                            source={require('../../img/arrow_down.png')}
                                        ></Image>
                                    </TouchableOpacity>

                                }

                            </View>


                        </View>







                        {open_asset % 2 == 0 ?
                            <View>
                            </View>

                            :

                            <View>
                                {listAsset.map((item, index) => {
                                    return (



                                        <View style={{ flexDirection: 'row', marginLeft: 24, marginRight: 24, marginBottom: 8 }} >





                                            <View style={{ flex: 0.8, flexDirection: 'row', alignItems: 'center' }} >
                                                <Text style={{ color: "#3E5481", fontFamily: "Prompt-Regular", fontSize: 18, marginLeft: 24 }}>{item.Name}</Text>

                                            </View>




                                            <View style={{ flex: 0.1, flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end' }} >

                                                <Text style={{ color: "#3E5481", fontFamily: "Prompt-Regular", fontSize: 20, marginRight: 4 }}>{item.Amount}</Text>

                                            </View>


                                            <View style={{ flex: 0.1, flexDirection: 'row', alignItems: 'center' }} >
                                            </View>







                                        </View>
                                    )
                                })
                                }

                            </View>

                        }


                    </View>











                    <View style={{ height: 1, backgroundColor: "#EFEFEF", width: '100%', marginTop: 6 }} >

                    </View>


                    <View style={{ flexDirection: 'row', height: 50, marginLeft: 24, marginRight: 24, marginTop: 6 }} >

                        <View style={{ flex: 0.8, flexDirection: 'row', alignItems: 'center' }} >
                            <Text style={{ color: "#3E5481", fontFamily: "Prompt-Regular", fontSize: 18, marginLeft: 24 }}>จำนวนคน</Text>

                        </View>





                        <View style={{ flex: 0.1, flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end' }} >
                            {/* <Image

                                source={require('../../img/reduce.png')}
                            ></Image> */}


                            <Text style={{ color: "#3E5481", fontFamily: "Prompt-Regular", fontSize: 20, marginRight: 4 }}>{countPerson}</Text>

                            {/* <Image

                                source={require('../../img/increase.png')}
                            ></Image> */}

                        </View>

                        <View style={{ flex: 0.1, flexDirection: 'row', alignItems: 'center' }} >

                            <Text style={{ color: "#3E5481", fontFamily: "Prompt-Regular", fontSize: 20 }}> คน.</Text>
                        </View>


                    </View>


                    <View style={{ height: 1, backgroundColor: "#EFEFEF", width: '100%', marginTop: 6 }} >

                    </View>


                    <View style={{ flexDirection: 'row', height: 50, marginLeft: 24, marginRight: 24, marginTop: 6 }} >

                        <View style={{ flex: 0.8, flexDirection: 'row', alignItems: 'center' }} >
                            <Text style={{ color: "#3E5481", fontFamily: "Prompt-Regular", fontSize: 18, marginLeft: 24 }}>ชั่วโมงการซ่อมบำรุง</Text>

                        </View>





                        <View style={{ flex: 0.1, flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end' }} >

                            <Text style={{ color: "#3E5481", fontFamily: "Prompt-Regular", fontSize: 20, marginRight: 4 }}>{countWork}</Text>

                        </View>


                        <View style={{ flex: 0.1, flexDirection: 'row', alignItems: 'center' }} >
                            <Text style={{ color: "#3E5481", fontFamily: "Prompt-Regular", fontSize: 20 }}> ชม.</Text>
                        </View>


                    </View>


                    <View style={{ height: 1, backgroundColor: "#EFEFEF", width: '100%', marginTop: 6 }} >

                    </View>


                    <Text style={{
                        color: "#3E5481", fontFamily: "Prompt-Bold", fontSize: 20, marginTop: 12, marginLeft: 24,
                        marginRight: 24,
                    }}>รายละเอียด</Text>


                    <Text style={{
                        color: "#9FA5C0", fontFamily: "Prompt-Regular", fontSize: 20, marginTop: 12, marginLeft: 24,
                        marginRight: 24,
                    }}>{detail}</Text>

                    {/* <TextInput
                        multiline={true}
                        style={{
                            fontSize: 18, marginTop: 12, paddingLeft: 8, height: 100, fontFamily: "Prompt-Regular", textAlignVertical: 'top', borderWidth: 1,
                            borderRadius: 12,
                            borderColor: '#D0DBEA',
                            marginLeft: 24,
                            marginRight: 24,
                            fontFamily: "Prompt-Regular",
                            color: '#3E5481'
                        }}
                        value={detail}
                        editable={false}
                        selectTextOnFocus={false}
                        onChange={(value) => {

                        }}
                    /> */}

                </View>


            </ScrollView>

        </View>



    );
}

History.navigationOptions = ({ navigation }) => ({
    title: 'ประวัติการทำงาน',
    headerBackTitleVisible: false,
    headerTitleStyle: { color: '#fff', fontFamily: 'Prompt-Bold' },
    headerBackTitleStyle: { color: '#fff', fontFamily: 'Prompt-Bold' },
    headerStyle: { backgroundColor: '#3E5481' },
    headerTintColor: 'white',
})

const styles = StyleSheet.create({
    MainContainer: {
        justifyContent: 'center',
        flex: 1,
        backgroundColor: '#FFF',
        flexDirection: "column",
    },
    loadding_style: {
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
        backgroundColor: '#fff',
    },
    appleBtn: { height: 44, width: 320, marginTop: 16 },
    spinnerTextStyle: {
        color: '#FFF',
        fontFamily: "NotoSans-Regular",
        fontSize: 14
    },
    button: {
        marginTop: 24,
        backgroundColor: "#e42179",
        width: "100%",
    },
    stretch: {
        resizeMode: "stretch",
        justifyContent: 'center',
        alignItems: 'center'
    },
    defaultText: {
        fontSize: 16,
        color: '#000'
    },
    imageShow: {
        width: '100%',
        height: '100%',
        resizeMode: 'contain',
    },

});

export default History