import React, { useState, useCallback, useMemo, useEffect } from 'react';
import { StyleSheet, View, Text, Keyboard, Alert, Image, ActivityIndicator, AsyncStorage, Dimensions, Platform, StatusBar, Linking, TouchableOpacity, TextInput } from 'react-native';
import { Button, Input, Card, Overlay } from "react-native-elements";
import { useNavigation, useNavigationParam } from 'react-navigation-hooks'
import Ionicons from "react-native-vector-icons/FontAwesome5";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import axios from 'axios';
import SearchableDropdown from 'react-native-searchable-dropdown';
import ImagePicker from 'react-native-image-picker';
import Loading from 'react-native-loading-spinner-overlay';

const options = {
    // title: 'Select Avatar',
    // customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
    quality: 1.0,
    maxWidth: 1400,
    maxHeight: 1400,
    storageOptions: {
        skipBackup: true,
        path: 'images',
    },
};


const AddDetail = () => {
    const { navigate } = useNavigation();

    const [name, setTopic] = useState('');
    const [token, setToken] = useState('');
    const [detail, setDetail] = useState('');
    const [countPerson, setCountPerson] = useState(0);
    const [countWork, setCountWork] = useState(0);


    const [nameMachine, setNameMachine] = useState('กรุณาเลือกส่วนประกอบเครื่องจักร')


    const [image_one, setImageOne] = useState('');
    const [image_two, setImageTwo] = useState('');
    const [image_three, setImageThree] = useState('');
    const [image_four, setImageFour] = useState('');

    const [partMachine, setPartMachine] = useState('');
    const [partIdMachine, setPartIdMachine] = useState('');
    const [changePart, setChangePart] = useState(0);
    const [listAsset, setListAsset] = useState([]);
    const [listAssetSend, setListAssetSend] = useState([]);
    const [listAssetDialog, setListAssetDialog] = useState([{ name: 'กรุณเลือกเครื่องจักร', count: 0, id: 0 }]);


    const [open_asset, setOpenAsset] = useState(0);
    const [open_machine, setOpenMachine] = useState(0);


    const [dialogAssetVisible, setDialogVisibleAsset] = useState(false);
    const [dialogMachineVisible, setDialogVisibleMachine] = useState(false);
    const [dialogQuestionVisible, setDialogVisibleQuestion] = useState(false);
    const [dialogWarningVisible, setDialogVisibleWarning] = useState(false);
    const [dialogConfirmVisible, setDialogVisibleConfirm] = useState(false);
    const [dialogCancelVisible, setDialogVisibleCancel] = useState(false);

    const [status, setStatus] = useState(false);

    const [type, setType] = useState(0);
    const [repairId, setRepairId] = useState(0);




    const [listData, setListData] = useState([]);
    const [listDataTwo, setListTwoData] = useState([]);

    const [type_create, setTypeCreate] = useState([]);
    const [isLoading, setLoading] = useState(false);


    useEffect(() => {
        async function fetchMyAPI() {
            var result_id = await AsyncStorage.getItem('result_id')
            setRepairId(parseInt(await AsyncStorage.getItem('repair_type_id')))
            setType(parseInt(await AsyncStorage.getItem('repair_type_create')))
            setTypeCreate(await AsyncStorage.getItem('type_crate'))
            setToken(await AsyncStorage.getItem('token'))
            setTopic(await AsyncStorage.getItem('topic_name'))
            callGetPartMachineList(await AsyncStorage.getItem('token'), result_id)
            callGetWastingAssetList(await AsyncStorage.getItem('token'))
        }
        fetchMyAPI()
    }, [])



    chooseImageFile = () => {
        ImagePicker.showImagePicker(options, (response) => {


            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            } else {
                //  setImage1(response.uri)
                // console.log('1 : ', 'data:image/jpeg;base64,' + response.data)
                // setBase64Image1('data:image/jpeg;base64,' + response.data)
                callWebUploadPicture(1, 'data:image/jpeg;base64,' + response.data)
            }
        });
    };


    chooseImageFileTwo = () => {
        ImagePicker.showImagePicker(options, (response) => {


            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            } else {
                //  setImage2(response.uri)
                // setBase64Image2('data:image/jpeg;base64,' + response.data)
                callWebUploadPicture(2, 'data:image/jpeg;base64,' + response.data)
            }
        });
    };



    chooseImageFileThree = () => {
        ImagePicker.showImagePicker(options, (response) => {


            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            } else {
                // setImage3(response.uri)
                // setBase64Image3('data:image/jpeg;base64,' + response.data)
                callWebUploadPicture(3, 'data:image/jpeg;base64,' + response.data)
            }
        });
    };



    chooseImageFileFour = () => {
        ImagePicker.showImagePicker(options, (response) => {


            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            } else {
                //  setImage4(response.uri)
                // setBase64Image4('data:image/jpeg;base64,' + response.data)
                callWebUploadPicture(4, 'data:image/jpeg;base64,' + response.data)
            }
        });
    };


    onOpenDropDownAsset = (count) => {
        var count_open = count
        count_open += 1
        setOpenAsset(count_open)
    }


    onOpenDropDownMachine = (count) => {

        var count_open = count
        count_open += 1
        setOpenMachine(count_open)
    }


    {/* ชัวโมงการซ่อมบำรุง */ }
    onIncreaseWork = (count) => {
        var count_increase = count
        count_increase += 1
        setCountWork(count_increase)
    }

    onDecreaseWork = (count) => {
        if (count > 1) {
            var count_decrease = count
            count_decrease -= 1
            setCountWork(count_decrease)
        }
    }

    {/* จำนวนคน */ }
    onIncreasePerson = (count) => {
        var count_increase = count
        count_increase += 1
        setCountPerson(count_increase)
    }

    onDecreasePerson = (count) => {
        if (count > 1) {
            var count_decrease = count
            count_decrease -= 1
            setCountPerson(count_decrease)
        }
    }

    {/* เปลี่ยนส่วนประกอบเครื่องจักร */ }
    onIncreaseMachine = (count) => {
        var count_increase = count
        count_increase += 1
        setChangePart(count_increase)
    }

    onDecreaseMachine = (count) => {
        if (count > 1) {
            var count_decrease = count
            count_decrease -= 1
            setChangePart(count_decrease)
        }
    }


    {/* ชิ้นส่วนเครื่องจักร */ }
    onIncreaseListMachine = (count, pos) => {
        var count_increase = count
        count_increase += 1
        let newArr = [...listAsset];
        newArr[pos].Amount = count_increase;
        setListAsset(newArr)
    }

    onDecreaseListMachine = (count, pos) => {
        console.log('count : ', count)
        console.log('pos : ', pos)
        if (count > 1) {
            var count_decrease = count
            count_decrease -= 1
            let newArr = [...listAsset];
            newArr[pos].Amount = count_decrease;
            setListAsset(newArr)
        }
    }


    {/* ชิ้นส่วนเครื่องจักร */ }
    onIncreaseListMachineDialog = (count_Value, pos) => {
        var count_increase = count_Value
        count_increase += 1
        let newArr = [...listAssetDialog];
        newArr[pos].count = count_increase;
        setListAssetDialog(newArr)
    }

    onDecreaseListMachineDialog = (count_Value, pos) => {
        if (count_Value > 1) {
            var count_decrease = count_Value
            count_decrease -= 1
            let newArr = [...listAssetDialog];
            newArr[pos].count = count_decrease;
            setListAssetDialog(newArr)
        }
    }


    onChangeTextName = (value, pos, value_id) => {
        let newArr = [...listAssetDialog];
        newArr[pos].name = value;
        newArr[pos].id = value_id;
        setListAssetDialog(newArr)
    }

    onChangeTextDetail = (value) => {
        setDetail(value)
    }




    callGetPartMachineList = async (token, machine_id) => {
        console.log('1234 : ', 'http://apitpm.bsisugarcane.com/api/MaintenanceApp/api/MaintenanceApp/GetPartMachineList?token=' + token + '&MachineUnitID=' + machine_id)
        axios.get('http://apitpm.bsisugarcane.com/api/MaintenanceApp/api/MaintenanceApp/GetPartMachineList?token=' + token + '&MachineUnitID=' + machine_id)
            .then((response) => {


                var list = []
                var objectData = {}
                var i = 0
                console.log('response first : ', response.data.Results)
                for (i = 0; i < response.data.Results.length; i++) {
                    objectData = {
                        name: response.data.Results[i].PartName,
                        id: response.data.Results[i].Id,
                    }
                    list.push(objectData)
                }


                setListData(list)
            })
            .catch((error) => {
            })
            .finally(function () {
            });

    };


    callGetWastingAssetList = async (token) => {
        axios.get('http://apitpm.bsisugarcane.com/api/MaintenanceApp/api/MaintenanceApp/GetWastingAssetList?token=' + token)
            .then((response) => {

                console.log('555 + 555 : ', response.data)

                var list = []
                var objectData = {}
                var i = 0
                console.log('response first : ', response.data.Results)
                for (i = 0; i < response.data.Results.length; i++) {
                    objectData = {
                        id: response.data.Results[i].Id,
                        name: response.data.Results[i].Name
                    }
                    list.push(objectData)
                }


                setListTwoData(list)
            })
            .catch((error) => {
            })
            .finally(function () {
            });

    };


    callWebUploadPicture = async (type_data, image_data) => {

        const data = {
            type: type_data,
            image: image_data
        }

        axios.post('http://apitpm.bsisugarcane.com/api/MaintenanceApp/api/MaintenanceApp/webUploadPicture', data)
            .then((response) => {


                console.log('data : ', response.data.Results.ImagePath)
                if (type_data == 1) {
                    setImageOne(response.data.Results.ImagePath)
                } else if (type_data == 2) {
                    setImageTwo(response.data.Results.ImagePath)
                } else if (type_data == 3) {
                    setImageThree(response.data.Results.ImagePath)
                } else if (type_data == 4) {
                    setImageFour(response.data.Results.ImagePath)
                }
            })
            .catch((error) => {
            })
            .finally(function () {
            });

    };



    callPostInsertRepairLog = async () => {
        setLoading(true)
        const data = {
            repairID: repairId,
            partMachineID: partIdMachine,
            status: status,
            changePart: changePart,
            detail: detail,
            base64Image1: image_one,
            base64Image2: image_two,
            base64Image3: image_three,
            base64Image4: image_four,
            topic: name,
            hours: countWork,
            amounttPerson: countPerson,
            type: type,
            isWastingAsset: listAssetSend
        }

        console.log('data send : ', data)
        console.log('data send token : ', token)

        axios.post('http://apitpm.bsisugarcane.com/api/MaintenanceApp/api/MaintenanceApp/PostInsertRepairLog?token=' + token, data)
            .then((response) => {
                setLoading(false)
                if (response.data.Messages == "create successfully.") {
                    setDialogVisibleWarning(false)
                    setDialogVisibleConfirm(true)
                } else {
                    setDialogVisibleWarning(false)
                    setDialogVisibleCancel(true)
                }


            })
            .catch((error) => {
                setLoading(false)
                console.log('error result : ', error)
            })
            .finally(function () {
                setLoading(false)
            });

    };


    var popupMachine = (
        <Overlay fullScreen isVisible={dialogMachineVisible} overlayStyle={{ flex: 1, backgroundColor: '#FFF', width: '100%', borderRadius: 24 }} onBackdropPress={() => console.log('yahoo')} >
            <View style={{ alignItems: 'center', justifyContent: 'center', flexdirection: 'column' }}>

                <View style={{ height: '80%', width: '100%', alignItems: 'center', }}>
                    <Text style={{
                        color: '#3E5481',
                        fontFamily: "Prompt-Bold",
                        fontSize: 24,
                        marginTop: 60
                    }}>
                        ส่วนประกอบเครื่องจักร
                </Text>



                    <SearchableDropdown
                        onItemSelect={(item) => {
                            console.log('name : ', item.name)
                            setNameMachine(item.name)
                            setPartIdMachine(item.id)
                        }}
                        containerStyle={{ marginTop: 24, width: '90%' }}
                        itemStyle={{
                            padding: 10,
                            backgroundColor: '#FFF',
                            borderColor: '#EFEFEF',
                            borderWidth: 1,
                            fontFamily: "Prompt-Regular",
                        }}
                        itemTextStyle={{ color: '#222', fontFamily: "Prompt-Regular" }}
                        items={listData}
                        resetValue={false}
                        textInputProps={
                            {
                                placeholder: nameMachine,
                                placeholderTextColor: "#000",
                                fontFamily: "Prompt-Regular",
                                underlineColorAndroid: "transparent",
                                style: {
                                    padding: 12,
                                    borderWidth: 1,
                                    borderColor: '#ccc',
                                    borderRadius: 8,
                                },
                                onTextChange: text => console.log('text : ', text)
                            }
                        }

                    />

                    <View style={{ flexDirection: 'row', height: 50, marginLeft: 24, marginRight: 24, marginTop: 24 }} >

                        <View style={{ flex: 0.6, flexDirection: 'row', alignItems: 'center' }} >
                            <Text style={{ color: "#3E5481", fontFamily: "Prompt-Bold", fontSize: 22 }}>จำนวน</Text>

                        </View>



                        <View style={{ flex: 0.1, flexDirection: 'row', alignItems: 'center' }} >
                            <TouchableOpacity
                                onPress={() => onDecreaseMachine(changePart)
                                } >
                                <Image

                                    source={require('../../img/decrease_grey.png')}
                                ></Image>

                            </TouchableOpacity>
                        </View>
                        <View style={{ flex: 0.2, flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }} >


                            <Text style={{ color: "#3E5481", fontFamily: "Prompt-Regular", fontSize: 20 }}>{changePart}</Text>
                        </View>

                        <View style={{ flex: 0.1, flexDirection: 'row', alignItems: 'center' }} >
                            <TouchableOpacity
                                onPress={() => onIncreaseMachine(changePart)
                                } >
                                <Image

                                    source={require('../../img/increase.png')}
                                ></Image>
                            </TouchableOpacity>
                        </View>


                    </View>


                </View>


                <View style={{ height: '20%', width: '100%', alignItems: 'center', }}>


                    <View style={{ flexDirection: 'row', height: 50, marginTop: 24 }} >

                        <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }} >





                            <Button
                                buttonStyle={{ backgroundColor: "#4DB6AC", height: 50, width: 300, alignSelf: "center", marginLeft: 8, marginRight: 8, borderRadius: 25, borderWidth: 1, borderColor: '#4DB6AC' }}
                                title="ยืนยัน"
                                titleStyle={{ color: '#FFF', fontSize: 18, fontFamily: "Prompt-Bold" }}
                                onPress={() => {
                                    if (changePart != 0) {
                                        setDialogVisibleMachine(false)
                                        setPartMachine(nameMachine)
                                    } else {
                                        showAlert('กรุณาใส่จำนวน')
                                    }
                                }

                                }>
                            </Button>

                        </View>










                    </View>


                    <View style={{ flexDirection: 'row', height: 50, marginTop: 24 }} >
                        <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }} >
                            <Button
                                buttonStyle={{ backgroundColor: "#F4F5F7", height: 50, width: 300, alignSelf: "center", marginLeft: 8, marginRight: 8, borderRadius: 25, borderWidth: 1, borderColor: '#F4F5F7' }}
                                title="ยกเลิก"
                                titleStyle={{ color: '#2E3E5C', fontSize: 18, fontFamily: "Prompt-Bold" }}
                                onPress={() => {
                                    setDialogVisibleMachine(false)
                                    setPartMachine('')
                                }
                                }>
                            </Button>

                        </View>

                    </View>


                </View>

            </View>











        </Overlay>

    )



    var popupAsset = (
        <Overlay fullScreen isVisible={dialogAssetVisible} overlayStyle={{ flex: 1, backgroundColor: '#FFF', width: '100%', borderRadius: 24 }} onBackdropPress={() => console.log('yahoo')} >
            <View style={{ alignItems: 'center', justifyContent: 'center', flexdirection: 'column' }}>




                <View style={{ height: '70%', width: '100%', alignItems: 'center', }}>


                    <Text style={{
                        color: '#3E5481',
                        fontFamily: "Prompt-Bold",
                        fontSize: 24,
                        marginTop: 60
                    }}>
                        เปลี่ยนชิ้นส่วนเครื่องจักร
                </Text>


                    {/* {listAsset.map((item, index) => {
                                        return ( */}

                    {/* <KeyboardAwareScrollView extraHeight={screenWidth - 200} style={{ width: '100%' }} > */}
                    <View style={{ width: '100%' }}>
                        {listAssetDialog.map((item, index) => {
                            return (
                                <View style={{ flexDirection: 'column' }} >

                                    <SearchableDropdown
                                        onItemSelect={(item) => {
                                            onChangeTextName(item.name, index, item.id)
                                        }}
                                        containerStyle={{ marginTop: 24, width: '90%', alignSelf: 'center' }}
                                        itemStyle={{
                                            padding: 10,
                                            backgroundColor: '#FFF',
                                            borderColor: '#EFEFEF',
                                            borderWidth: 1,
                                            fontFamily: "Prompt-Regular",
                                        }}
                                        itemTextStyle={{ color: '#222', fontFamily: "Prompt-Regular" }}
                                        items={listDataTwo}
                                        resetValue={false}
                                        textInputProps={
                                            {
                                                placeholder: listAssetDialog[index].name,
                                                placeholderTextColor: "#000",
                                                fontFamily: "Prompt-Regular",
                                                underlineColorAndroid: "transparent",
                                                style: {
                                                    padding: 12,
                                                    borderWidth: 1,
                                                    borderColor: '#ccc',
                                                    borderRadius: 8,
                                                },
                                                onTextChange: text => console.log('text : ', text)
                                            }
                                        }

                                    />









                                    <View style={{ flexDirection: 'row', height: 50, marginLeft: 24, marginRight: 24 }} >

                                        <View style={{ flex: 0.6, flexDirection: 'row', alignItems: 'center' }} >
                                            <Text style={{ color: "#3E5481", fontFamily: "Prompt-Bold", fontSize: 22 }}>จำนวน</Text>

                                        </View>



                                        <View style={{ flex: 0.1, flexDirection: 'row', alignItems: 'center' }} >
                                            <TouchableOpacity
                                                onPress={() => onDecreaseListMachineDialog(item.count, index)
                                                } >
                                                <Image

                                                    source={require('../../img/decrease_grey.png')}
                                                ></Image>

                                            </TouchableOpacity>
                                        </View>
                                        <View style={{ flex: 0.2, flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }} >


                                            <Text style={{ color: "#3E5481", fontFamily: "Prompt-Regular", fontSize: 20 }}>{item.count}</Text>
                                        </View>

                                        <View style={{ flex: 0.1, flexDirection: 'row', alignItems: 'center' }} >
                                            <TouchableOpacity
                                                onPress={() => onIncreaseListMachineDialog(item.count, index)
                                                } >
                                                <Image

                                                    source={require('../../img/increase.png')}
                                                ></Image>
                                            </TouchableOpacity>
                                        </View>


                                    </View>




                                </View>
                            )

                        }
                        )}

                    </View>

                    {/* </KeyboardAwareScrollView> */}

                </View>



                <View style={{ height: '30%', width: '100%', alignItems: 'center', }}>

                    <View style={{ flexDirection: 'row', height: 50, marginTop: 24 }} >
                        <TouchableOpacity
                            style={{ flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'center', backgroundColor: '#F4F5F7', borderRadius: 25 }}
                            onPress={() => {
                                var newArray = [...listAssetDialog, { name: 'กรุณเลือกเครื่องจักร', count: 0, id: 0 }];
                                setListAssetDialog(newArray)
                                console.log('size array : ', newArray)
                            }
                            } >

                            <View >



                                <Image

                                    source={require('../../img/plus_blue.png')}
                                ></Image>

                            </View>

                        </TouchableOpacity>









                    </View>




                    <View style={{ flexDirection: 'row', marginTop: 24 }} >







                        <Button
                            buttonStyle={{ backgroundColor: "#4DB6AC", height: 50, width: '100%', alignSelf: "center", marginLeft: 8, marginRight: 8, borderRadius: 25, borderWidth: 1, borderColor: '#4DB6AC' }}
                            title="ยืนยัน"
                            titleStyle={{ color: '#FFF', fontSize: 18, fontFamily: "Prompt-Bold" }}
                            onPress={() => {
                                var check = 0
                                var check_count = 0
                                var k = 0
                                for (k = 0; k < listAssetDialog.length; k++) {
                                    if (listAssetDialog[k].name == '' || listAssetDialog[k].name == null || listAssetDialog[k].name == undefined) {
                                        check += 1
                                    }

                                    if (listAssetDialog[k].count == 0) {
                                        check_count += 1
                                    }
                                }


                                console.log('size check  : ', check)

                                if (check >= 1) {
                                    showAlert('กรุณากรอกชื่อชิ้นส่วนเครื่องจักร')
                                } else if (check_count >= 1) {
                                    showAlert('กรุณาใส่จำนวนส่วนเครื่องจักร')
                                } else {
                                    var list = []
                                    var listSend = []
                                    var k = 0
                                    for (k = 0; k < listAssetDialog.length; k++) {
                                        objectData = {
                                            Name: listAssetDialog[k].name,
                                            Amount: listAssetDialog[k].count
                                        }

                                        objectSendData = {
                                            wastingAssetID: listAssetDialog[k].id,
                                            amount: listAssetDialog[k].count
                                        }
                                        list.push(objectData)
                                        listSend.push(objectSendData)
                                    }
                                    setListAsset(list)
                                    setListAssetSend(listSend)
                                    setDialogVisibleAsset(false)
                                }
                            }

                            }>
                        </Button>












                    </View>


                    <View style={{ flexDirection: 'row', marginTop: 24 }} >

                        <Button
                            buttonStyle={{ backgroundColor: "#F4F5F7", height: 50, width: '100%', alignSelf: "center", marginLeft: 8, marginRight: 8, borderRadius: 25, borderWidth: 1, borderColor: '#F4F5F7' }}
                            title="ยกเลิก"
                            titleStyle={{ color: '#2E3E5C', fontSize: 18, fontFamily: "Prompt-Bold" }}
                            onPress={() => {
                                var i = 0
                                var sum = -1

                                for (i = 0; i < listAssetDialog.length; i++) {
                                    sum += 1
                                }



                                if (listAssetDialog.length != 1) {
                                    var size = listAssetDialog.length - (listAssetDialog.length + sum)
                                    const newArray = listAssetDialog.slice(0, size)
                                    setListAssetDialog(newArray)
                                }

                                setDialogVisibleAsset(false)


                            }
                            }>
                        </Button>


                    </View>
                </View>

            </View>











        </Overlay>

    )


    var popupQuestion = (
        <Overlay fullScreen isVisible={dialogQuestionVisible} overlayStyle={{ flex: 0.4, backgroundColor: '#FFF', width: '90%', borderRadius: 24 }} onBackdropPress={() => console.log('yahoo')} >
            <View style={{ alignItems: 'center', justifyContent: 'center', }}>



                <Image
                    style={{ width: 120, height: 120, marginTop: 24 }}
                    source={require('../../img/question.png')}
                ></Image>

                <Text style={{
                    color: '#3E5481',
                    fontFamily: "Prompt-Bold",
                    fontSize: 24,
                    marginTop: 24
                }}>
                    ต้องการปิดงานหรือไม่
                </Text>






                <View style={{ flexDirection: 'row', height: 50, marginTop: 48 }} >

                    <View style={{ flex: 0.5, flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }} >
                        <Button
                            buttonStyle={{ backgroundColor: "#F4F5F7", height: 50, width: 150, alignSelf: "center", marginLeft: 8, marginRight: 8, borderRadius: 25, borderWidth: 1, borderColor: '#F4F5F7' }}
                            title="ไม่ใช่"
                            titleStyle={{ color: '#2E3E5C', fontSize: 18, fontFamily: "Prompt-Bold" }}
                            onPress={() => {
                                setStatus(false)
                                setDialogVisibleQuestion(false)
                                setDialogVisibleWarning(true)
                            }
                            }>
                        </Button>

                    </View>





                    <View style={{ flex: 0.5, flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }} >





                        <Button
                            buttonStyle={{ backgroundColor: "#4DB6AC", height: 50, width: 150, alignSelf: "center", marginLeft: 8, marginRight: 8, borderRadius: 25, borderWidth: 1, borderColor: '#4DB6AC' }}
                            title="ใช่"
                            titleStyle={{ color: '#FFF', fontSize: 18, fontFamily: "Prompt-Bold" }}
                            onPress={() => {
                                setStatus(true)
                                setDialogVisibleQuestion(false)
                                setDialogVisibleWarning(true)
                                // navigate('DetailRepair')
                                // setDialogVisible(false)
                            }

                            }>
                        </Button>

                    </View>


                </View>


            </View>











        </Overlay>

    )


    var popupWarning = (
        <Overlay fullScreen isVisible={dialogWarningVisible} overlayStyle={{ flex: 0.6, backgroundColor: '#FFF', width: '90%', borderRadius: 24 }} onBackdropPress={() => console.log('yahoo')} >
            <View style={{ alignItems: 'center', justifyContent: 'center', }}>

                <Image
                    style={{ width: 120, height: 120, marginTop: 24 }}
                    source={require('../../img/warning_two.png')}
                ></Image>

                <Text style={{
                    color: '#3E5481',
                    fontFamily: "Prompt-Bold",
                    fontSize: 24,
                    marginTop: 24
                }}>
                    ยืนยันข้อมูล
                </Text>


                <View
                    style={{
                        marginTop: 24,

                    }}

                >


                    <Text style={{
                        color: '#2E3E5C',
                        fontFamily: "Prompt-Regular",
                        fontSize: 14,
                        textAlign: 'center',
                        marginLeft: 12,
                        marginRight: 12,


                    }}>
                        โปรดตรวจสอบข้อมูลของท่านก่อนทำการบันทึก หากตรวจสอบเรียบร้อยแล้ว กดปุ่ม “ยืนยัน” เพื่อให้ระบบทำรายการบันทึกข้อมูล
                </Text>

                </View>





                <View style={{ flexDirection: 'row', height: 50, marginTop: 36 }} >

                    <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }} >





                        <Button
                            buttonStyle={{ backgroundColor: "#4DB6AC", height: 50, width: 300, alignSelf: "center", marginLeft: 8, marginRight: 8, borderRadius: 25, borderWidth: 1, borderColor: '#4DB6AC' }}
                            title="ยืนยัน"
                            titleStyle={{ color: '#FFF', fontSize: 18, fontFamily: "Prompt-Bold" }}
                            onPress={() => {
                                setDialogVisibleWarning(false)
                                callPostInsertRepairLog()
                            }

                            }>
                        </Button>

                    </View>










                </View>


                <View style={{ flexDirection: 'row', height: 50, marginTop: 24 }} >
                    <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }} >
                        <Button
                            buttonStyle={{ backgroundColor: "#F4F5F7", height: 50, width: 300, alignSelf: "center", marginLeft: 8, marginRight: 8, borderRadius: 25, borderWidth: 1, borderColor: '#F4F5F7' }}
                            title="เเก้ไข"
                            titleStyle={{ color: '#2E3E5C', fontSize: 18, fontFamily: "Prompt-Bold" }}
                            onPress={() => {
                                setDialogVisibleWarning(false)
                            }
                            }>
                        </Button>

                    </View>

                </View>


            </View>











        </Overlay>

    )



    var popupConfirm = (
        <Overlay fullScreen isVisible={dialogConfirmVisible} overlayStyle={{ flex: 0.4, backgroundColor: '#FFF', width: '90%', borderRadius: 24 }} onBackdropPress={() => console.log('yahoo')} >
            <View style={{ alignItems: 'center', justifyContent: 'center', }}>

                <Image
                    style={{ width: 120, height: 120, marginTop: 24 }}
                    source={require('../../img/warning_two.png')}
                ></Image>

                <Text style={{
                    color: '#3E5481',
                    fontFamily: "Prompt-Bold",
                    fontSize: 24,
                    marginTop: 24
                }}>
                    ดำเนินการเสร็จสิ้น
                </Text>


                <View
                    style={{
                        marginTop: 24,

                    }}

                >


                    <Text style={{
                        color: '#2E3E5C',
                        fontFamily: "Prompt-Regular",
                        fontSize: 14,
                        textAlign: 'center',
                        marginLeft: 12,
                        marginRight: 12,


                    }}>
                        ข้อมูลของท่านถูกบันทึกลงระบบเรียบร้อยแล้ว
                </Text>

                </View>





                <View style={{ flexDirection: 'row', height: 50, marginTop: 36 }} >

                    <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }} >





                        <Button
                            buttonStyle={{ backgroundColor: "#4DB6AC", height: 50, width: 300, alignSelf: "center", marginLeft: 8, marginRight: 8, borderRadius: 25, borderWidth: 1, borderColor: '#4DB6AC' }}
                            title="กลับสู่รายการซ่อม"
                            titleStyle={{ color: '#FFF', fontSize: 18, fontFamily: "Prompt-Bold" }}
                            onPress={() => {
                                setDialogVisibleConfirm(false)
                                navigate('DetailRepair')
                            }

                            }>
                        </Button>

                    </View>










                </View>



            </View>











        </Overlay>

    )



    var popupCancel = (
        <Overlay fullScreen isVisible={dialogCancelVisible} overlayStyle={{ flex: 0.4, backgroundColor: '#FFF', width: '90%', borderRadius: 24 }} onBackdropPress={() => console.log('yahoo')} >
            <View style={{ alignItems: 'center', justifyContent: 'center', }}>

                <Image
                    style={{ width: 120, height: 120, marginTop: 24 }}
                    source={require('../../img/warning_two.png')}
                ></Image>

                <Text style={{
                    color: '#3E5481',
                    fontFamily: "Prompt-Bold",
                    fontSize: 24,
                    marginTop: 24
                }}>
                    ไม่สามารถบันทึกข้อมูล
                </Text>


                <View
                    style={{
                        marginTop: 24,

                    }}

                >


                    <Text style={{
                        color: '#2E3E5C',
                        fontFamily: "Prompt-Regular",
                        fontSize: 14,
                        textAlign: 'center',
                        marginLeft: 12,
                        marginRight: 12,


                    }}>
                        กรุณาตรวจสอบข้อมูลของท่านอีกครั้ง
                </Text>

                </View>





                <View style={{ flexDirection: 'row', height: 50, marginTop: 36 }} >

                    <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }} >





                        <Button
                            buttonStyle={{ backgroundColor: "#4DB6AC", height: 50, width: 300, alignSelf: "center", marginLeft: 8, marginRight: 8, borderRadius: 25, borderWidth: 1, borderColor: '#4DB6AC' }}
                            title="กลับสู่รายการซ่อม"
                            titleStyle={{ color: '#FFF', fontSize: 18, fontFamily: "Prompt-Bold" }}
                            onPress={() => {
                                setDialogVisibleCancel(false)
                            }

                            }>
                        </Button>

                    </View>










                </View>



            </View>











        </Overlay>

    )


    showAlert = (title, message) => {
        Alert.alert(
            title,
            message,
            [
                {
                    text: 'OK',
                    onPress: () => {
                        //  setLoading(false)
                        //navigate('Record')
                    }


                },
            ],
            { cancelable: false },
        );
    }


    const { width: screenWidth } = Dimensions.get('window');
    return (
        <View style={{
            flex: 1,
            backgroundColor: "#fff",
            width: "100%",
        }}>
            {isLoading ?
                <Loading
                    visible={true}
                    textContent={'Loading...'}
                    textStyle={styles.spinnerTextStyle}
                />
                :
                <View style={styles.MainContainer}>

                    {popupMachine}
                    {popupAsset}
                    {popupQuestion}
                    {popupWarning}
                    {popupConfirm}
                    {popupCancel}

                    <KeyboardAwareScrollView extraHeight={screenWidth - 200}  >
                        {/* <ScrollView> */}

                        <View style={{ flex: 1, flexDirection: 'column' }} >


                            <Text style={[styles.baseText, { color: "#3E5481", fontFamily: "Prompt-Bold", fontSize: 36, marginTop: 48, marginLeft: 24 }]}>{name}</Text>

                            <View style={{
                                flexDirection: "row",
                                height: 80,
                                marginTop: 36,
                                justifyContent: 'center', alignItems: 'center'
                            }}>

                                <View style={{ flex: 0.4 }}>
                                    {image_one == '' ?
                                        <TouchableOpacity onPress={() => { chooseImageFile() }}>
                                            <View style={{
                                                height: 140, justifyContent: 'center',
                                                alignItems: 'center',
                                                backgroundColor: '#EEF1F6',
                                                borderRadius: 20,
                                                marginRight: 12
                                            }}>
                                                <Image

                                                    source={require('../../img/camera_color.png')}
                                                ></Image>

                                            </View>
                                        </TouchableOpacity>
                                        :
                                        <TouchableOpacity onPress={() => { chooseImageFile() }}>
                                            <View style={{
                                                height: 140, justifyContent: 'center',
                                                alignItems: 'center',
                                                marginRight: 12
                                            }}>
                                                <Image
                                                    style={styles.imageShow}
                                                    source={{ uri: image_one }}
                                                ></Image>

                                            </View>
                                        </TouchableOpacity>
                                    }
                                </View>
                                <View style={{ flex: 0.4 }}>
                                    {image_two == '' ?
                                        <TouchableOpacity onPress={() => { chooseImageFileTwo() }}>
                                            <View style={{
                                                height: 140, justifyContent: 'center',
                                                alignItems: 'center',
                                                backgroundColor: '#EEF1F6',
                                                borderRadius: 20,
                                                marginLeft: 12
                                            }}>
                                                <Image

                                                    source={require('../../img/camera_color.png')}
                                                ></Image>

                                            </View>
                                        </TouchableOpacity>
                                        :
                                        <TouchableOpacity onPress={() => { chooseImageFileTwo() }}>
                                            <View style={{
                                                height: 140, justifyContent: 'center',
                                                alignItems: 'center',
                                                marginRight: 12
                                            }}>
                                                <Image
                                                    style={styles.imageShow}
                                                    source={{ uri: image_two }}
                                                ></Image>

                                            </View>
                                        </TouchableOpacity>
                                    }
                                </View>
                            </View>

                            <View style={{
                                flexDirection: "row",
                                height: 80,
                                marginTop: 72,
                                justifyContent: 'center', alignItems: 'center'
                            }}>
                                <View style={{ flex: 0.4 }}>
                                    {image_three == '' ?
                                        <TouchableOpacity onPress={() => { chooseImageFileThree() }}>
                                            <View style={{
                                                height: 140, justifyContent: 'center',
                                                alignItems: 'center',
                                                backgroundColor: '#EEF1F6',
                                                borderRadius: 20,
                                                marginRight: 12
                                            }}>
                                                <Image

                                                    source={require('../../img/camera_color.png')}
                                                ></Image>

                                            </View>
                                        </TouchableOpacity>
                                        :
                                        <TouchableOpacity onPress={() => { chooseImageFileThree() }}>
                                            <View style={{
                                                height: 140, justifyContent: 'center',
                                                alignItems: 'center',
                                                marginRight: 12
                                            }}>
                                                <Image
                                                    style={styles.imageShow}
                                                    source={{ uri: image_three }}
                                                ></Image>

                                            </View>
                                        </TouchableOpacity>
                                    }
                                </View>
                                <View style={{ flex: 0.4 }}>
                                    {image_four == '' ?
                                        <TouchableOpacity onPress={() => { chooseImageFileFour() }}>
                                            <View style={{
                                                height: 140, justifyContent: 'center',
                                                alignItems: 'center',
                                                backgroundColor: '#EEF1F6',
                                                borderRadius: 20,
                                                marginLeft: 12
                                            }}>
                                                <Image

                                                    source={require('../../img/camera_color.png')}
                                                ></Image>

                                            </View>
                                        </TouchableOpacity>
                                        :
                                        <TouchableOpacity onPress={() => { chooseImageFileFour() }}>
                                            <View style={{
                                                height: 140, justifyContent: 'center',
                                                alignItems: 'center',
                                                marginRight: 12
                                            }}>
                                                <Image
                                                    style={styles.imageShow}
                                                    source={{ uri: image_four }}
                                                ></Image>

                                            </View>
                                        </TouchableOpacity>
                                    }
                                </View>
                            </View>



                            {/* เปลี่ยนส่วนประกอบเครื่องจักร */}
                            {partMachine == '' ?
                                <TouchableOpacity
                                    onPress={() => {
                                        setDialogVisibleMachine(true)
                                        setChangePart(0)
                                    }
                                    } >
                                    <View style={{ flexDirection: 'row', borderRadius: 10, height: 50, marginLeft: 24, marginRight: 24, marginTop: 48, borderColor: "#689AB7", borderWidth: 1 }} >

                                        <View style={{ flex: 0.15, flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }} >


                                            <Image

                                                source={require('../../img/plus_machine.png')}
                                            ></Image>

                                        </View>


                                        <View style={{ height: '100%', backgroundColor: "#689AB7", width: 1 }} >

                                        </View>


                                        <View style={{ flex: 0.85, flexDirection: 'row', alignItems: 'center' }} >

                                            <Text style={{ color: "#689AB7", fontFamily: "Prompt-Bold", fontSize: 20, marginLeft: 24 }}>เปลี่ยนส่วนประกอบเครื่องจักร</Text>
                                        </View>


                                    </View>
                                </TouchableOpacity>
                                :

                                <View style={{ flex: 1, flexDirection: 'column' }} >
                                    <View style={{ flexDirection: 'row', borderRadius: 10, height: 50, marginTop: 48 }} >



                                        <View style={{ flex: 0.85, flexDirection: 'row', alignItems: 'center' }} >

                                            <Text style={{ color: "#3E5481", fontFamily: "Prompt-Bold", fontSize: 20, marginLeft: 24 }}>เปลี่ยนส่วนประกอบเครื่องจักร</Text>
                                        </View>

                                        <View style={{ flex: 0.15, flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }} >


                                            {open_machine % 2 == 0 ?
                                                <TouchableOpacity
                                                    onPress={() => onOpenDropDownMachine(open_machine)
                                                    } >
                                                    <Image

                                                        source={require('../../img/arrow_up.png')}
                                                    ></Image>
                                                </TouchableOpacity>

                                                :

                                                <TouchableOpacity
                                                    onPress={() => onOpenDropDownMachine(open_machine)
                                                    } >
                                                    <Image

                                                        source={require('../../img/arrow_down.png')}
                                                    ></Image>
                                                </TouchableOpacity>
                                            }


                                        </View>

                                    </View>




                                    {open_machine % 2 == 0 ?
                                        <View>
                                        </View>
                                        :
                                        <View style={{ flexDirection: 'row', marginLeft: 24, marginRight: 24 }} >

                                            <View style={{ flex: 0.65, flexDirection: 'row', alignItems: 'center' }} >
                                                <Text style={{ color: "#3E5481", fontFamily: "Prompt-Regular", fontSize: 18, marginLeft: 24 }}>{partMachine}</Text>

                                            </View>



                                            <View style={{ flex: 0.1, flexDirection: 'row', alignItems: 'center' }} >
                                                <TouchableOpacity
                                                    onPress={() => onDecreaseMachine(changePart)
                                                    } >
                                                    <Image

                                                        source={require('../../img/decrease_grey.png')}
                                                    ></Image>
                                                </TouchableOpacity>
                                            </View>
                                            <View style={{ flex: 0.15, flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }} >


                                                <Text style={{ color: "#3E5481", fontFamily: "Prompt-Regular", fontSize: 20 }}>{changePart}</Text>
                                            </View>

                                            <View style={{ flex: 0.1, flexDirection: 'row', alignItems: 'center' }} >
                                                <TouchableOpacity
                                                    onPress={() => onIncreaseMachine(changePart)
                                                    } >
                                                    <Image

                                                        source={require('../../img/increase.png')}
                                                    ></Image>
                                                </TouchableOpacity>
                                            </View>




                                        </View>
                                    }
                                </View>

                            }


                            {/* เปลี่ยนชิ้นส่วนเครื่องจักร */}
                            {listAsset.length == 0 ?


                                <TouchableOpacity
                                    onPress={() => {
                                        setDialogVisibleAsset(true)
                                        setListAssetDialog([{ name: 'กรุณเลือกเครื่องจักร', count: 0, id: 0 }])
                                    }
                                    } >
                                    <View style={{ flexDirection: 'row', borderRadius: 10, height: 50, marginLeft: 24, marginRight: 24, marginTop: 12, borderColor: "#677E8E", borderWidth: 1 }} >

                                        <View style={{ flex: 0.15, flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }} >


                                            <Image

                                                source={require('../../img/plus_grey.png')}
                                            ></Image>

                                        </View>


                                        <View style={{ height: '100%', backgroundColor: "#677E8E", width: 1 }} >

                                        </View>


                                        <View style={{ flex: 0.85, flexDirection: 'row', alignItems: 'center' }} >

                                            <Text style={{ color: "#677E8E", fontFamily: "Prompt-Bold", fontSize: 20, marginLeft: 24 }}>เปลี่ยนชิ้นส่วนเครื่องจักร</Text>
                                        </View>


                                    </View>


                                </TouchableOpacity>
                                :

                                <View style={{ flex: 1, flexDirection: 'column' }} >
                                    <View style={{ flexDirection: 'row', borderRadius: 10, height: 50 }} >



                                        <View style={{ flex: 0.85, flexDirection: 'row', alignItems: 'center' }} >

                                            <Text style={{ color: "#3E5481", fontFamily: "Prompt-Bold", fontSize: 20, marginLeft: 24 }}>ชิ้นส่วนเครื่องจักร</Text>
                                        </View>

                                        <View style={{ flex: 0.15, flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }} >

                                            {open_asset % 2 == 0 ?
                                                <TouchableOpacity
                                                    onPress={() => onOpenDropDownAsset(open_asset)
                                                    } >
                                                    <Image

                                                        source={require('../../img/arrow_up.png')}
                                                    ></Image>
                                                </TouchableOpacity>
                                                :
                                                <TouchableOpacity
                                                    onPress={() => onOpenDropDownAsset(open_asset)
                                                    } >
                                                    <Image

                                                        source={require('../../img/arrow_down.png')}
                                                    ></Image>
                                                </TouchableOpacity>

                                            }

                                        </View>


                                    </View>

                                    {open_asset % 2 == 0 ?
                                        <View>
                                        </View> :

                                        <View>
                                            {listAsset.map((item, index) => {
                                                return (



                                                    <View style={{ flexDirection: 'row', marginLeft: 24, marginRight: 24, marginBottom: 18 }} >





                                                        <View style={{ flex: 0.65, flexDirection: 'row', alignItems: 'center' }} >
                                                            <Text style={{ color: "#3E5481", fontFamily: "Prompt-Regular", fontSize: 18, marginLeft: 24 }}>{item.Name}</Text>

                                                        </View>









                                                        <View style={{ flex: 0.1, flexDirection: 'row', alignItems: 'center' }} >
                                                            <TouchableOpacity
                                                                onPress={() => onDecreaseListMachine(item.Amount, index)
                                                                } >
                                                                <Image

                                                                    source={require('../../img/decrease_grey.png')}
                                                                ></Image>
                                                            </TouchableOpacity>
                                                        </View>
                                                        <View style={{ flex: 0.15, flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }} >


                                                            <Text style={{ color: "#3E5481", fontFamily: "Prompt-Regular", fontSize: 20 }}>{item.Amount}</Text>
                                                        </View>

                                                        <View style={{ flex: 0.1, flexDirection: 'row', alignItems: 'center' }} >
                                                            <TouchableOpacity
                                                                onPress={() => onIncreaseListMachine(item.Amount, index)
                                                                } >
                                                                <Image

                                                                    source={require('../../img/increase.png')}
                                                                ></Image>
                                                            </TouchableOpacity>
                                                        </View>







                                                    </View>
                                                )
                                            })
                                            }

                                        </View>

                                    }


                                </View>




                            }








                            <View style={{ height: 1, backgroundColor: "#EFEFEF", width: '100%', marginTop: 12 }} >

                            </View>

                            {/* จำนวนคน */}
                            <View style={{ flexDirection: 'row', height: 50, marginLeft: 24, marginRight: 24, marginTop: 6 }} >

                                <View style={{ flex: 0.65, flexDirection: 'row', alignItems: 'center' }} >
                                    <Text style={{ color: "#3E5481", fontFamily: "Prompt-Regular", fontSize: 18, marginLeft: 24 }}>จำนวนคน</Text>

                                </View>



                                <View style={{ flex: 0.1, flexDirection: 'row', alignItems: 'center' }} >
                                    <TouchableOpacity
                                        onPress={() => onDecreasePerson(countPerson)
                                        } >
                                        <Image

                                            source={require('../../img/decrease_grey.png')}
                                        ></Image>

                                    </TouchableOpacity>
                                </View>
                                <View style={{ flex: 0.15, flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }} >


                                    <Text style={{ color: "#3E5481", fontFamily: "Prompt-Regular", fontSize: 20 }}>{countPerson}</Text>
                                </View>

                                <View style={{ flex: 0.1, flexDirection: 'row', alignItems: 'center' }} >
                                    <TouchableOpacity
                                        onPress={() => onIncreasePerson(countPerson)
                                        } >
                                        <Image

                                            source={require('../../img/increase.png')}
                                        ></Image>
                                    </TouchableOpacity>
                                </View>


                            </View>


                            <View style={{ height: 1, backgroundColor: "#EFEFEF", width: '100%', marginTop: 6 }} >

                            </View>

                            {/* ชัวโมงการซ่อมบำรุง */}
                            <View style={{ flexDirection: 'row', height: 50, marginLeft: 24, marginRight: 24, marginTop: 6 }} >

                                <View style={{ flex: 0.65, flexDirection: 'row', alignItems: 'center' }} >
                                    <Text style={{ color: "#3E5481", fontFamily: "Prompt-Regular", fontSize: 18, marginLeft: 24 }}>ชั่วโมงการซ่อมบำรุง</Text>

                                </View>



                                <View style={{ flex: 0.1, flexDirection: 'row', alignItems: 'center' }} >
                                    <TouchableOpacity
                                        onPress={() => onDecreaseWork(countWork)
                                        } >
                                        <Image

                                            source={require('../../img/decrease_grey.png')}
                                        ></Image>

                                    </TouchableOpacity>
                                </View>
                                <View style={{ flex: 0.15, flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }} >


                                    <Text style={{ color: "#3E5481", fontFamily: "Prompt-Regular", fontSize: 20 }}>{countWork}</Text>
                                </View>

                                <View style={{ flex: 0.1, flexDirection: 'row', alignItems: 'center' }} >
                                    <TouchableOpacity
                                        onPress={() => onIncreaseWork(countWork)
                                        } >
                                        <Image

                                            source={require('../../img/increase.png')}
                                        ></Image>
                                    </TouchableOpacity>
                                </View>






                            </View>


                            <View style={{ height: 1, backgroundColor: "#EFEFEF", width: '100%', marginTop: 6 }} >

                            </View>


                            <TextInput
                                multiline={true}
                                style={{
                                    fontSize: 18, marginTop: 12, paddingLeft: 8, height: 100, fontFamily: "Prompt-Regular", textAlignVertical: 'top', borderWidth: 1,
                                    borderRadius: 12,
                                    borderColor: '#D0DBEA',
                                    marginLeft: 10,
                                    marginRight: 10,
                                    fontFamily: "Prompt-Regular"
                                }}
                                placeholder='กรอกรายละเอียดการซ่อมบำรุงเบื้องต้น'
                                // placeholderTextColor="#bdbdbd"
                                defaultValue={detail}
                                onChangeText={(value) => {
                                    onChangeTextDetail(value)
                                }}
                            />







                        </View>



                        {/* </ScrollView> */}

                    </KeyboardAwareScrollView>


                    <View style={{ flexDirection: 'row', height: 50, marginTop: 24, marginBottom: 48 }} >

                        <View style={{ flex: 0.5, flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }} >
                            <Button
                                buttonStyle={{ backgroundColor: "#F4F5F7", height: 50, width: 150, alignSelf: "center", marginLeft: 8, marginRight: 8, borderRadius: 25, borderWidth: 1, borderColor: '#F4F5F7' }}
                                title="ยกเลิก"
                                titleStyle={{ color: '#2E3E5C', fontSize: 18, fontFamily: "Prompt-Bold" }}
                                onPress={() =>
                                    navigate('DetailRepair')

                                }>
                            </Button>

                        </View>





                        <View style={{ flex: 0.5, flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }} >





                            <Button
                                buttonStyle={{ backgroundColor: "#4DB6AC", height: 50, width: 150, alignSelf: "center", marginLeft: 8, marginRight: 8, borderRadius: 25, borderWidth: 1, borderColor: '#4DB6AC' }}
                                title="บันทึก"
                                titleStyle={{ color: '#FFF', fontSize: 18, fontFamily: "Prompt-Bold" }}
                                onPress={() => {
                                    if (countPerson == 0) {
                                        showAlert('กรุณาเลือกจำนวนคน')
                                    } else if (countWork == 0) {
                                        showAlert('กรุณาเลือกชั่วโมงการซ่อมบำรุง')
                                    } else {
                                        setDialogVisibleQuestion(true)
                                    }

                                }
                                    // navigate('DetailRepair')

                                }>
                            </Button>

                        </View>


                    </View>



                </View>

            }
        </View>
    );
}

const styles = StyleSheet.create({
    MainContainer: {
        justifyContent: 'center',
        flex: 1,
        backgroundColor: '#FFF',
        flexDirection: "column",
    },
    loadding_style: {
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
        backgroundColor: '#fff',
    },
    appleBtn: { height: 44, width: 320, marginTop: 16 },
    spinnerTextStyle: {
        color: '#FFF',
        fontFamily: "NotoSans-Regular",
        fontSize: 14
    },
    button: {
        marginTop: 24,
        backgroundColor: "#e42179",
        width: "100%",
    },
    stretch: {
        resizeMode: "stretch",
        justifyContent: 'center',
        alignItems: 'center'
    },
    defaultText: {
        fontSize: 16,
        color: '#000'
    },
    imageShow: {
        width: '100%',
        height: '100%',
        resizeMode: 'contain',
    },
    spinnerTextStyle: {
        color: '#FFF',
        fontFamily: "THSarabunNew",
        fontSize: 20
    },

});

export default AddDetail