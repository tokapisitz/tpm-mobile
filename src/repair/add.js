import React, { useState, useCallback, useMemo, useEffect } from 'react';
import { StyleSheet, View, Text, Keyboard, Alert, Image, ActivityIndicator, AsyncStorage, Platform, StatusBar, Linking, TouchableOpacity, TouchableWithoutFeedback } from 'react-native';
import { Button, Input } from "react-native-elements";
import { useNavigation, useNavigationParam } from 'react-navigation-hooks'
import Ionicons from "react-native-vector-icons/FontAwesome5";
import axios from 'axios';
import Loading from 'react-native-loading-spinner-overlay';

const Add = () => {
    const { navigate } = useNavigation();

    const [type, setType] = useState('');
    const [token, setToken] = useState('');
    const [machineUnitID, setMachineUnitID] = useState('');
    const [topic, setTopic] = useState('');
    const [isLoading, setLoading] = useState(false);


    onChangeTextTopic = (value) => {
        setTopic(value)
    }


    showAlert = (title, message) => {
        Alert.alert(
            title,
            message,
            [
                {
                    text: 'OK',
                    onPress: () => {
                        //  setLoading(false)
                        //navigate('Record')
                    }


                },
            ],
            { cancelable: false },
        );
    }


    useEffect(() => {
        async function fetchMyAPI() {
            setToken(await AsyncStorage.getItem('token'))
            setType(await AsyncStorage.getItem('repair_type_create'))
            setMachineUnitID(await AsyncStorage.getItem('result_id'))
        }
        fetchMyAPI()
    }, [])



    callPostInsertRepair = async (token, machineUnitID, type, topic) => {
        setLoading(true)
        const data = {
            topic: topic,
            machineUnitID: machineUnitID,
            type: type
        }

        axios.post('http://apitpm.bsisugarcane.com/api/MaintenanceApp/api/MaintenanceApp/PostInsertRepair?token=' + token, data)
            .then((response) => {
                setLoading(false)
                if (response.data.Messages == "create successfully.") {
                    AsyncStorage.setItem('repair_type_id', response.data.Results.toString());
                    AsyncStorage.setItem('topic_name', topic + " EP 1");
                    AsyncStorage.setItem('type_crate', 'new');
                    navigate('AddDetailRepair')
                }
            })
            .catch((error) => {
                setLoading(false)
            })
            .finally(function () {
                setLoading(false)
            });

    };



    return (
        <View style={{
            flex: 1,
            backgroundColor: "#fff",
            width: "100%",
        }}>
            {isLoading ?
                <Loading
                    visible={true}
                    textContent={'Loading...'}
                    textStyle={styles.spinnerTextStyle}
                />
                :
                <View style={styles.MainContainer}>
                    {/* <View style={{
                flex: 0.4, justifyContent: 'center',
                alignItems: 'center',
            }} >
                <Image
                    style={styles.stretch}
                    source={require('../img/logo.png')}
                ></Image>
            </View> */}
                    <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
                        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }} >


                            <Text style={[styles.baseText, { color: "#3E5481", fontFamily: "Prompt-Bold", fontSize: 24, marginTop: 48 }]}> {type == '1' ? 'หัวข้อการซ่อมบำรุงแบบแผน' : 'หัวข้อการซ่อมบำรุงฉุกเฉิน'}</Text>



                            <Input
                                inputStyle={[styles.defaultText, { borderRadius: 30, fontSize: 18, paddingLeft: 24, height: 60, marginLeft: 18, marginRight: 18, borderColor: "#E0DFDF", marginTop: 24, borderWidth: 1, fontFamily: "Prompt-Regular", color: "#000" }]}
                                inputContainerStyle={{ borderBottomWidth: 0 }}
                                placeholder="กรอกหัวข้อการซ่อมบำรุง"
                                onChangeText={(value) => {
                                    onChangeTextTopic(value)
                                }}
                            />


                            <Button
                                buttonStyle={{ backgroundColor: "#4DB6AC", height: 60, width: 360, alignSelf: "center", marginTop: 24, borderRadius: 30, borderWidth: 1, borderColor: '#4DB6AC' }}
                                title="ต่อไป"
                                titleStyle={{ color: '#fff', fontSize: 18, fontFamily: "Prompt-Bold" }}
                                onPress={() => {
                                    //
                                    if (topic == '') {
                                        showAlert('กรุณากรอกหัวข้อเรื่อง')
                                    } else {
                                        callPostInsertRepair(token, machineUnitID, type, topic)
                                    }

                                }}>
                            </Button>


                            <Button
                                buttonStyle={{ backgroundColor: "#F4F5F7", height: 60, width: 360, alignSelf: "center", marginTop: 24, borderRadius: 30, borderWidth: 1, borderColor: '#F4F5F7' }}
                                title="ยกเลิก"
                                titleStyle={{ color: '#0B496C', fontSize: 18, fontFamily: "Prompt-Bold" }}
                                onPress={() => {
                                    navigate('ChooseRepair')
                                }
                                }>
                            </Button>






                        </View>

                    </TouchableWithoutFeedback>


                </View>
            }
        </View>
    );
}


Add.navigationOptions = ({ navigation }) => ({
    title: 'สร้างหัวข้อ',
    headerBackTitleVisible: false,
    headerTitleStyle: { color: '#fff', fontFamily: 'Prompt-Bold' },
    headerBackTitleStyle: { color: '#fff', fontFamily: 'Prompt-Bold' },
    headerStyle: { backgroundColor: '#3E5481' },
    headerTintColor: 'white',
})

const styles = StyleSheet.create({
    MainContainer: {
        justifyContent: 'center',
        flex: 1,
        backgroundColor: '#FFF',
        flexDirection: "column",
    },
    loadding_style: {
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
        backgroundColor: '#fff',
    },
    appleBtn: { height: 44, width: 320, marginTop: 16 },
    button: {
        marginTop: 24,
        backgroundColor: "#e42179",
        width: "100%",
    },
    stretch: {
        resizeMode: "stretch",
        justifyContent: 'center',
        alignItems: 'center'
    },
    defaultText: {
        fontSize: 16,
        color: '#000'
    },
    spinnerTextStyle: {
        color: '#FFF',
        fontFamily: "THSarabunNew",
        fontSize: 20
    },

});

export default Add