import React, { useState, useCallback, useMemo, useEffect } from 'react';
import { StyleSheet, View, Text, Keyboard, ScrollView, Alert, Image, ActivityIndicator, AsyncStorage, Platform, StatusBar, Linking, TouchableOpacity } from 'react-native';
import { Button, Input } from "react-native-elements";
import { useNavigation, useNavigationParam } from 'react-navigation-hooks'
import Ionicons from "react-native-vector-icons/FontAwesome5";
import axios from 'axios';
import { SliderBox } from "react-native-image-slider-box";

const Detail = () => {
    const { navigate } = useNavigation();
    const [department, setDepartment] = useState('');
    const [serialno, setSerialNo] = useState('');
    const [model, setModel] = useState('');
    const [machineType, setMachineType] = useState('');
    const [maintenancePeriod, setMaintenancePeriod] = useState('');
    const [location, setLocation] = useState('');
    const [detail, setDetail] = useState('');
    const [name, setName] = useState('');
    const [listImage, setListImage] = useState([]);


    useEffect(() => {
        async function fetchMyAPI() {
            var result_id = await AsyncStorage.getItem('result_id')
            callGetMachineUnitList(await AsyncStorage.getItem('token'), parseInt(result_id))
        }
        fetchMyAPI()
    }, [])



    callGetMachineUnitList = async (token, id) => {
        axios.get('http://apitpm.bsisugarcane.com/api/MaintenanceApp/api/MaintenanceApp/GetMachineUnitList?token=' + token)
            .then((response) => {

                var i = 0
                var pos = 0
                var list = []
                for (i = 0; i < response.data.Results.length; i++) {
                    if (response.data.Results[i].Id == id) {
                        pos = i
                    }
                }

                setDepartment(response.data.Results[pos].Department)
                setSerialNo(response.data.Results[pos].SerialNo)
                setModel(response.data.Results[pos].Model)
                setMachineType(response.data.Results[pos].MachineType)
                setMaintenancePeriod(response.data.Results[pos].MaintenancePeriod)
                setLocation(response.data.Results[pos].Location)
                setDetail(response.data.Results[pos].Detail)
                setName(response.data.Results[pos].Name)


                console.log('1 : ', response.data.Results[pos].Image1Path)


                if (response.data.Results[pos].Image1Path != "") {
                    list.push(response.data.Results[pos].Image1Path)
                }


                if (response.data.Results[pos].Image2Path != "") {
                    list.push(response.data.Results[pos].Image2Path)
                }


                if (response.data.Results[pos].Image3Path != "") {
                    list.push(response.data.Results[pos].Image3Path)
                }


                if (response.data.Results[pos].Image4Path != "") {
                    list.push(response.data.Results[pos].Image4Path)
                }

                if (response.data.Results[pos].Image5Path != "") {
                    list.push(response.data.Results[pos].Image5Path)
                }

                setListImage(list)
            })
            .catch((error) => {
            })
            .finally(function () {
            });

    };
    return (
        <View style={styles.MainContainer}>
            <View style={{
                flex: 0.7
            }} >
                <ScrollView>
                    <View style={{ flexDirection: 'column' }}  >
                        <SliderBox
                            images={listImage}
                            sliderBoxHeight={400}
                        />

                        <Text style={[styles.baseText, { color: "#3E5481", fontFamily: "Prompt-Bold", fontSize: 36, marginTop: 12, marginLeft: 24 }]}>{name}</Text>
                        <Text style={[styles.baseText, { color: "#9FA5C0", fontFamily: "Prompt-Regular", fontSize: 16, marginLeft: 24 }]}>แผนก : {department.trim()}</Text>
                        <Text style={[styles.baseText, { color: "#9FA5C0", fontFamily: "Prompt-Regular", fontSize: 16, marginLeft: 24, marginTop: 4 }]}>Serial NO. {serialno}</Text>
                        <Text style={[styles.baseText, { color: "#9FA5C0", fontFamily: "Prompt-Regular", fontSize: 16, marginLeft: 24, marginTop: 4 }]}>Model {model}</Text>
                        <Text style={[styles.baseText, { color: "#9FA5C0", fontFamily: "Prompt-Regular", fontSize: 16, marginLeft: 24, marginTop: 4 }]}>ประเภทเครื่องจักร : {machineType}</Text>
                        <Text style={[styles.baseText, { color: "#9FA5C0", fontFamily: "Prompt-Regular", fontSize: 16, marginLeft: 24, marginTop: 4 }]}>ระยะเวลาซ่อมบำรุง : {maintenancePeriod}</Text>
                        <Text style={[styles.baseText, { color: "#9FA5C0", fontFamily: "Prompt-Regular", fontSize: 16, marginLeft: 24, marginTop: 4 }]}>ตำแหน่งที่ตั้ง : {location}</Text>
                        <Text style={[styles.baseText, { color: "#9FA5C0", fontFamily: "Prompt-Regular", fontSize: 16, marginLeft: 24, marginTop: 4 }]}>รายละเอียด : {detail}</Text>

                    </View>
                </ScrollView>
            </View>
            <View style={{ flex: 0.3, justifyContent: 'center', alignItems: 'center' }} >




                <Button
                    buttonStyle={{ backgroundColor: "#00598B", height: 60, width: 360, alignSelf: "center", borderRadius: 30, borderWidth: 1, borderColor: '#00598B' }}
                    title="เปิดซ่อมใหม่"
                    titleStyle={{ color: '#fff', fontSize: 18, fontFamily: "Prompt-Bold" }}
                    onPress={() => {
                        navigate('ChooseRepair')
                    }
                    }>
                </Button>


                <Button
                    buttonStyle={{ backgroundColor: "#563860", height: 60, width: 360, alignSelf: "center", marginTop: 12, borderRadius: 30, borderWidth: 1, borderColor: '#563860' }}
                    title="ทำงานต่อ"
                    titleStyle={{ color: '#fff', fontSize: 18, fontFamily: "Prompt-Bold" }}
                    onPress={() => {
                        AsyncStorage.setItem('repair_type_list', '1');
                        AsyncStorage.setItem('repair_type_name', name);
                        AsyncStorage.setItem('repair_type_department', department);
                        AsyncStorage.setItem('value_type', "continue");
                        navigate('ListDetailRepair')
                    }}>
                </Button>




                <Button
                    buttonStyle={{ backgroundColor: "#525252", height: 60, width: 360, alignSelf: "center", marginTop: 12, borderRadius: 30, borderWidth: 1, borderColor: '#525252' }}
                    title="ประวัติการทำงาน"
                    titleStyle={{ color: '#fff', fontSize: 18, fontFamily: "Prompt-Bold" }}
                    onPress={() => {
                        AsyncStorage.setItem('repair_type_list', 'null');
                        AsyncStorage.setItem('repair_type_name', name);
                        AsyncStorage.setItem('repair_type_department', department);
                        AsyncStorage.setItem('value_type', "history");
                        navigate('ListDetailRepair')
                    }}>
                </Button>









            </View>




        </View>
    );
}

Detail.navigationOptions = ({ navigation }) => ({
    title: 'รายละเอียด',
    headerBackTitleVisible: false,
    headerTitleStyle: { color: '#fff', fontFamily: 'Prompt-Bold' },
    headerBackTitleStyle: { color: '#fff', fontFamily: 'Prompt-Bold' },
    headerStyle: { backgroundColor: '#3E5481' },
    headerTintColor: 'white',
})

const styles = StyleSheet.create({
    MainContainer: {
        justifyContent: 'center',
        flex: 1,
        backgroundColor: '#FFF',
        flexDirection: "column",
    },
    loadding_style: {
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
        backgroundColor: '#fff',
    },
    appleBtn: { height: 44, width: 320, marginTop: 16 },
    spinnerTextStyle: {
        color: '#FFF',
        fontFamily: "NotoSans-Regular",
        fontSize: 14
    },
    button: {
        marginTop: 24,
        backgroundColor: "#e42179",
        width: "100%",
    },
    stretch: {
        width: '100%',
        height: 400,
        resizeMode: "stretch",
        justifyContent: 'center',
        alignItems: 'center'
    },
    defaultText: {
        fontSize: 16,
        color: '#000'
    },

});

export default Detail