import React, { useEffect, useState } from 'react';
import { StyleSheet, View, Text, Keyboard, Alert, Image, ActivityIndicator, AsyncStorage, Platform, StatusBar, Linking, TouchableOpacity } from 'react-native';
import { Button, Input, Overlay } from "react-native-elements";
import { useNavigation, useNavigationParam } from 'react-navigation-hooks'
import axios from 'axios';
import QRCodeScanner from 'react-native-qrcode-scanner';
import { RNCamera } from 'react-native-camera';
import SearchableDropdown from 'react-native-searchable-dropdown';

const Barcode = () => {
    const { navigate } = useNavigation();
    const [listData, setListData] = useState([]);
    const [dialogVisible, setDialogVisible] = useState(false);
    const [name, setName] = useState('กรุณาเลือกเครื่องจักร')
    const [id, setId] = useState(0)
    const [token, setToken] = useState('')


    useEffect(() => {
        async function fetchMyAPI() {
            setToken(await AsyncStorage.getItem('token'))
            callGetMachineUnitList(await AsyncStorage.getItem('token'))
        }
        fetchMyAPI()
    }, [])



    callGetMachineUnitList = async (token) => {
        axios.get('http://apitpm.bsisugarcane.com/api/MaintenanceApp/api/MaintenanceApp/GetMachineUnitList?token=' + token)
            .then((response) => {


                var list = []
                var objectData = {}
                var i = 0
                console.log('response first : ', response.data.Results)
                for (i = 0; i < response.data.Results.length; i++) {
                    objectData = {
                        name: response.data.Results[i].Name,
                        id: response.data.Results[i].Id,
                    }
                    list.push(objectData)
                }


                setListData(list)
            })
            .catch((error) => {
            })
            .finally(function () {
            });

    };



    callPostQRCodeProblem = async (token, MachineUnitId) => {
        axios.post('http://apitpm.bsisugarcane.com/api/MaintenanceApp/api/MaintenanceApp/PostQRCodeProblem?token=' + token + '&MachineUnitId=' + MachineUnitId)
            .then((response) => {
                console.log('data success : ', response.data.Messages)
                if (response.data.Messages == "create successfully.") {
                    AsyncStorage.setItem('result_id', MachineUnitId);
                    navigate('DetailRepair')
                } else {
                    showAlert('ข้อความ', 'ไม่มีรหัสนี้ในระบบ')
                }
            })
            .catch((error) => {
                console.log('error success : ', error)
                showAlert('ข้อความ', 'ไม่มีรหัสนี้ในระบบ')
            })
            .finally(function () {
            });

    };

    onSuccess = e => {
        callPostQRCodeProblem(token, e.data)
        // var i = 0
        // var sum = 0
        // for (i = 0; i < listData.length; i++) {
        //     if (listData[i].id == e.data) {
        //         sum += 1
        //     }
        // }

        // if (sum == 1) {
        //     AsyncStorage.setItem('result_id', e.data);
        //     navigate('DetailRepair')
        // } else {
        //     showAlert('ข้อความ', 'ไม่มีรหัสนี้ในระบบ')
        // }
    };



    showAlert = (title, message) => {
        Alert.alert(
            title,
            message,
            [
                {
                    text: 'OK', onPress: () => {
                    }

                },
            ],
            { cancelable: false },
        );
    }

    var popupProblemQrCode = (
        <Overlay fullScreen isVisible={dialogVisible} overlayStyle={{ flex: 1, backgroundColor: '#FFF', width: '100%', borderRadius: 24 }} onBackdropPress={() => console.log('yahoo')} >
            <View style={{ alignItems: 'center', justifyContent: 'center', flexdirection : 'column' }}>

           
                <View style={{height : '85%' , width : '100%' , alignItems: 'center',}}>

                

                <Text style={{
                    color: '#3E5481',
                    fontFamily: "Prompt-Bold",
                    fontSize: 24,
                    marginTop: 48
                }}>
                    รายงานปัญหา QR Code
                </Text>



                <SearchableDropdown
                    onItemSelect={(item) => {
                        AsyncStorage.setItem('result_id', item.id.toString());
                        setName(item.name)
                        setId(item.id)
                    }}
                    containerStyle={{ marginTop: 24, width: '90%' , height: 240}}
                    itemStyle={{
                        padding: 10,
                        backgroundColor: '#FFF',
                        borderColor: '#EFEFEF',
                        borderWidth: 1,
                        fontFamily: "Prompt-Regular",
                    }}
                    itemTextStyle={{ color: '#222', fontFamily: "Prompt-Regular" }}
                    items={listData}
                    resetValue={false}
                    textInputProps={
                        {
                            placeholder: name,
                            placeholderTextColor:"#000",
                            fontFamily: "Prompt-Regular",
                            underlineColorAndroid: "transparent",
                            style: {
                                padding: 12,
                                borderWidth: 1,
                                borderColor: '#ccc',
                                borderRadius: 8,
                            },
                            onTextChange: text => console.log('text : ', text)
                        }
                    }

                />
</View>
               


                <View style={{ flexDirection: 'row', height: '15%' }} >

                    <View style={{ flex: 0.5, flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }} >
                        <Button
                            buttonStyle={{ backgroundColor: "#F4F5F7", height: 50, width: 150, alignSelf: "center", marginLeft: 8, marginRight: 8, borderRadius: 25, borderWidth: 1, borderColor: '#F4F5F7' }}
                            title="ยกเลิก"
                            titleStyle={{ color: '#2E3E5C', fontSize: 18, fontFamily: "Prompt-Bold" }}
                            onPress={() =>
                                setDialogVisible(false)

                            }>
                        </Button>

                    </View>





                    <View style={{ flex: 0.5, flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }} >





                        <Button
                            buttonStyle={{ backgroundColor: "#4DB6AC", height: 50, width: 150, alignSelf: "center", marginLeft: 8, marginRight: 8, borderRadius: 25, borderWidth: 1, borderColor: '#4DB6AC' }}
                            title="ยืนยัน"
                            titleStyle={{ color: '#FFF', fontSize: 18, fontFamily: "Prompt-Bold" }}
                            onPress={() => {
                                navigate('DetailRepair')
                                setDialogVisible(false)
                            }

                            }>
                        </Button>

                    </View>


                </View>


            </View>











        </Overlay>

    )
    return (
        <View style={styles.MainContainer}>
            <StatusBar barStyle="light-content" />

            {popupProblemQrCode}

            <QRCodeScanner
                onRead={this.onSuccess}
                topContent={
                    <Text style={styles.centerText}>
                        สเเกน QR Code
                    </Text>
                }
                bottomContent={
                    <TouchableOpacity>
                        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }} >
                            {/* <View
                                style={{ backgroundColor: "#4F4F4F", height: 60, width: 340, justifyContent: "center", alignSelf: "center", marginTop: 48, borderRadius: 30, borderWidth: 1, borderColor: '#4F4F4F', flexDirection: 'row' }}
                            >
                                <Image
                                    style={{ width: 26, height: 26, alignSelf: 'center' }}
                                    source={require('../../img/barcode.png')}
                                ></Image>

                                <Text style={[styles.baseText, { marginTop: 4, marginLeft: 16, color: "#FFF", fontFamily: "Prompt-Regular", fontSize: 18, alignSelf: 'center' }]}>Scan QRCode</Text>

                            </View> */}



                            <TouchableOpacity onPress={() => { setDialogVisible(true) }}>
                                <View
                                    style={{ backgroundColor: "#4F4F4F", height: 60, width: 340, justifyContent: "center", alignSelf: "center", marginTop: 24, borderRadius: 30, borderWidth: 1, borderColor: '#4F4F4F', flexDirection: 'row' }}
                                >

                                    <Image
                                        style={{ width: 26, height: 26, alignSelf: 'center' }}
                                        source={require('../../img/warning.png')}
                                    ></Image>

                                    <Text style={[styles.baseText, { marginTop: 4, marginLeft: 16, color: "#FFF", fontFamily: "Prompt-Regular", fontSize: 18, alignSelf: 'center' }]}>ไม่สามารแสกนได้</Text>

                                </View>
                            </TouchableOpacity>
                        </View>
                    </TouchableOpacity>
                }
            />











        </View>
    );
}


Barcode.navigationOptions = ({ navigation }) => ({
    title: 'QR Code',
    headerBackTitleVisible: false,
    headerTitleStyle: { color: '#fff', fontFamily: 'Prompt-Bold' },
    headerBackTitleStyle: { color: '#fff', fontFamily: 'Prompt-Bold' },
    headerStyle: { backgroundColor: '#3E5481' },
    headerTintColor: 'white',
})

const styles = StyleSheet.create({
    MainContainer: {
        flex: 1,
        backgroundColor: '#000',
        opacity: 0.9
    },
    loadding_style: {
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
        backgroundColor: '#fff',
    },
    appleBtn: { height: 44, width: 320, marginTop: 16 },
    button: {
        marginTop: 24,
        backgroundColor: "#e42179",
        width: "100%",
    },
    stretch: {
        width: 200,
        height: 200,
        resizeMode: "stretch",
        justifyContent: 'center',
        alignItems: 'center'
    },
    centerText: {
        color: '#FFF',
        fontFamily: "Prompt-Regular",
        fontSize: 32
    }

});

export default Barcode