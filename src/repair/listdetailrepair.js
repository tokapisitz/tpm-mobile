import React, { useState, useCallback, useMemo, useEffect } from 'react';
import { StyleSheet, View, Text, Keyboard, Alert, ScrollView, Image, ActivityIndicator, AsyncStorage, Platform, StatusBar, Linking, TouchableOpacity } from 'react-native';
import { Button, Input } from "react-native-elements";
import { useNavigation, useNavigationParam } from 'react-navigation-hooks'
import Ionicons from "react-native-vector-icons/FontAwesome5";
import axios from 'axios';

const ListDetailRepair = () => {
    const { navigate } = useNavigation();
    const [listData, setListData] = useState([]);
    const [name, setName] = useState('');
    const [nameTwo, setNameTwo] = useState('');
    const [type, setType] = useState('');
    const [value_type, setValueType] = useState('');

    //const [pos, setPos] = useState('');
    useEffect(() => {
        async function fetchMyAPI() {
            var result_id = await AsyncStorage.getItem('result_id')
            callGetRepairList(await AsyncStorage.getItem('token'), parseInt(await AsyncStorage.getItem('repair_type_id')), await AsyncStorage.getItem('repair_type_list'), result_id)
            setName(await AsyncStorage.getItem('repair_type_name'))
            setNameTwo(await AsyncStorage.getItem('repair_type_head_2'))
            setType(await AsyncStorage.getItem('repair_type_list'))
            setValueType(await AsyncStorage.getItem('value_type'))
        }
        fetchMyAPI()
    }, [])



    callGetRepairList = async (token, id, type, result_id) => {
        var type_result = ""
        if (type == 'null') {
            type_result = '1'
        } else {
            type_result = '0'
        }

        console.log('1234 : ', 'http://apitpm.bsisugarcane.com/api/MaintenanceApp/api/MaintenanceApp/GetRepairList?token=' + token + '&MachineUnitID=' + result_id + '&status=' + type_result)
        axios.get('http://apitpm.bsisugarcane.com/api/MaintenanceApp/api/MaintenanceApp/GetRepairList?token=' + token + '&MachineUnitID=' + result_id + '&status=' + type_result)
            .then((response) => {


                //  console.log('data 1 : ', response.data.Results[pos].RepairHistoryLogs)
                var list = []
                var objectData = {}
                var i = 0
                var j = 0
                var index = 0


                for (j = 0; j < response.data.Results.length; j++) {
                    if (response.data.Results[j].Id == id) {
                        index = j
                    }
                }

                for (i = 0; i < response.data.Results[index].RepairHistoryLogs.length; i++) {
                    objectData = {
                        name: response.data.Results[index].RepairHistoryLogs[i].Topic,
                        type: response.data.Results[index].RepairHistoryLogs[i].StatusText,
                        date: response.data.Results[index].RepairHistoryLogs[i].CreatedDateText,
                        id: response.data.Results[index].RepairHistoryLogs[i].Id,
                    }
                    list.push(objectData)
                }

                console.log('response success : ', 'EP ' + (list.length + 1))
                setListData(list)
            })
            .catch((error) => {
                console.log('response error : ', error)
                //  showAlert('ข้อความ', 'เข้าสู่ระบบไม่สำเร็จ กรุณาตรวจสอบ')
            })
            .finally(function () {
            });

    };
    return (
        <View style={styles.MainContainer}>
            {/* <View style={{
                flex: 0.4, justifyContent: 'center',
                alignItems: 'center',
            }} >
                <Image
                    style={styles.stretch}
                    source={require('../img/logo.png')}
                ></Image>
            </View> */}
            <View style={{ flex: 1 }} >


                <View style={{ flexDirection: 'row', alignItems: 'center' }}>

                    <View style={{ flex: 0.95 }}>



                        <Text style={[styles.baseText, { color: "#3E5481", fontFamily: "Prompt-Bold", fontSize: 36, marginLeft: 24 }]}>{nameTwo}</Text>


                    </View>

                    <TouchableOpacity
                        onPress={() => {
                            AsyncStorage.setItem('type_crate', 'continue');
                            AsyncStorage.setItem('topic_name', nameTwo + ' EP ' + (listData.length + 1));
                            navigate('AddDetailRepair')
                        }
                        }
                    >

                        {type == 'null' ?
                            <View>
                            </View>
                            :
                            <Image
                                style={{ width: 36, height: 36 }}
                                source={require('../../img/add_repair.png')}
                            ></Image>

                        }


                    </TouchableOpacity>

                </View>



                <Text style={[styles.baseText, { color: "#9FA5C0", fontFamily: "Prompt-Regular", fontSize: 16, marginLeft: 24 }]}>{name}</Text>



                <View style={{ width: '100%', height: 1, backgroundColor: '#EFEFEF', marginTop: 24 }} />


                <ScrollView style={{ marginBottom: 24 }}>
                    {listData.map((item, index) => {
                        return (


                            <View style={{ flex: 1, flexDirection: 'row', borderBottomWidth: 1, borderColor: "#EFEFEF", backgroundColor: '#FFF' }}>
                                <View style={{ flex: 0.9 }}>

                                    <View style={{ flex: .5, flexDirection: 'row', marginLeft: 16, marginTop: 12 }}>
                                        <Text style={{ fontFamily: 'Prompt-Bold', color: '#3E5481', fontSize: 24 }}>{item.name}</Text>



                                    </View>
                                    <View style={{ flex: .5, flexDirection: 'row', marginLeft: 16 }}>

                                        <Text style={{ fontFamily: 'Prompt-Bold', color: '#D1D1D3', fontSize: 14 }}>{item.date}</Text>

                                        {/* <Icon name={item.funding_source_details == null ? '' : 'credit-card'} size={12} style={{ marginTop: 2 }}> </Icon> */}

                                    </View>

                                    <View style={{ flex: .5, flexDirection: 'row', marginLeft: 16, marginBottom: 12 }}>

                                        <Text style={{ fontFamily: 'Prompt-Bold', color: '#D1D1D3', fontSize: 14 }}>{item.type}</Text>

                                        {/* <Icon name={item.funding_source_details == null ? '' : 'credit-card'} size={12} style={{ marginTop: 2 }}> </Icon> */}

                                    </View>

                                </View>
                                <View style={{ flex: 0.1, justifyContent: 'center' }}>
                                    <TouchableOpacity
                                        onPress={() => {
                                            AsyncStorage.setItem('detail_id', item.id.toString());
                                            if (value_type == "history") {
                                                navigate('History')
                                            } else {
                                                navigate('Continue')
                                            }

                                        }
                                        }
                                    >


                                        <Image

                                            source={require('../../img/arrow_repair.png')}
                                        ></Image>

                                    </TouchableOpacity>

                                </View>
                            </View>

                        )

                    }

                    )}
                </ScrollView>








            </View>




        </View>
    );
}

ListDetailRepair.navigationOptions = ({ navigation }) => ({
    title: 'รายละเอียด',
    headerBackTitleVisible: false,
    headerTitleStyle: { color: '#fff', fontFamily: 'Prompt-Bold' },
    headerBackTitleStyle: { color: '#fff', fontFamily: 'Prompt-Bold' },
    headerStyle: { backgroundColor: '#3E5481' },
    headerTintColor: 'white',
})

const styles = StyleSheet.create({
    MainContainer: {
        justifyContent: 'center',
        flex: 1,
        backgroundColor: '#FFF',
        flexDirection: "column",
    },
    loadding_style: {
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
        backgroundColor: '#fff',
    },
    appleBtn: { height: 44, width: 320, marginTop: 16 },
    spinnerTextStyle: {
        color: '#FFF',
        fontFamily: "NotoSans-Regular",
        fontSize: 14
    },
    button: {
        marginTop: 24,
        backgroundColor: "#e42179",
        width: "100%",
    },
    stretch: {
        resizeMode: "stretch",
        justifyContent: 'center',
        alignItems: 'center'
    },
    defaultText: {
        fontSize: 16,
        color: '#000'
    },

});

export default ListDetailRepair