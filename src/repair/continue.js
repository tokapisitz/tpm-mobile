import React, { useState, useCallback, useMemo, useEffect } from 'react';
import { StyleSheet, View, Text, Keyboard, Alert, ScrollView, Image, ActivityIndicator, AsyncStorage, Dimensions, Platform, StatusBar, Linking, TouchableOpacity, TextInput } from 'react-native';
import { Button, Input, Overlay } from "react-native-elements";
import { useNavigation, useNavigationParam } from 'react-navigation-hooks'
import Ionicons from "react-native-vector-icons/FontAwesome5";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import axios from 'axios';
import SearchableDropdown from 'react-native-searchable-dropdown';
import ImagePicker from 'react-native-image-picker';



const options = {
    // title: 'Select Avatar',
    // customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
    quality: 1.0,
    maxWidth: 1400,
    maxHeight: 1400,
    storageOptions: {
        skipBackup: true,
        path: 'images',
    },
};


const Continue = () => {
    const { navigate } = useNavigation();
    const [name, setTopic] = useState('');
    const [token, setToken] = useState('');
    const [detail, setDetail] = useState('');
    const [countPerson, setCountPerson] = useState(0);
    const [countWork, setCountWork] = useState(0);


    const [nameMachine, setNameMachine] = useState('กรุณาเลือกส่วนประกอบเครื่องจักร')


    const [image_one, setImageOne] = useState('');
    const [image_two, setImageTwo] = useState('');
    const [image_three, setImageThree] = useState('');
    const [image_four, setImageFour] = useState('');

    const [partMachine, setPartMachine] = useState('');
    const [partIdMachine, setPartIdMachine] = useState('');
    const [changePart, setChangePart] = useState(0);
    const [listAsset, setListAsset] = useState([]);
    const [listAssetResult, setListAssetResult] = useState([]);
    const [listAssetDialog, setListAssetDialog] = useState([{ name: '', count: 0 }]);


    const [open_asset, setOpenAsset] = useState(0);
    const [open_machine, setOpenMachine] = useState(0);


    const [dialogAssetVisible, setDialogVisibleAsset] = useState(false);
    const [dialogMachineVisible, setDialogVisibleMachine] = useState(false);
    const [dialogQuestionVisible, setDialogVisibleQuestion] = useState(false);
    const [dialogWarningVisible, setDialogVisibleWarning] = useState(false);
    const [dialogConfirmVisible, setDialogVisibleConfirm] = useState(false);
    const [dialogCancelVisible, setDialogVisibleCancel] = useState(false);

    const [status, setStatus] = useState(false);

    const [type, setType] = useState(0);
    const [repairId, setRepairId] = useState(0);




    const [listData, setListData] = useState([]);
    //const [pos, setPos] = useState('');

    //  const [token, setToken] = useState('');
    const [repairTypeId, setRepairTypeId] = useState(0);
    const [repairTypeList, setRepairTypeList] = useState('');
    const [detailId, setDetailId] = useState(0);
    useEffect(() => {
        async function fetchMyAPI() {
            var result_id = await AsyncStorage.getItem('result_id')
            setType(parseInt(await AsyncStorage.getItem('detail_type')))
            setToken(await AsyncStorage.getItem('token'))
            setRepairTypeId(parseInt(await AsyncStorage.getItem('repair_type_id')))
            setRepairTypeList(await AsyncStorage.getItem('repair_type_list'))
            setDetailId(parseInt(await AsyncStorage.getItem('detail_id')))
            callGetRepairList(await AsyncStorage.getItem('token'), parseInt(await AsyncStorage.getItem('repair_type_id')), await AsyncStorage.getItem('repair_type_list'), result_id, parseInt(await AsyncStorage.getItem('detail_id')))
            callGetPartMachineList(await AsyncStorage.getItem('token'), result_id)
        }
        fetchMyAPI()
    }, [])


    {/* ชัวโมงการซ่อมบำรุง */ }
    onIncreaseWork = (count) => {
        var count_increase = count
        count_increase += 1
        setCountWork(count_increase)
    }

    onDecreaseWork = (count) => {
        if (count > 1) {
            var count_decrease = count
            count_decrease -= 1
            setCountWork(count_decrease)
        }
    }

    {/* จำนวนคน */ }
    onIncreasePerson = (count) => {
        var count_increase = count
        count_increase += 1
        setCountPerson(count_increase)
    }

    onDecreasePerson = (count) => {
        if (count > 1) {
            var count_decrease = count
            count_decrease -= 1
            setCountPerson(count_decrease)
        }
    }


    {/* เปลี่ยนส่วนประกอบเครื่องจักร */ }
    onIncreaseMachine = (count) => {
        var count_increase = count
        count_increase += 1
        setChangePart(count_increase)
    }

    onDecreaseMachine = (count) => {
        if (count > 1) {
            var count_decrease = count
            count_decrease -= 1
            setChangePart(count_decrease)
        }
    }


    {/* ชิ้นส่วนเครื่องจักร */ }
    onIncreaseListMachine = (count, pos) => {
        var count_increase = count
        count_increase += 1
        let newArr = [...listAsset];
        newArr[pos].Amount = count_increase;
        setListAsset(newArr)
    }

    onDecreaseListMachine = (count, pos) => {
        console.log('count : ', count)
        console.log('pos : ', pos)
        if (count > 1) {
            var count_decrease = count
            count_decrease -= 1
            let newArr = [...listAsset];
            newArr[pos].Amount = count_decrease;
            setListAsset(newArr)
        }
    }


    onOpenDropDownAsset = (count) => {
        var count_open = count
        count_open += 1
        setOpenAsset(count_open)
    }


    onOpenDropDownMachine = (count) => {

        var count_open = count
        count_open += 1
        setOpenMachine(count_open)
    }


    chooseImageFile = () => {
        ImagePicker.showImagePicker(options, (response) => {


            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            } else {
                //  setImage1(response.uri)
                // console.log('1 : ', 'data:image/jpeg;base64,' + response.data)
                // setBase64Image1('data:image/jpeg;base64,' + response.data)
                callWebUploadPicture(1, 'data:image/jpeg;base64,' + response.data)
            }
        });
    };


    chooseImageFileTwo = () => {
        ImagePicker.showImagePicker(options, (response) => {


            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            } else {
                //  setImage2(response.uri)
                // setBase64Image2('data:image/jpeg;base64,' + response.data)
                callWebUploadPicture(2, 'data:image/jpeg;base64,' + response.data)
            }
        });
    };



    chooseImageFileThree = () => {
        ImagePicker.showImagePicker(options, (response) => {


            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            } else {
                // setImage3(response.uri)
                // setBase64Image3('data:image/jpeg;base64,' + response.data)
                callWebUploadPicture(3, 'data:image/jpeg;base64,' + response.data)
            }
        });
    };



    chooseImageFileFour = () => {
        ImagePicker.showImagePicker(options, (response) => {


            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            } else {
                //  setImage4(response.uri)
                // setBase64Image4('data:image/jpeg;base64,' + response.data)
                callWebUploadPicture(4, 'data:image/jpeg;base64,' + response.data)
            }
        });
    };





    {/* ชิ้นส่วนเครื่องจักร */ }
    onIncreaseListMachineDialog = (count_Value, pos) => {
        var count_increase = count_Value
        count_increase += 1
        let newArr = [...listAssetDialog];
        newArr[pos].count = count_increase;
        setListAssetDialog(newArr)
    }

    onDecreaseListMachineDialog = (count_Value, pos) => {
        if (count_Value > 1) {
            var count_decrease = count_Value
            count_decrease -= 1
            let newArr = [...listAssetDialog];
            newArr[pos].count = count_decrease;
            setListAssetDialog(newArr)
        }
    }


    onChangeTextName = (value, pos) => {
        let newArr = [...listAssetDialog];
        newArr[pos].name = value;
        setListAssetDialog(newArr)
    }

    onChangeTextDetail = (value) => {
        setDetail(value)
    }





    callGetRepairList = async (token, id, type, result_id, detail_id) => {
        var type_result = ""
        if (type == 'null') {
            type_result = '1'
        } else {
            type_result = '0'
        }

        axios.get('http://apitpm.bsisugarcane.com/api/MaintenanceApp/api/MaintenanceApp/GetRepairList?token=' + token + '&MachineUnitID=' + result_id + '&status=' + type_result)
            .then((response) => {

                var list = []
                var objectData = {}
                var i = 0
                var j = 0
                var index = 0
                var k = 0
                var l = 0



                for (j = 0; j < response.data.Results.length; j++) {
                    if (response.data.Results[j].Id == id) {
                        index = j
                    }
                }

                for (i = 0; i < response.data.Results[index].RepairHistoryLogs.length; i++) {
                    if (response.data.Results[index].RepairHistoryLogs[i].Id == detail_id) {
                        console.log('data : ', response.data.Results[index].RepairHistoryLogs[i])
                        setTopic(response.data.Results[index].RepairHistoryLogs[i].Topic)
                        setDetail(response.data.Results[index].RepairHistoryLogs[i].Detail)
                        setCountPerson(response.data.Results[index].RepairHistoryLogs[i].AmountPerson)
                        setCountWork(response.data.Results[index].RepairHistoryLogs[i].Hours)
                        setRepairId(response.data.Results[index].RepairHistoryLogs[i].Id)
                        var imageOne = ""
                        if (response.data.Results[index].RepairHistoryLogs[i].Image1 == null) {
                            imageOne = ""
                        } else {
                            imageOne = response.data.Results[index].RepairHistoryLogs[i].Image1
                        }
                        setImageOne(imageOne)

                        var imageTwo = ""
                        if (response.data.Results[index].RepairHistoryLogs[i].Image2 == null) {
                            imageTwo = ""
                        } else {
                            imageTwo = response.data.Results[index].RepairHistoryLogs[i].Image2
                        }
                        setImageTwo(imageTwo)

                        var imageThree = ""
                        if (response.data.Results[index].RepairHistoryLogs[i].Image3 == null) {
                            imageThree = ""
                        } else {
                            imageThree = response.data.Results[index].RepairHistoryLogs[i].Image3
                        }
                        setImageThree(imageThree)

                        var imageFour = ""
                        if (response.data.Results[index].RepairHistoryLogs[i].Image4 == null) {
                            imageFour = ""
                        } else {
                            imageFour = response.data.Results[index].RepairHistoryLogs[i].Image4
                        }
                        setImageFour(imageFour)


                        setPartMachine(response.data.Results[index].RepairHistoryLogs[i].PartMachine)
                        setChangePart(response.data.Results[index].RepairHistoryLogs[i].ChangePart)

                        for (k = 0; k < response.data.Results[index].RepairHistoryLogs[i].IsWastingAssetLogs.length; k++) {
                            objectData = {
                                Name: response.data.Results[index].RepairHistoryLogs[i].IsWastingAssetLogs[k].Name,
                                Amount: response.data.Results[index].RepairHistoryLogs[i].IsWastingAssetLogs[k].Amount,
                                Id: response.data.Results[index].RepairHistoryLogs[i].IsWastingAssetLogs[k].WastingAssetID,
                            }
                            list.push(objectData)

                        }



                    }
                }

                setListAssetResult(list)
                callGetWastingAssetList(token)
            })
            .catch((error) => {
                console.log('response error : ', error)
            })
            .finally(function () {
            });

    };


    callGetPartMachineList = async (token, machine_id) => {
        console.log('1234 : ', 'http://apitpm.bsisugarcane.com/api/MaintenanceApp/api/MaintenanceApp/GetPartMachineList?token=' + token + '&MachineUnitID=' + machine_id)
        axios.get('http://apitpm.bsisugarcane.com/api/MaintenanceApp/api/MaintenanceApp/GetPartMachineList?token=' + token + '&MachineUnitID=' + machine_id)
            .then((response) => {


                var list = []
                var objectData = {}
                var i = 0
                console.log('response first : ', response.data.Results)
                for (i = 0; i < response.data.Results.length; i++) {
                    objectData = {
                        name: response.data.Results[i].PartName,
                        id: response.data.Results[i].Id,
                    }
                    list.push(objectData)
                }


                setListData(list)
            })
            .catch((error) => {
            })
            .finally(function () {
            });

    };

    callGetWastingAssetList = async (token) => {
        axios.get('http://apitpm.bsisugarcane.com/api/MaintenanceApp/api/MaintenanceApp/GetWastingAssetList?token=' + token)
            .then((response) => {


                var list = []
                var objectData = {}
                var i = 0
                var l = 0
                // console.log('response first 1234 : ', response.data.Results)
                for (i = 0; i < response.data.Results.length; i++) {
                    for (l = 0; l < listAssetResult.length; l++) {
                        if (response.data.Results[i].Id == listAssetResult[l].Id) {

                            objectData = {
                                Name: response.data.Results[i].Name,
                                Amount: listAssetResult[l].Amount,
                            }
                            list.push(objectData)

                        }
                    }
                }

                console.log('name : ', list)
                setListAsset(list)
            })
            .catch((error) => {
                //  console.log('error first 1234 : ', error)
            })
            .finally(function () {
            });

    };


    callWebUploadPicture = async (type_data, image_data) => {

        const data = {
            type: type_data,
            image: image_data
        }

        axios.post('http://apitpm.bsisugarcane.com/api/MaintenanceApp/api/MaintenanceApp/webUploadPicture', data)
            .then((response) => {


                console.log('data : ', response.data.Results.ImagePath)
                if (type_data == 1) {
                    setImageOne(response.data.Results.ImagePath)
                } else if (type_data == 2) {
                    setImageTwo(response.data.Results.ImagePath)
                } else if (type_data == 3) {
                    setImageThree(response.data.Results.ImagePath)
                } else if (type_data == 4) {
                    setImageFour(response.data.Results.ImagePath)
                }
            })
            .catch((error) => {
            })
            .finally(function () {
            });

    };

    callPostInsertRepairLog = async () => {
        const data = {
            repairID: repairId,
            partMachineID: partIdMachine,
            status: status,
            changePart: changePart,
            detail: detail,
            base64Image1: image_one,
            base64Image2: image_two,
            base64Image3: image_three,
            base64Image4: image_four,
            topic: name,
            hours: countWork,
            amounttPerson: countPerson,
            type: type,
            isWastingAsset: listAsset
        }

        console.log('data send : ', data)
        console.log('data send token : ', token)

        axios.post('http://apitpm.bsisugarcane.com/api/MaintenanceApp/api/MaintenanceApp/PostInsertRepairLog?token=' + token, data)
            .then((response) => {
                if (response.data.Messages == "create successfully.") {
                    setDialogVisibleWarning(false)
                    setDialogVisibleConfirm(true)
                } else {
                    setDialogVisibleWarning(false)
                    setDialogVisibleCancel(true)
                }


            })
            .catch((error) => {
                console.log('error result : ', error)
            })
            .finally(function () {
            });

    };


    var popupMachine = (
        <Overlay fullScreen isVisible={dialogMachineVisible} overlayStyle={{ flex: 0.45, backgroundColor: '#FFF', width: '80%', borderRadius: 24 }} onBackdropPress={() => console.log('yahoo')} >
            <View style={{ alignItems: 'center', justifyContent: 'center', }}>

                <Text style={{
                    color: '#3E5481',
                    fontFamily: "Prompt-Bold",
                    fontSize: 24,
                    marginTop: 24
                }}>
                    ส่วนประกอบเครื่องจักร
                </Text>



                <SearchableDropdown
                    onItemSelect={(item) => {
                        console.log('name : ', item.name)
                        setNameMachine(item.name)
                        setPartIdMachine(item.id)
                    }}
                    containerStyle={{ marginTop: 24, width: '90%' }}
                    itemStyle={{
                        padding: 10,
                        backgroundColor: '#FFF',
                        borderColor: '#EFEFEF',
                        borderWidth: 1,
                        fontFamily: "Prompt-Regular",
                    }}
                    itemTextStyle={{ color: '#222', fontFamily: "Prompt-Regular" }}
                    items={listData}
                    resetValue={false}
                    textInputProps={
                        {
                            placeholder: nameMachine,
                            fontFamily: "Prompt-Regular",
                            underlineColorAndroid: "transparent",
                            style: {
                                padding: 12,
                                borderWidth: 1,
                                borderColor: '#ccc',
                                borderRadius: 8,
                            },
                            onTextChange: text => console.log('text : ', text)
                        }
                    }

                />

                <View style={{ flexDirection: 'row', height: 50, marginLeft: 24, marginRight: 24, marginTop: 24 }} >

                    <View style={{ flex: 0.6, flexDirection: 'row', alignItems: 'center' }} >
                        <Text style={{ color: "#3E5481", fontFamily: "Prompt-Bold", fontSize: 22 }}>จำนวน</Text>

                    </View>



                    <View style={{ flex: 0.1, flexDirection: 'row', alignItems: 'center' }} >
                        <TouchableOpacity
                            onPress={() => onDecreaseMachine(changePart)
                            } >
                            <Image

                                source={require('../../img/reduce.png')}
                            ></Image>

                        </TouchableOpacity>
                    </View>
                    <View style={{ flex: 0.2, flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }} >


                        <Text style={{ color: "#3E5481", fontFamily: "Prompt-Regular", fontSize: 20 }}>{changePart}</Text>
                    </View>

                    <View style={{ flex: 0.1, flexDirection: 'row', alignItems: 'center' }} >
                        <TouchableOpacity
                            onPress={() => onIncreaseMachine(changePart)
                            } >
                            <Image

                                source={require('../../img/increase.png')}
                            ></Image>
                        </TouchableOpacity>
                    </View>


                </View>




                <View style={{ flexDirection: 'row', height: 50, marginTop: 24 }} >

                    <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }} >





                        <Button
                            buttonStyle={{ backgroundColor: "#4DB6AC", height: 50, width: 300, alignSelf: "center", marginLeft: 8, marginRight: 8, borderRadius: 25, borderWidth: 1, borderColor: '#4DB6AC' }}
                            title="ยืนยัน"
                            titleStyle={{ color: '#FFF', fontSize: 18, fontFamily: "Prompt-Bold" }}
                            onPress={() => {
                                if (changePart != 0) {
                                    setDialogVisibleMachine(false)
                                    setPartMachine(nameMachine)
                                }
                            }

                            }>
                        </Button>

                    </View>










                </View>


                <View style={{ flexDirection: 'row', height: 50, marginTop: 24 }} >
                    <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }} >
                        <Button
                            buttonStyle={{ backgroundColor: "#F4F5F7", height: 50, width: 300, alignSelf: "center", marginLeft: 8, marginRight: 8, borderRadius: 25, borderWidth: 1, borderColor: '#F4F5F7' }}
                            title="ยกเลิก"
                            titleStyle={{ color: '#2E3E5C', fontSize: 18, fontFamily: "Prompt-Bold" }}
                            onPress={() => {
                                setDialogVisibleMachine(false)
                                setPartMachine('-')
                            }
                            }>
                        </Button>

                    </View>

                </View>


            </View>











        </Overlay>

    )



    var popupAsset = (
        <Overlay fullScreen isVisible={dialogAssetVisible} overlayStyle={{ flex: 0.8, backgroundColor: '#FFF', width: '80%', borderRadius: 24 }} onBackdropPress={() => console.log('yahoo')} >
            <View style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }}>




                <View style={{ flex: 0.6, alignItems: 'center' }}>


                    <Text style={{
                        color: '#3E5481',
                        fontFamily: "Prompt-Bold",
                        fontSize: 24
                    }}>
                        เปลี่ยนชิ้นส่วนเครื่องจักร
                </Text>


                    {/* {listAsset.map((item, index) => {
                                        return ( */}

                    <KeyboardAwareScrollView extraHeight={screenWidth - 200}  >

                        {listAssetDialog.map((item, index) => {
                            return (
                                <View style={{ flexDirection: 'column', width: 300 }} >

                                    <Input
                                        overflow="hidden"
                                        keyboardAppearance="dark"
                                        placeholder="กรุณากรอกชิ้นส่วน"
                                        inputContainerStyle={{ backgroundColor: "#FFF", borderColor: '#D0DBEA', borderRadius: 8, paddingLeft: 8, borderWidth: 1, height: 50, marginTop: 24 }}
                                        inputStyle={{ fontSize: 15, fontFamily: "Prompt-Regular" }}
                                        onChangeText={(value) => {
                                            onChangeTextName(value, index)
                                        }}
                                    />



                                    <View style={{
                                        width: 300
                                    }}>





                                        <View style={{ flexDirection: 'row', height: 50, marginLeft: 24, marginRight: 24 }} >

                                            <View style={{ flex: 0.6, flexDirection: 'row', alignItems: 'center' }} >
                                                <Text style={{ color: "#3E5481", fontFamily: "Prompt-Bold", fontSize: 22 }}>จำนวน</Text>

                                            </View>



                                            <View style={{ flex: 0.1, flexDirection: 'row', alignItems: 'center' }} >
                                                <TouchableOpacity
                                                    onPress={() => onDecreaseListMachineDialog(item.count, index)
                                                    } >
                                                    <Image

                                                        source={require('../../img/reduce.png')}
                                                    ></Image>

                                                </TouchableOpacity>
                                            </View>
                                            <View style={{ flex: 0.2, flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }} >


                                                <Text style={{ color: "#3E5481", fontFamily: "Prompt-Regular", fontSize: 20 }}>{item.count}</Text>
                                            </View>

                                            <View style={{ flex: 0.1, flexDirection: 'row', alignItems: 'center' }} >
                                                <TouchableOpacity
                                                    onPress={() => onIncreaseListMachineDialog(item.count, index)
                                                    } >
                                                    <Image

                                                        source={require('../../img/increase.png')}
                                                    ></Image>
                                                </TouchableOpacity>
                                            </View>


                                        </View>


                                    </View>

                                </View>
                            )

                        }
                        )}



                    </KeyboardAwareScrollView>

                </View>




            </View>











        </Overlay>

    )


    var popupQuestion = (
        <Overlay fullScreen isVisible={dialogQuestionVisible} overlayStyle={{ flex: 0.4, backgroundColor: '#FFF', width: '90%', borderRadius: 24 }} onBackdropPress={() => console.log('yahoo')} >
            <View style={{ alignItems: 'center', justifyContent: 'center', }}>



                <Image
                    style={{ width: 120, height: 120, marginTop: 24 }}
                    source={require('../../img/question.png')}
                ></Image>

                <Text style={{
                    color: '#3E5481',
                    fontFamily: "Prompt-Bold",
                    fontSize: 24,
                    marginTop: 24
                }}>
                    ต้องการปิดงานหรือไม่
                </Text>






                <View style={{ flexDirection: 'row', height: 50, marginTop: 48 }} >

                    <View style={{ flex: 0.5, flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }} >
                        <Button
                            buttonStyle={{ backgroundColor: "#F4F5F7", height: 50, width: 150, alignSelf: "center", marginLeft: 8, marginRight: 8, borderRadius: 25, borderWidth: 1, borderColor: '#F4F5F7' }}
                            title="ไม่ใช่"
                            titleStyle={{ color: '#2E3E5C', fontSize: 18, fontFamily: "Prompt-Bold" }}
                            onPress={() => {
                                setStatus(false)
                                setDialogVisibleQuestion(false)
                                setDialogVisibleWarning(true)
                            }
                            }>
                        </Button>

                    </View>





                    <View style={{ flex: 0.5, flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }} >





                        <Button
                            buttonStyle={{ backgroundColor: "#4DB6AC", height: 50, width: 150, alignSelf: "center", marginLeft: 8, marginRight: 8, borderRadius: 25, borderWidth: 1, borderColor: '#4DB6AC' }}
                            title="ใช่"
                            titleStyle={{ color: '#FFF', fontSize: 18, fontFamily: "Prompt-Bold" }}
                            onPress={() => {
                                setStatus(true)
                                setDialogVisibleQuestion(false)
                                setDialogVisibleWarning(true)
                                // navigate('DetailRepair')
                                // setDialogVisible(false)
                            }

                            }>
                        </Button>

                    </View>


                </View>


            </View>











        </Overlay>

    )


    var popupWarning = (
        <Overlay fullScreen isVisible={dialogWarningVisible} overlayStyle={{ flex: 0.6, backgroundColor: '#FFF', width: '90%', borderRadius: 24 }} onBackdropPress={() => console.log('yahoo')} >
            <View style={{ alignItems: 'center', justifyContent: 'center', }}>

                <Image
                    style={{ width: 120, height: 120, marginTop: 24 }}
                    source={require('../../img/warning_two.png')}
                ></Image>

                <Text style={{
                    color: '#3E5481',
                    fontFamily: "Prompt-Bold",
                    fontSize: 24,
                    marginTop: 24
                }}>
                    ยืนยันข้อมูล
                </Text>


                <View
                    style={{
                        marginTop: 24,

                    }}

                >


                    <Text style={{
                        color: '#2E3E5C',
                        fontFamily: "Prompt-Regular",
                        fontSize: 14,
                        textAlign: 'center',
                        marginLeft: 12,
                        marginRight: 12,


                    }}>
                        โปรดตรวจสอบข้อมูลของท่านก่อนทำการบันทึก หากตรวจสอบเรียบร้อยแล้ว กดปุ่ม “ยืนยัน” เพื่อให้ระบบทำรายการบันทึกข้อมูล
                </Text>

                </View>





                <View style={{ flexDirection: 'row', height: 50, marginTop: 36 }} >

                    <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }} >





                        <Button
                            buttonStyle={{ backgroundColor: "#4DB6AC", height: 50, width: 300, alignSelf: "center", marginLeft: 8, marginRight: 8, borderRadius: 25, borderWidth: 1, borderColor: '#4DB6AC' }}
                            title="ยืนยัน"
                            titleStyle={{ color: '#FFF', fontSize: 18, fontFamily: "Prompt-Bold" }}
                            onPress={() => {
                                setDialogVisibleWarning(false)
                                callPostInsertRepairLog()
                            }

                            }>
                        </Button>

                    </View>










                </View>


                <View style={{ flexDirection: 'row', height: 50, marginTop: 24 }} >
                    <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }} >
                        <Button
                            buttonStyle={{ backgroundColor: "#F4F5F7", height: 50, width: 300, alignSelf: "center", marginLeft: 8, marginRight: 8, borderRadius: 25, borderWidth: 1, borderColor: '#F4F5F7' }}
                            title="เเก้ไข"
                            titleStyle={{ color: '#2E3E5C', fontSize: 18, fontFamily: "Prompt-Bold" }}
                            onPress={() => {
                                setDialogVisibleWarning(false)
                            }
                            }>
                        </Button>

                    </View>

                </View>


            </View>











        </Overlay>

    )



    var popupConfirm = (
        <Overlay fullScreen isVisible={dialogConfirmVisible} overlayStyle={{ flex: 0.4, backgroundColor: '#FFF', width: '90%', borderRadius: 24 }} onBackdropPress={() => console.log('yahoo')} >
            <View style={{ alignItems: 'center', justifyContent: 'center', }}>

                <Image
                    style={{ width: 120, height: 120, marginTop: 24 }}
                    source={require('../../img/warning_two.png')}
                ></Image>

                <Text style={{
                    color: '#3E5481',
                    fontFamily: "Prompt-Bold",
                    fontSize: 24,
                    marginTop: 24
                }}>
                    ดำเนินการเสร็จสิ้น
                </Text>


                <View
                    style={{
                        marginTop: 24,

                    }}

                >


                    <Text style={{
                        color: '#2E3E5C',
                        fontFamily: "Prompt-Regular",
                        fontSize: 14,
                        textAlign: 'center',
                        marginLeft: 12,
                        marginRight: 12,


                    }}>
                        ข้อมูลของท่านถูกบันทึกลงระบบเรียบร้อยแล้ว
                </Text>

                </View>





                <View style={{ flexDirection: 'row', height: 50, marginTop: 36 }} >

                    <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }} >





                        <Button
                            buttonStyle={{ backgroundColor: "#4DB6AC", height: 50, width: 300, alignSelf: "center", marginLeft: 8, marginRight: 8, borderRadius: 25, borderWidth: 1, borderColor: '#4DB6AC' }}
                            title="กลับสู่รายการซ่อม"
                            titleStyle={{ color: '#FFF', fontSize: 18, fontFamily: "Prompt-Bold" }}
                            onPress={() => {
                                setDialogVisibleConfirm(false)
                                navigate('DetailRepair')
                            }

                            }>
                        </Button>

                    </View>










                </View>



            </View>











        </Overlay>

    )



    var popupCancel = (
        <Overlay fullScreen isVisible={dialogCancelVisible} overlayStyle={{ flex: 0.4, backgroundColor: '#FFF', width: '90%', borderRadius: 24 }} onBackdropPress={() => console.log('yahoo')} >
            <View style={{ alignItems: 'center', justifyContent: 'center', }}>

                <Image
                    style={{ width: 120, height: 120, marginTop: 24 }}
                    source={require('../../img/warning_two.png')}
                ></Image>

                <Text style={{
                    color: '#3E5481',
                    fontFamily: "Prompt-Bold",
                    fontSize: 24,
                    marginTop: 24
                }}>
                    ไม่สามารถบันทึกข้อมูล
                </Text>


                <View
                    style={{
                        marginTop: 24,

                    }}

                >


                    <Text style={{
                        color: '#2E3E5C',
                        fontFamily: "Prompt-Regular",
                        fontSize: 14,
                        textAlign: 'center',
                        marginLeft: 12,
                        marginRight: 12,


                    }}>
                        กรุณาตรวจสอบข้อมูลของท่านอีกครั้ง
                </Text>

                </View>





                <View style={{ flexDirection: 'row', height: 50, marginTop: 36 }} >

                    <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }} >





                        <Button
                            buttonStyle={{ backgroundColor: "#4DB6AC", height: 50, width: 300, alignSelf: "center", marginLeft: 8, marginRight: 8, borderRadius: 25, borderWidth: 1, borderColor: '#4DB6AC' }}
                            title="กลับสู่รายการซ่อม"
                            titleStyle={{ color: '#FFF', fontSize: 18, fontFamily: "Prompt-Bold" }}
                            onPress={() => {
                                setDialogVisibleCancel(false)
                            }

                            }>
                        </Button>

                    </View>










                </View>



            </View>











        </Overlay>

    )




    const { width: screenWidth } = Dimensions.get('window');

    return (
        <View style={styles.MainContainer}>

            {/* {popupMachine}
            {popupAsset}
            {popupQuestion}
            {popupWarning}
            {popupConfirm}
            {popupCancel} */}

            <ScrollView style={{ marginBottom: 48 }}>

                <View style={{ flex: 1, flexDirection: 'column' }} >


                    <Text style={[styles.baseText, { color: "#3E5481", fontFamily: "Prompt-Bold", fontSize: 36, marginTop: 12, marginLeft: 24 }]}>{name}</Text>

                    <View style={{
                        flexDirection: "row",
                        height: 80,
                        marginTop: 36,
                        justifyContent: 'center', alignItems: 'center'
                    }}>
                        <View style={{ flex: 0.4 }}>
                            {image_one == '' ?
                                <View style={{
                                    height: 140, justifyContent: 'center',
                                    alignItems: 'center',
                                    backgroundColor: '#EEF1F6',
                                    borderRadius: 20,
                                    marginRight: 12
                                }}>
                                    <Image
                                        style={{ width: 60, height: 50 }}
                                        source={require('../../img/camera_grey.png')}
                                    ></Image>

                                    <Text style={{ color: "#9FA5C0", fontFamily: "Prompt-Regular", fontSize: 15, marginLeft: 18, marginRight: 18, marginTop: 12 }}>ไม่มีรูปภาพ</Text>

                                </View>


                                :
                                <View style={{
                                    height: 140, justifyContent: 'center',
                                    alignItems: 'center',
                                    marginRight: 12
                                }}>
                                    <Image
                                        style={styles.imageShow}
                                        source={{ uri: image_one }}
                                    ></Image>

                                </View>
                            }
                        </View>
                        <View style={{ flex: 0.4 }}>
                            {image_two == '' ?
                                <View style={{
                                    height: 140, justifyContent: 'center',
                                    alignItems: 'center',
                                    backgroundColor: '#EEF1F6',
                                    borderRadius: 20,
                                    marginLeft: 12
                                }}>
                                    <Image
                                        style={{ width: 60, height: 50 }}
                                        source={require('../../img/camera_grey.png')}
                                    ></Image>

                                    <Text style={{ color: "#9FA5C0", fontFamily: "Prompt-Regular", fontSize: 15, marginLeft: 18, marginRight: 18, marginTop: 12 }}>ไม่มีรูปภาพ</Text>
                                </View>
                                :
                                <View style={{
                                    height: 140, justifyContent: 'center',
                                    alignItems: 'center',
                                    borderRadius: 20,
                                    marginRight: 12
                                }}>
                                    <Image
                                        style={styles.imageShow}
                                        source={{ uri: image_two }}
                                    ></Image>

                                </View>
                            }
                        </View>
                    </View>

                    <View style={{
                        flexDirection: "row",
                        height: 80,
                        marginTop: 72,
                        justifyContent: 'center', alignItems: 'center'
                    }}>
                        <View style={{ flex: 0.4 }}>
                            {image_three == '' ?
                                <View style={{
                                    height: 140, justifyContent: 'center',
                                    alignItems: 'center',
                                    backgroundColor: '#EEF1F6',
                                    borderRadius: 20,
                                    marginRight: 12
                                }}>
                                    <Image
                                        style={{ width: 60, height: 50 }}
                                        source={require('../../img/camera_grey.png')}
                                    ></Image>

                                    <Text style={{ color: "#9FA5C0", fontFamily: "Prompt-Regular", fontSize: 15, marginLeft: 18, marginRight: 18, marginTop: 12 }}>ไม่มีรูปภาพ</Text>

                                </View>
                                :
                                <View style={{
                                    height: 140, justifyContent: 'center',
                                    alignItems: 'center',
                                    borderRadius: 20,
                                    marginRight: 12
                                }}>
                                    <Image
                                        style={styles.imageShow}
                                        source={{ uri: image_three }}
                                    ></Image>

                                </View>

                            }
                        </View>
                        <View style={{ flex: 0.4 }}>
                            {image_four == '' ?
                                <View style={{
                                    height: 140, justifyContent: 'center',
                                    alignItems: 'center',
                                    backgroundColor: '#EEF1F6',
                                    borderRadius: 20,
                                    marginLeft: 12
                                }}>
                                    <Image
                                        style={{ width: 60, height: 50 }}
                                        source={require('../../img/camera_grey.png')}
                                    ></Image>

                                    <Text style={{ color: "#9FA5C0", fontFamily: "Prompt-Regular", fontSize: 15, marginLeft: 18, marginRight: 18, marginTop: 12 }}>ไม่มีรูปภาพ</Text>

                                </View>
                                :

                                <View style={{
                                    height: 140, justifyContent: 'center',
                                    alignItems: 'center',
                                    borderRadius: 20,
                                    marginRight: 12
                                }}>
                                    <Image
                                        style={styles.imageShow}
                                        source={{ uri: image_four }}
                                    ></Image>



                                </View>

                            }
                        </View>
                    </View>







                    <View style={{ flex: 1, flexDirection: 'column' }} >
                        <View style={{ flexDirection: 'row', borderRadius: 10, height: 50, marginTop: 48 }} >



                            <View style={{ flex: 0.85, flexDirection: 'row', alignItems: 'center' }} >

                                <Text style={{ color: "#3E5481", fontFamily: "Prompt-Bold", fontSize: 20, marginLeft: 24 }}>เปลี่ยนส่วนประกอบเครื่องจักร</Text>
                            </View>

                            <View style={{ flex: 0.15, flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }} >


                                {open_machine % 2 == 0 ?
                                    <TouchableOpacity
                                        onPress={() => onOpenDropDownMachine(open_machine)
                                        } >
                                        <Image

                                            source={require('../../img/arrow_up.png')}
                                        ></Image>
                                    </TouchableOpacity>

                                    :

                                    <TouchableOpacity
                                        onPress={() => onOpenDropDownMachine(open_machine)
                                        } >
                                        <Image

                                            source={require('../../img/arrow_down.png')}
                                        ></Image>
                                    </TouchableOpacity>
                                }


                            </View>

                        </View>




                        {open_machine % 2 == 0 ?
                            <View>
                            </View>
                            :
                            partMachine == "-" || partMachine == '' ?
                                <View>
                                </View>
                                :
                                <View style={{ flexDirection: 'row', marginLeft: 24, marginRight: 24 }} >

                                    <View style={{ flex: 0.8, flexDirection: 'row', alignItems: 'center' }} >
                                        <Text style={{ color: "#3E5481", fontFamily: "Prompt-Regular", fontSize: 18, marginLeft: 24 }}>{partMachine}</Text>

                                    </View>





                                    {/* <View style={{ flex: 0.15, flexDirection: 'row', alignItems: 'center' }} >
    
    
    
                                    <Text style={{ color: "#3E5481", fontFamily: "Prompt-Regular", fontSize: 20 }}>{changePart}</Text>
    
    
                                </View> */}






                                    <View style={{ flex: 0.1, flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end' }} >

                                        <Text style={{ color: "#3E5481", fontFamily: "Prompt-Regular", fontSize: 20, marginRight: 4 }}>{changePart}</Text>

                                    </View>


                                    <View style={{ flex: 0.1, flexDirection: 'row', alignItems: 'center' }} >
                                    </View>


                                </View>

                        }
                    </View>





























                    {/* เปลี่ยนชิ้นส่วนเครื่องจักร */}

                    <View style={{ flex: 1, flexDirection: 'column' }} >
                        <View style={{ flexDirection: 'row', borderRadius: 10, height: 50 }} >



                            <View style={{ flex: 0.85, flexDirection: 'row', alignItems: 'center' }} >

                                <Text style={{ color: "#3E5481", fontFamily: "Prompt-Bold", fontSize: 20, marginLeft: 24 }}>ชิ้นส่วนเครื่องจักร</Text>
                            </View>

                            <View style={{ flex: 0.15, flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }} >

                                {open_asset % 2 == 0 ?
                                    <TouchableOpacity
                                        onPress={() => onOpenDropDownAsset(open_asset)
                                        } >
                                        <Image

                                            source={require('../../img/arrow_up.png')}
                                        ></Image>
                                    </TouchableOpacity>
                                    :
                                    <TouchableOpacity
                                        onPress={() => onOpenDropDownAsset(open_asset)
                                        } >
                                        <Image

                                            source={require('../../img/arrow_down.png')}
                                        ></Image>
                                    </TouchableOpacity>

                                }

                            </View>


                        </View>







                        {open_asset % 2 == 0 ?
                            <View>
                            </View>

                            :

                            <View>
                                {listAsset.map((item, index) => {
                                    return (



                                        <View style={{ flexDirection: 'row', marginLeft: 24, marginRight: 24, marginBottom: 8 }} >





                                            <View style={{ flex: 0.8, flexDirection: 'row', alignItems: 'center' }} >
                                                <Text style={{ color: "#3E5481", fontFamily: "Prompt-Regular", fontSize: 18, marginLeft: 24 }}>{item.Name}</Text>

                                            </View>




                                            <View style={{ flex: 0.1, flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end' }} >

                                                <Text style={{ color: "#3E5481", fontFamily: "Prompt-Regular", fontSize: 20, marginRight: 4 }}>{item.Amount}</Text>

                                            </View>


                                            <View style={{ flex: 0.1, flexDirection: 'row', alignItems: 'center' }} >
                                            </View>







                                        </View>
                                    )
                                })
                                }

                            </View>

                        }


                    </View>










                    <View style={{ height: 1, backgroundColor: "#EFEFEF", width: '100%', marginTop: 6 }} >

                    </View>


                    <View style={{ flexDirection: 'row', height: 50, marginLeft: 24, marginRight: 24, marginTop: 6 }} >

                        <View style={{ flex: 0.8, flexDirection: 'row', alignItems: 'center' }} >
                            <Text style={{ color: "#3E5481", fontFamily: "Prompt-Regular", fontSize: 18, marginLeft: 24 }}>จำนวนคน</Text>

                        </View>





                        <View style={{ flex: 0.1, flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end' }} >
                            {/* <Image

                source={require('../../img/reduce.png')}
            ></Image> */}


                            <Text style={{ color: "#3E5481", fontFamily: "Prompt-Regular", fontSize: 20, marginRight: 4 }}>{countPerson}</Text>

                            {/* <Image

                source={require('../../img/increase.png')}
            ></Image> */}

                        </View>

                        <View style={{ flex: 0.1, flexDirection: 'row', alignItems: 'center' }} >

                            <Text style={{ color: "#3E5481", fontFamily: "Prompt-Regular", fontSize: 20 }}> คน.</Text>
                        </View>


                    </View>


                    <View style={{ height: 1, backgroundColor: "#EFEFEF", width: '100%', marginTop: 6 }} >

                    </View>


                    <View style={{ flexDirection: 'row', height: 50, marginLeft: 24, marginRight: 24, marginTop: 6 }} >

                        <View style={{ flex: 0.8, flexDirection: 'row', alignItems: 'center' }} >
                            <Text style={{ color: "#3E5481", fontFamily: "Prompt-Regular", fontSize: 18, marginLeft: 24 }}>ชั่วโมงการซ่อมบำรุง</Text>

                        </View>





                        <View style={{ flex: 0.1, flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end' }} >

                            <Text style={{ color: "#3E5481", fontFamily: "Prompt-Regular", fontSize: 20, marginRight: 4 }}>{countWork}</Text>

                        </View>


                        <View style={{ flex: 0.1, flexDirection: 'row', alignItems: 'center' }} >
                            <Text style={{ color: "#3E5481", fontFamily: "Prompt-Regular", fontSize: 20 }}> ชม.</Text>
                        </View>


                    </View>


                    <View style={{ height: 1, backgroundColor: "#EFEFEF", width: '100%', marginTop: 6 }} >

                    </View>


                    <Text style={{
                        color: "#3E5481", fontFamily: "Prompt-Bold", fontSize: 20, marginTop: 12, marginLeft: 24,
                        marginRight: 24,
                    }}>รายละเอียด</Text>


                    <Text style={{
                        color: "#9FA5C0", fontFamily: "Prompt-Regular", fontSize: 20, marginTop: 12, marginLeft: 24,
                        marginRight: 24,
                    }}>{detail}</Text>

                    {/* <TextInput
        multiline={true}
        style={{
            fontSize: 18, marginTop: 12, paddingLeft: 8, height: 100, fontFamily: "Prompt-Regular", textAlignVertical: 'top', borderWidth: 1,
            borderRadius: 12,
            borderColor: '#D0DBEA',
            marginLeft: 24,
            marginRight: 24,
            fontFamily: "Prompt-Regular",
            color: '#3E5481'
        }}
        value={detail}
        editable={false}
        selectTextOnFocus={false}
        onChange={(value) => {

        }}
    /> */}

                </View>


            </ScrollView>


        </View>


    );
}

Continue.navigationOptions = ({ navigation }) => ({
    title: 'ทำงานต่อ',
    headerBackTitleVisible: false,
    headerTitleStyle: { color: '#fff', fontFamily: 'Prompt-Bold' },
    headerBackTitleStyle: { color: '#fff', fontFamily: 'Prompt-Bold' },
    headerStyle: { backgroundColor: '#3E5481' },
    headerTintColor: 'white',
})

const styles = StyleSheet.create({
    MainContainer: {
        justifyContent: 'center',
        flex: 1,
        backgroundColor: '#FFF',
        flexDirection: "column",
    },
    loadding_style: {
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
        backgroundColor: '#fff',
    },
    appleBtn: { height: 44, width: 320, marginTop: 16 },
    spinnerTextStyle: {
        color: '#FFF',
        fontFamily: "NotoSans-Regular",
        fontSize: 14
    },
    button: {
        marginTop: 24,
        backgroundColor: "#e42179",
        width: "100%",
    },
    stretch: {
        resizeMode: "stretch",
        justifyContent: 'center',
        alignItems: 'center'
    },
    defaultText: {
        fontSize: 16,
        color: '#000'
    },
    imageShow: {
        width: '100%',
        height: '100%',
        resizeMode: 'contain',
    },

});

export default Continue