import React, { useState, useCallback, useMemo } from 'react';
import { StyleSheet, View, Text, Keyboard, Alert, Image, ActivityIndicator, AsyncStorage, Platform, StatusBar, Linking, TouchableOpacity, TouchableWithoutFeedback } from 'react-native';
import { Button, Input } from "react-native-elements";
import { useNavigation, useNavigationParam } from 'react-navigation-hooks'
import Ionicons from "react-native-vector-icons/FontAwesome5";
import Loading from 'react-native-loading-spinner-overlay';
import axios from 'axios';



const Login = () => {
    const { navigate } = useNavigation();

    const [isLoading, setLoading] = useState(false);
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');



    onChangeTextUsername = (value) => {
        setUsername(value)
    }

    onChangeTextPassword = (value) => {
        setPassword(value)
    }


    showAlert = (title, message) => {
        Alert.alert(
            title,
            message,
            [
                {
                    text: 'OK',
                    onPress: () => {
                        setLoading(false)
                        //navigate('Record')
                    }


                },
            ],
            { cancelable: false },
        );
    }


    callLogin = async (username, password) => {
        console.log('username : ', username)
        console.log('password : ', password)
        setLoading(true)

        axios.get('http://apitpm.bsisugarcane.com/api/Account/Login?username=' + username + '&password=' + password)
            .then((response) => {
                setLoading(false)
                console.log('response success : ', response.data.Results.token)

                if (response.data.Results.token != '') {
                    AsyncStorage.setItem('token', response.data.Results.token);
                    callProfile(response.data.Results.token)
                }
            })
            .catch((error) => {
                showAlert('ข้อความ', 'เข้าสู่ระบบไม่สำเร็จ กรุณาตรวจสอบ')
            })
            .finally(function () {
            });

    };


    callProfile = async (token) => {
        setLoading(true)
        axios.get('http://apitpm.bsisugarcane.com/api/Account/Profile?token=' + token)
            .then((response) => {
                var position = ""
                if (response.data.Results.UserRoles == null) {
                    position = ""
                } else {
                    position = response.data.Results.UserRoles
                }
                setLoading(false)
                AsyncStorage.setItem('name', response.data.Results.FullName);
                AsyncStorage.setItem('position', position);
                navigate('Choose')
            })
            .catch((error) => {
                console.log('error : ', error)
                // showAlert('ข้อความ', 'เข้าสู่ระบบไม่สำเร็จ กรุณาตรวจสอบ')
            })
            .finally(function () {
            });

    };


    return (
        <View style={{
            flex: 1,
            backgroundColor: "#fff",
            width: "100%",
        }}>
            {isLoading ?
                <Loading
                    visible={true}
                    textContent={'Loading...'}
                    textStyle={styles.spinnerTextStyle}
                />
                :
                <View style={styles.MainContainer}>
                    {/* <View style={{
                        flex: 0.4, justifyContent: 'center',
                        alignItems: 'center',
                    }} >

                    </View> */}
                    <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
                        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }} >

                            <Image
                                style={styles.stretch}
                                source={require('../img/lin.png')}
                            ></Image>

                            <Text style={[styles.baseText, { color: "#2E3E5C", fontFamily: "Prompt-Bold", fontSize: 22, marginTop: 48 }]}>Welcome TPM</Text>

                            <Text style={[styles.baseText, { color: "#9FA5C0", fontFamily: "Prompt-Regular", fontSize: 15, marginTop: 8 }]}>Please enter your account here</Text>
                            <Input
                                overflow="hidden"
                                keyboardAppearance="dark"
                                placeholder="User"
                                inputContainerStyle={{ backgroundColor: "#FFF", borderColor: '#D0DBEA', borderRadius: 25, borderWidth: 1, marginLeft: 24, marginRight: 24, paddingLeft: 24, height: 50, marginTop: 24 }}
                                inputStyle={{ fontSize: 15, fontFamily: "Prompt-Regular", height: 50 }}
                                leftIcon={<Ionicons name="user" size={20} style={{ paddingRight: 8, color: "#2E3E5C" }} solid />}
                                onChangeText={(value) => {
                                    onChangeTextUsername(value)
                                }}
                            />
                            <Input
                                overflow="hidden"
                                keyboardAppearance="dark"
                                placeholder="Password"
                                secureTextEntry={true}
                                inputContainerStyle={{ backgroundColor: "#FFF", borderColor: '#D0DBEA', borderRadius: 25, borderWidth: 1, marginLeft: 24, marginRight: 24, paddingLeft: 24, height: 50 }}
                                inputStyle={{ fontSize: 15, fontFamily: "Prompt-Regular", height: 50 }}
                                leftIcon={<Ionicons name="lock" size={20} style={{ paddingRight: 8, color: "#2E3E5C" }} />}
                                // rightIcon={<Ionicons name="eye" size={20} style={{ paddingRight: 20, color: "#2E3E5C" }} />}
                                onChangeText={(value) => {
                                    onChangeTextPassword(value)
                                }}
                            />
                            <Button
                                buttonStyle={{ backgroundColor: "#00598B", height: 50, justifyContent : "center" , marginTop: 48, borderRadius: 25, borderWidth: 1, borderColor: '#00598B' , marginLeft : 24 , marginRight : 24 }}
                                containerStyle={{width : '100%'}}
                                title="Login"
                                titleStyle={{ color: '#FFF', fontSize: 16, fontFamily: "Prompt-Bold" }}
                                onPress={() => {
                                    if (username == '') {
                                        showAlert('ข้อความ', 'กรุณากรอก username')
                                    } else if (password == '') {
                                        showAlert('ข้อความ', 'กรุณากรอก password')
                                    } else {
                                        callLogin(username, password)
                                    }

                                }
                                }>
                            </Button>
                        </View>

                    </TouchableWithoutFeedback>


                </View>
            }
        </View >
    );
}

const styles = StyleSheet.create({
    MainContainer: {
        justifyContent: 'center',
        flex: 1,
        backgroundColor: '#FFF',
        flexDirection: "column",
    },
    loadding_style: {
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
        backgroundColor: '#fff',
    },
    appleBtn: { height: 44, width: 320, marginTop: 16 },
    button: {
        marginTop: 24,
        backgroundColor: "#e42179",
        width: "100%",
    },
    stretch: {
        resizeMode: "stretch",
        justifyContent: 'center',
        alignItems: 'center'
    },
    defaultText: {
        fontSize: 16,
        color: '#000'
    },
    spinnerTextStyle: {
        color: '#FFF',
        fontFamily: "THSarabunNew",
        fontSize: 20
    },

});

export default Login